package project.helper;

import java.sql.Connection;

import adis.bnk.par.ZpracovaniPAR;

public class TestZpracovaniPAR {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		
		DbConnection dbConn = new DbConnection();
		dbConn.connect();
		Connection conn = dbConn.get_conn();

		System.out.println(String.format("Connection-URL    : %s", conn.getMetaData().getURL()));
		System.out.println(String.format("Connection-Catalog: %s", conn.getCatalog()));
		System.out.println();

		
//		XmlCheckSelfEmplRequest r = new XmlCheckSelfEmplRequest(conn, 1);
////		CheckSelfEmplRequest re = r.getRequest();
////		ByteArrayOutputStream baos = r.convertObjectToXmlOutputStream(re);
////		Helper.writeFile("e:/temp/pokus1.xml", baos.toByteArray());
//		r.createOrUpdateXmlToDb();

//		XmlCheckSelfEmplResponse r = new XmlCheckSelfEmplResponse(conn, 2);
////		CheckSelfEmplResponse re = r.getResponse();
////		ByteArrayOutputStream baos = r.convertObjectToXmlOutputStream(re);
////		Helper.writeFile("e:/temp/pokus1.xml", baos.toByteArray());
//		r.createOrUpdateXmlToDb();



		//checkSelfEmplRequest
//		processZpracovaniPAR(conn, 1);
		
		//checkSelfEmplResponse
//		processZpracovaniPAR(conn, 11);
////		processZpracovaniPAR(conn, 12);
////		processZpracovaniPAR(conn, 13);
////		processZpracovaniPAR(conn, 14);
		
		//enteredFlatRateRequest
//		processZpracovaniPAR(conn, 21);
//		processZpracovaniPAR(conn, 82);

		//enteredFlatRateResponse
//		processZpracovaniPAR(conn, 31);
		
		//providePaymentDetailsRequest
//		processZpracovaniPAR(conn, 41);
		
		//providePaymentDetailsResponse
//		processZpracovaniPAR(conn, 51);
//		processZpracovaniPAR(conn, 52);
////		processZpracovaniPAR(conn, 53);

		//terminateFlatRateRequest
//		processZpracovaniPAR(conn, 61);
//		processZpracovaniPAR(conn, 105);
		
		//terminateFlatRateResponse
//		processZpracovaniPAR(conn, 71);
//		processZpracovaniPAR(conn, 72);
//		processZpracovaniPAR(conn, 73);
		

		//chengeSubjectIdRequest
		processZpracovaniPAR(conn, 81);
		processZpracovaniPAR(conn, 82);
		processZpracovaniPAR(conn, 83);

		//chengeSubjectIdRequest
//		processZpracovaniPAR(conn, 91);
//		processZpracovaniPAR(conn, 92);
//		processZpracovaniPAR(conn, 93);
		
		System.out.println("Data zpracována...");
		
		conn.close();
		dbConn.close();
		
	}
	
	private static void processZpracovaniPAR(Connection conn, int cZaznamu) throws Exception {
		long startTime;
		long elapsedTime;

		startTime = System.nanoTime();
		ZpracovaniPAR zpracovaniPAR = new ZpracovaniPAR(conn, cZaznamu);
		zpracovaniPAR.zpracovani();
		elapsedTime = System.nanoTime() - startTime;
		System.out.println(String.format("Pro záznam: %s", elapsedTime/1000000));
		System.out.println(String.format("Total execution time in millis: %s", elapsedTime/1000000));
		System.out.println();
		
	}

}
