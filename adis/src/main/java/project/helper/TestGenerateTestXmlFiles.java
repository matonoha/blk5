package project.helper;

import java.io.ByteArrayOutputStream;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;

import org.xml.sax.SAXException;

import adis.bnk.par.changeSubjectId.request.ChangeSubjectIdRequest;
import adis.bnk.par.changeSubjectId.request.HelperXmlChangeSubjectIdRequest;
import adis.bnk.par.changeSubjectId.response.ChangeSubjectIdResponse;
import adis.bnk.par.changeSubjectId.response.HelperXmlChangeSubjectIdResponse;
import adis.bnk.par.checkSelfEmpl.request.CheckSelfEmplRequest;
import adis.bnk.par.checkSelfEmpl.request.HelperXmlCheckSelfEmplRequest;
import adis.bnk.par.checkSelfEmpl.response.CheckSelfEmplResponse;
import adis.bnk.par.checkSelfEmpl.response.HelperXmlCheckSelfEmplResponse;
import adis.bnk.par.enteredFlatRate.request.EnteredFlatRate;
import adis.bnk.par.enteredFlatRate.request.HelperXmlEnteredFlatRateRequest;
import adis.bnk.par.enteredFlatRate.response.EnteredFlatRateResponse;
import adis.bnk.par.enteredFlatRate.response.HelperXmlEnteredFlatRateResponse;
import adis.bnk.par.providePaymentDetails.request.HelperXmlProvidePaymentDetailsRequest;
import adis.bnk.par.providePaymentDetails.request.ProvidePaymentDetailsRequest;
import adis.bnk.par.providePaymentDetails.response.HelperXmlProvidePaymentDetailsResponse;
import adis.bnk.par.providePaymentDetails.response.ProvidePaymentDetailsResponse;
import adis.bnk.par.terminateFlatRate.request.HelperXmlTerminateFlatRateRequest;
import adis.bnk.par.terminateFlatRate.request.TerminateFlatRateRequest;
import adis.bnk.par.terminateFlatRate.response.HelperXmlTerminateFlatRateResponse;
import adis.bnk.par.terminateFlatRate.response.TerminateFlatRateResponse;

public class TestGenerateTestXmlFiles {

	public static void main(String[] args) throws JAXBException, SAXException, DatatypeConfigurationException {
		// TODO Auto-generated method stub

		long startTime;
		long elapsedTime;
		String filePath = "e:/work/GIST/ADIS/Balik05/DatovyModel F9b/V02/TestXmlFiles/";
		String fileName;
		ByteArrayOutputStream baos;
		String fullPathFileName;
		
		int pocetSubjektu;
		boolean generate = false;

		//-----------------------

		generate = false;
		if (generate) {
			startTime = System.nanoTime();
			
			pocetSubjektu = 1000;
			
			fileName = String.format("IDR-INT_IA_CheckSelfEmpl-P1_Request_v1.8_help_%s.xml", pocetSubjektu);
			HelperXmlCheckSelfEmplRequest helper = new HelperXmlCheckSelfEmplRequest(null);
			CheckSelfEmplRequest request = helper.getTestRequest(pocetSubjektu);
			baos = helper.convertObjectToXmlOutputStream(request);
			fullPathFileName = Helper.writeFile(String.format("%s%s", filePath, fileName), baos.toByteArray());
	
			elapsedTime = System.nanoTime() - startTime;
			System.out.println(String.format("%s", fullPathFileName));
			System.out.println(String.format("Total execution time in millis: %s", elapsedTime/1000000));
		}
		
		//----------------------------
		//-----------------------

		generate = false;
		if (generate) {
			startTime = System.nanoTime();
			
			pocetSubjektu = 20;
			int pocetIntervalySvcVSubjektu = 3;
			int pocetIntervalyZamVSubjektu = 3;
			
			fileName = String.format("IDR-INT_IA_CheckSelfEmpl-P2_Response_v1.9_help_%s_%s_%s.xml", pocetSubjektu, pocetIntervalySvcVSubjektu, pocetIntervalyZamVSubjektu);
			HelperXmlCheckSelfEmplResponse helper = new HelperXmlCheckSelfEmplResponse(null);
			CheckSelfEmplResponse response = helper.getTestResponse(pocetSubjektu, pocetIntervalySvcVSubjektu, pocetIntervalyZamVSubjektu);
			baos = helper.convertObjectToXmlOutputStream(response);
			fullPathFileName = Helper.writeFile(String.format("%s%s", filePath, fileName), baos.toByteArray());
	
			elapsedTime = System.nanoTime() - startTime;
			System.out.println(String.format("%s", fullPathFileName));
			System.out.println(String.format("Total execution time in millis: %s", elapsedTime/1000000));
		}
		
		//----------------------------
		//-----------------------
		
		generate = false;
		if (generate) {
			startTime = System.nanoTime();
			
			HelperXmlProvidePaymentDetailsRequest.PrijemceEnum prijemce = HelperXmlProvidePaymentDetailsRequest.PrijemceEnum.VZP;
			int pocetPolozek = 50;
			int pocetPlatebVPolozce = 2;
			
			fileName = String.format("IDR-INT_IA_ProvidePaymentDetails-P1_Request_v1.6_help_%s_%s_%s.xml", prijemce.toString(), pocetPolozek, pocetPlatebVPolozce);
			HelperXmlProvidePaymentDetailsRequest helper = new HelperXmlProvidePaymentDetailsRequest(null);
			ProvidePaymentDetailsRequest request = helper.getTestRequest(prijemce, pocetPolozek, pocetPlatebVPolozce);
			baos = helper.convertObjectToXmlOutputStream(request);
			fullPathFileName = Helper.writeFile(String.format("%s%s", filePath, fileName), baos.toByteArray());
	
			elapsedTime = System.nanoTime() - startTime;
			System.out.println(String.format("%s", fullPathFileName));
			System.out.println(String.format("Total execution time in millis: %s", elapsedTime/1000000));
		}

		//----------------------------
		//-----------------------
		
		generate = false;
		if (generate) {
			startTime = System.nanoTime();
			
			HelperXmlProvidePaymentDetailsResponse.PrijemceEnum prijemce = HelperXmlProvidePaymentDetailsResponse.PrijemceEnum.CSSZ;
			int pocetPolozek = 20;
			int pocetPlatebVPolozce = 3;
			
			fileName = String.format("IDR-INT_IA_ProvidePaymentDetails-P2_Response_v1.7_help_%s_%s_%s.xml", prijemce.toString(), pocetPolozek, pocetPlatebVPolozce);
			HelperXmlProvidePaymentDetailsResponse helper = new HelperXmlProvidePaymentDetailsResponse(null);
			ProvidePaymentDetailsResponse response = helper.getTestResponse(prijemce, pocetPolozek, pocetPlatebVPolozce);
			baos = helper.convertObjectToXmlOutputStream(response);
			fullPathFileName = Helper.writeFile(String.format("%s%s", filePath, fileName), baos.toByteArray());
	
			elapsedTime = System.nanoTime() - startTime;
			System.out.println(String.format("%s", fullPathFileName));
			System.out.println(String.format("Total execution time in millis: %s", elapsedTime/1000000));
		}

		//----------------------------
		//-----------------------

		generate = false;
		if (generate) {
			startTime = System.nanoTime();
			
			pocetSubjektu = 50;
			
			fileName = String.format("IDR-INT_IA_EnteredFlatRate-P1_Request_v1-9_help_%s.xml", pocetSubjektu);
			HelperXmlEnteredFlatRateRequest helper = new HelperXmlEnteredFlatRateRequest(null);
			EnteredFlatRate request = helper.getTestRequest(pocetSubjektu);
			baos = helper.convertObjectToXmlOutputStream(request);
			fullPathFileName = Helper.writeFile(String.format("%s%s", filePath, fileName), baos.toByteArray());
	
			elapsedTime = System.nanoTime() - startTime;
			System.out.println(String.format("%s", fullPathFileName));
			System.out.println(String.format("Total execution time in millis: %s", elapsedTime/1000000));
		}
		
		//----------------------------
		//-----------------------

		generate = false;
		if (generate) {
			startTime = System.nanoTime();
			
			pocetSubjektu = 50;
			
			fileName = String.format("IDR-INT_IA_EnteredFlatRate-P2_Response_v1.6_help_%s.xml", pocetSubjektu);
			HelperXmlEnteredFlatRateResponse helper = new HelperXmlEnteredFlatRateResponse(null);
			EnteredFlatRateResponse response = helper.getTestResponse(pocetSubjektu);
			baos = helper.convertObjectToXmlOutputStream(response);
			fullPathFileName = Helper.writeFile(String.format("%s%s", filePath, fileName), baos.toByteArray());
	
			elapsedTime = System.nanoTime() - startTime;
			System.out.println(String.format("%s", fullPathFileName));
			System.out.println(String.format("Total execution time in millis: %s", elapsedTime/1000000));
		}
		
		//----------------------------
		//-----------------------

		generate = false;
		if (generate) {
			startTime = System.nanoTime();
			
			int pocetPolozek = 50;
			int pocetKodyUpresneniDuvodu = 5;
			
			fileName = String.format("IDR-INT_IA_TerminateFlatRate-P1_Request_v1.6_help_%s_%s.xml", pocetPolozek, pocetKodyUpresneniDuvodu);
			HelperXmlTerminateFlatRateRequest helper = new HelperXmlTerminateFlatRateRequest(null);
			TerminateFlatRateRequest request = helper.getTestRequest(pocetPolozek, pocetKodyUpresneniDuvodu);
			baos = helper.convertObjectToXmlOutputStream(request);
			fullPathFileName = Helper.writeFile(String.format("%s%s", filePath, fileName), baos.toByteArray());
	
			elapsedTime = System.nanoTime() - startTime;
			System.out.println(String.format("%s", fullPathFileName));
			System.out.println(String.format("Total execution time in millis: %s", elapsedTime/1000000));
		}
		
		//----------------------------
		//-----------------------

		generate = false;
		if (generate) {
			startTime = System.nanoTime();
			
			int pocetPolozek = 2000;
			
			fileName = String.format("IDR-INT_IA_TerminateFlatRate-P2_Response_v1.1_help_%s.xml", pocetPolozek);
			HelperXmlTerminateFlatRateResponse helper = new HelperXmlTerminateFlatRateResponse(null);
			TerminateFlatRateResponse response = helper.getTestResponse(pocetPolozek);
			baos = helper.convertObjectToXmlOutputStream(response);
			fullPathFileName = Helper.writeFile(String.format("%s%s", filePath, fileName), baos.toByteArray());
	
			elapsedTime = System.nanoTime() - startTime;
			System.out.println(String.format("%s", fullPathFileName));
			System.out.println(String.format("Total execution time in millis: %s", elapsedTime/1000000));
		}
		
		//----------------------------
		//-----------------------

		generate = true;
		if (generate) {
			startTime = System.nanoTime();
			
			int pocetPolozek = 1000;
			
			fileName = String.format("ADIS_IA_ChangeSubjectId-P1_Request_v1.16_help_%s.xml", pocetPolozek);
			HelperXmlChangeSubjectIdRequest helper = new HelperXmlChangeSubjectIdRequest(null);
			ChangeSubjectIdRequest request = helper.getTestRequest(pocetPolozek);
			baos = helper.convertObjectToXmlOutputStream(request);
			fullPathFileName = Helper.writeFile(String.format("%s%s", filePath, fileName), baos.toByteArray());
	
			elapsedTime = System.nanoTime() - startTime;
			System.out.println(String.format("%s", fullPathFileName));
			System.out.println(String.format("Total execution time in millis: %s", elapsedTime/1000000));
		}
		
		//----------------------------
		//-----------------------

		generate = false;
		if (generate) {
			startTime = System.nanoTime();
			
			int pocetPolozek = 100;
			
			fileName = String.format("ADIS_IA_ChangeSubjectId-P2_Response_v1.2_help_%s.xml", pocetPolozek);
			HelperXmlChangeSubjectIdResponse helper = new HelperXmlChangeSubjectIdResponse(null);
			ChangeSubjectIdResponse response = helper.getTestResponse(pocetPolozek);
			baos = helper.convertObjectToXmlOutputStream(response);
			fullPathFileName = Helper.writeFile(String.format("%s%s", filePath, fileName), baos.toByteArray());
	
			elapsedTime = System.nanoTime() - startTime;
			System.out.println(String.format("%s", fullPathFileName));
			System.out.println(String.format("Total execution time in millis: %s", elapsedTime/1000000));
		}
		
		//----------------------------
		
		System.out.println("Data zpracována...");

	}

}
