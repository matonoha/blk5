package project.helper;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FillTXParOdDoXml {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		class Tuple<X, Y> { 
			  public final X x; 
			  public final Y y; 
			  public Tuple(X x, Y y) { 
			    this.x = x; 
			    this.y = y; 
			  } 
			} 
		
		DbConnection dbConn = new DbConnection();
		dbConn.connect();
		Connection conn = dbConn.get_conn();
		
		try {
			conn.setAutoCommit(false);

			List<Tuple<String, Integer>> list = new ArrayList<Tuple<String, Integer>>();

			//checkSelfEmplResponse
//			list.add(
//					new Tuple<String, Integer>(
//							"e:/work/GIST/ADIS/Balik05/DatovyModel F9b/V01/TestXmlFiles/IDR-INT_IA_CheckSelfEmpl-P2_Response_v1.9_help_20_3_3.xml",
//							11)
//			);
////			list.add(
////					new Tuple<String, Integer>(
////							"e:/work/GIST/ADIS/Balik05/DatovyModel F9b/V01/TestXmlFiles/",
////							12)
////			);
////			list.add(
////					new Tuple<String, Integer>(
////							"e:/work/GIST/ADIS/Balik05/DatovyModel F9b/V01/TestXmlFiles/",
////							13)
////			);
////			list.add(
////					new Tuple<String, Integer>(
////							"e:/work/GIST/ADIS/Balik05/DatovyModel F9b/V01/TestXmlFiles/",
////							14)
////			);

			//enteredFlatRateResponse
//			list.add(
//					new Tuple<String, Integer>(
//							"e:/work/GIST/ADIS/Balik05/DatovyModel F9b/V01/TestXmlFiles/IDR-INT_IA_EnteredFlatRate-P2_Response_v1.6_help_50.xml",
//							31)
//			);
//
//			//providePaymentDetailsResponse
//			list.add(
//					new Tuple<String, Integer>(
//							"e:/work/GIST/ADIS/Balik05/DatovyModel F9b/V01/TestXmlFiles/IDR-INT_IA_ProvidePaymentDetails-P2_Response_v1.7_help_CSSZ_20_3.xml",
//							51)
//			);
//			list.add(
//					new Tuple<String, Integer>(
//							"e:/work/GIST/ADIS/Balik05/DatovyModel F9b/V01/TestXmlFiles/IDR-INT_IA_ProvidePaymentDetails-P2_Response_v1.7_help_VZP_20_3.xml",
//							52)
//			);
////			list.add(
////					new Tuple<String, Integer>(
////							"e:/work/GIST/ADIS/Balik05/DatovyModel F9b/V01/TestXmlFiles/",
////							53)
////			);

			//terminateFlatRateResponse
//			list.add(
//					new Tuple<String, Integer>(
//							"e:/work/GIST/ADIS/Balik05/DatovyModel F9b/V01/TestXmlFiles/IDR-INT_IA_TerminateFlatRate-P2_Response_v1.1_help_50.xml",
//							71)
//			);
//			list.add(
//					new Tuple<String, Integer>(
//							"e:/work/GIST/ADIS/Balik05/DatovyModel F9b/V01/TestXmlFiles/IDR-INT_IA_TerminateFlatRate-P2_Response_v1.1_help_1000.xml",
//							72)
//			);
//			list.add(
//					new Tuple<String, Integer>(
//							"e:/work/GIST/ADIS/Balik05/DatovyModel F9b/V01/TestXmlFiles/IDR-INT_IA_TerminateFlatRate-P2_Response_v1.1_help_2000.xml",
//							73)
//			);

			//changeSubjectIdResponse
			list.add(
					new Tuple<String, Integer>(
							"e:/work/GIST/ADIS/Balik05/DatovyModel F9b/V02/TestXmlFiles/ADIS_IA_ChangeSubjectId-P1_Request_v1.16_help_10.xml",
							91)
			);
			list.add(
					new Tuple<String, Integer>(
							"e:/work/GIST/ADIS/Balik05/DatovyModel F9b/V02/TestXmlFiles/ADIS_IA_ChangeSubjectId-P1_Request_v1.16_help_100.xml",
							92)
			);
			list.add(
					new Tuple<String, Integer>(
							"e:/work/GIST/ADIS/Balik05/DatovyModel F9b/V02/TestXmlFiles/ADIS_IA_ChangeSubjectId-P1_Request_v1.16_help_1000.xml",
							93)
			);
			
			
			System.out.println(String.format("Connection-URL    : %s", conn.getMetaData().getURL()));
			System.out.println(String.format("Connection-Catalog: %s", conn.getCatalog()));
			System.out.println();
			
			for(Tuple<String, Integer> item : list) {
				File xmlFile = new File(item.x);
				byte[] bytesPrilSoub = Helper.readFileToByteArray(xmlFile);
		
				PreparedStatement stmt = conn.prepareStatement("update t_x_par_oddo_xml set xml_dosly=? where c_zaznamu = ?;");
				stmt.setBytes(1, bytesPrilSoub);
				stmt.setInt(2, item.y);
				boolean result = stmt.execute();
		
				conn.commit();
				
				stmt = conn.prepareStatement("select xml_dosly from t_x_par_oddo_xml where c_zaznamu = ?;");
				stmt.setInt(1, item.y);
				ResultSet resultSet = stmt.executeQuery();
				
				byte[] byteArray = null;
				while(resultSet.next()) {
					byteArray = resultSet.getBytes("xml_dosly");
				}
		
				if (byteArray != null) {
					Helper.writeFile("e:/temp/byteArray.xml", byteArray);
				}
		
				System.out.println(String.format("Data %s vložena do tabulky s identifikátorem %s...", item.x, item.y));
			}
		} 
		catch(Exception exc) {
			conn.rollback();
			throw exc;
		} finally {
			conn.close();
			dbConn.close();
		}

	}

}
