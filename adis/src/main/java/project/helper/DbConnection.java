package project.helper;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnection {
	private Connection _conn = null;
	public Connection get_conn() {
		return _conn;
	}

	private Driver _driver = null;

	//conection do 10.0.2.109:1533 - Test2
//	private static String _connString = "jdbc:informix-sqli://"
//			+ "10.0.2.109:1533"
//			+ "/test2"
//			+ ":informixserver=ol_informix1170"
//			+ ";user=test"
//			+ ";password=test*0"
//			+ ";CLIENT_LOCALE=en_US.utf8"
//			+ ";DB_LOCALE=en_US.utf8"
//			+ ";charSet=utf8";
//	private final String _defaultSchema = "test";

	//conection do localhost:4928 - Test2
//	private static String _connString = "jdbc:informix-sqli://"
//			+ "localhost:4928"
//			+ "/test2"
//			+ ":informixserver=ol_informix1210_6"
//			+ ";user=ifxuser"
//			+ ";password=TesTTesT*0"
//			+ ";CLIENT_LOCALE=en_US.utf8"
//			+ ";DB_LOCALE=en_US.utf8"
//			+ ";charSet=utf8";
//	private final String _defaultSchema = "ifxuser";
	
	
	//connection do GIST 41.D6C
//	private static String _connString = "jdbc:informix-sqli://"
//			+ "k1:11051"
//			+ "/adis_db_fu"
//			+ ":INFORMIXSERVER=db_serv_tcp1"
//			+ ";user=wwwspr"
//			+ ";password=wwwspr"
//			+ ";DBCENTURY=C;DB_LOCALE=cs_cz.8859-2@sis;GL_DATE=%d.%m.%Y;IFX_LOCK_MODE_WAIT=5;OPTOFC=1;lobcache=20000000";
//	private final String _defaultSchema = "prog";	

	//connection do GIST 41.X9
//	private static String _connString = "jdbc:informix-sqli://"
//			+ "k1:11052"
//			+ "/adis_db_fu"
//			+ ":INFORMIXSERVER=db_serv_tcp2"
//			+ ";user=wwwspr"
//			+ ";password=wwwspr"
//			+ ";DBCENTURY=C;DB_LOCALE=cs_cz.8859-2@sis;GL_DATE=%d.%m.%Y;IFX_LOCK_MODE_WAIT=5;OPTOFC=1;lobcache=20000000";
//	private final String _defaultSchema = "prog";	

	//connection do GIST 41.X10
	private static String _connString = "jdbc:informix-sqli://"
			+ "k1:11055"
			+ "/adis_db_fu"
			+ ":INFORMIXSERVER=db_serv_tcp5"
			+ ";user=wwwspr"
			+ ";password=wwwspr"
			+ ";DBCENTURY=C;DB_LOCALE=cs_cz.8859-2@sis;GL_DATE=%d.%m.%Y;IFX_LOCK_MODE_WAIT=5;OPTOFC=1;lobcache=20000000";
	private final String _defaultSchema = "prog";	
	
	public DbConnection() {}
	
	public void connect() {
		try {
			_driver = new com.informix.jdbc.IfxDriver();
			DriverManager.registerDriver(_driver);
			this._conn = DriverManager.getConnection(_connString);
			this._conn.setSchema(_defaultSchema);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void close() {
		try {
			this._conn.close();
			DriverManager.deregisterDriver(_driver);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
