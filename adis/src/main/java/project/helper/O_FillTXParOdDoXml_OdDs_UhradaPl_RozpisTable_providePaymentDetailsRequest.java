package project.helper;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;



/**
 * @author valenta
 *
 * pro generovani zaznamu providePaymentDetailsRequest
 */
public class O_FillTXParOdDoXml_OdDs_UhradaPl_RozpisTable_providePaymentDetailsRequest {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		String typZazn = "O";
		String typSoub = "R";

		int cZaznamu = 41;
		int pocetZaznamuTXParOdDs = 20;
		int maxPocetZaznamuTXParUhradaPl = 7;
		
		DbConnection dbConn = new DbConnection();
		dbConn.connect();
		Connection conn = dbConn.get_conn();
		
		try {

			if (maxPocetZaznamuTXParUhradaPl < 1) {
				maxPocetZaznamuTXParUhradaPl = 1;
			}

			conn.setAutoCommit(false);
	
			PreparedStatement stmt = conn.prepareStatement("delete from t_x_par_uhrada_pl where c_zaznamu = ?;");
			stmt.setInt(1, cZaznamu);
			stmt.execute();

			stmt = conn.prepareStatement("delete from t_x_par_rozpis where c_zaznamu = ?;");
			stmt.setInt(1, cZaznamu);
			stmt.execute();

			stmt = conn.prepareStatement("delete from t_x_par_od_ds where c_zaznamu = ?;");
			stmt.setInt(1, cZaznamu);
			stmt.execute();

			stmt = conn.prepareStatement("delete from t_x_par_oddo_xml where c_zaznamu = ?;");
			stmt.setInt(1, cZaznamu);
			stmt.execute();

			//TXParOdDoXml
			String TXParOdDoXmlInsert = 
					String.format(
							" insert into t_x_par_oddo_xml " +
							" (c_zaznamu, typ_zazn, typ_soub, d_vznik, c_soubor, correlation_id, c_fu)" +
							" values " +
							" (%s, '%s', '%s', current, %s, '%s', %s); ", 
							cZaznamu, //c_zaznamu
							typZazn, //typ_zazn
							typSoub, //typ_soub
							((int)(Math.random()*100000)), //c_soubor
							UUID.randomUUID().toString(), //correlation_id
							((int)(Math.random()*999)) //c_fu
							);
		
			stmt = conn.prepareStatement(TXParOdDoXmlInsert);
			stmt.execute();
			
			int pocetCelk = 0;
			double kcCelk = 0;
			
			for(int i=1; i<=pocetZaznamuTXParOdDs; i++) {
	
				String messageIdFirstPart = String.format("%s%s", "00000000", i);
				messageIdFirstPart = messageIdFirstPart.substring(messageIdFirstPart.length()-8);
				String messageId = String.format("%s-8c14-471c-9701-f3f3ab532cc3", messageIdFirstPart);
				
				//TXParOdDs
				String TXParOdDsInsert = 
						String.format(
								" insert into t_x_par_od_ds " +
								" (c_zaznamu, message_id, aifo, rodne_cislo, c_poj, ecp, " +
								"  jmeno, prijmeni, rodnepr, d_naroz, ke_dni, " +
								"  id_zahajeni, id_zahajeni_o, id_zahajeni_orig, d_zahajeni, d_zahsvc, " +
								"  id_ukonceni, id_ukonceni_o, pref_cssz)" +
								" values " +
								" (%s, '%s', %s, %s, %s, %s, " +
								"  'Jan%s', 'Novák%s', %s, DATE('6/%s/1990'), null, " + 
								"  null, null, null, null, null, " + 
								"  null, null, '%s'); ", 
								cZaznamu, //c_zaznamu
								messageId, //message_id
								(int)(Math.random()*2) == 0 ? String.format("'bafe64fcbfcfabfa1666666%s'", (int)(Math.random()*9)) : "null" , //aifo
								(int)(Math.random()*2) == 0 ? String.format("'166666666%s'", (int)(Math.random()*9)) : "null", //rodne_cislo
								(int)(Math.random()*2) == 0 ? String.format("'160000000%s'", (int)(Math.random()*9)) : "null", //c_poj
								(int)(Math.random()*2) == 0 ? String.format("'161111111%s'", (int)(Math.random()*9)) : "null", //ecp
								i, //jmeno
								i, //prijmeni
								(int)(Math.random()*2) == 0 ? String.format("'Novák rodné%s'", i) : "null", //rodnepr
								((int)(Math.random()*11))+1, //d_naroz
								(int)(Math.random()*2) == 0 ? "rc" : "ecp"//pref_cssz
								);
	
				PreparedStatement stmtTXParOdDs = conn.prepareStatement(TXParOdDsInsert);
				stmtTXParOdDs.execute();

				System.out.println(String.format("Zaznam t_x_par_od_ds: %s::%s vložen...", cZaznamu, i));
				
				int maxPocetZaznamuTXParUhradaPlConcrete = (int)(Math.random()*maxPocetZaznamuTXParUhradaPl);
				if (maxPocetZaznamuTXParUhradaPlConcrete < 1) {
					maxPocetZaznamuTXParUhradaPlConcrete = 1;
				}

				for(int j=1; j<=maxPocetZaznamuTXParUhradaPlConcrete; j++) {
					
					String cUhrady = String.format("%s00000%s00000000", i, j);
					cUhrady = cUhrady.substring(0, 15);
					double kcUhrady = 10000.5+(10*i)+j;
					
					String cPuvUhrady = "null";
					String typUhrady = "PLATBA";
					if ((int)(Math.random()*2) == 0) {
						cPuvUhrady = String.format("'%s'", String.format("%s00000%s00000000", j, i).substring(0,15));
						typUhrady = "OPRAVA";
					}
					
					
					//TXParUhradaPl
					String TXParUhradaPlInsert = 
							String.format(
									" insert into t_x_par_uhrada_pl " +
									" (c_zaznamu, message_id, c_uhrady, c_puv_uhrady, typ_uhrady, obdobi, c_popl, n_titul, d_uhrady, kc_uhrady, k_zprac_cssz, p_zprac_cssz) " +
									" values " +
									" (%s, '%s', %s, %s, '%s', '0%s/2021', '123456789', '%s', DATE('7/%s/2021'), %s, null, null); ",
									cZaznamu, //c_zaznamu
									messageId, //message_id
									cUhrady, //c_uhrady
									cPuvUhrady, //c_puv_uhrady
									typUhrady, //typ_uhrady
									((int)(Math.random()*9)), //obdobi
									(int)(Math.random()*2) == 0 ? "PLPOJ" : "PLPRI", //n_titul
									((int)(Math.random()*11))+1, //d_uhrady
									kcUhrady //kc_uhrady
									);
	
					PreparedStatement stmtTXParUhradaPl = conn.prepareStatement(TXParUhradaPlInsert);
					stmtTXParUhradaPl.execute();
					
					pocetCelk++;
					kcCelk = kcCelk + kcUhrady;
					
					System.out.println(String.format("	Zaznam t_x_par_uhrada_pl: %s::%s:%s vložen...", cZaznamu, i, j));
					
				}

			}
			
			//TXParRozpis
			String TXParRozpisInsert = 
					String.format(
							" insert into t_x_par_rozpis " +
							" (c_zaznamu, kod_prijemce, c_popl_prijemce, spec_symb, obdobi, d_platby, pocet_celk " +
							" ,kc_celk, n_dan, kc_dan, pocet_dan, n_prisl, kc_prisl, pocet_prisl, status_zprac) " +
							" values " +
							" (%s, 'VZP', '9', 1, '0%s/2020', DATE('6/%s/2020'), %s " +
							" ,%s, 'PLPOJ', 9000.00, 4, 'PLPRI', 3000.50, 5, null); ", 
							cZaznamu, //c_zaznamu 
							((int)(Math.random()*8))+1, //obdobi
							((int)(Math.random()*11))+1, //d_platby
							pocetCelk, //pocet_celk
							kcCelk //kc_celk
							);

			PreparedStatement stmtTXParRozpis = conn.prepareStatement(TXParRozpisInsert);
			stmtTXParRozpis.execute();

			System.out.println(String.format("Zaznam t_x_par_rozpis: %s vložen...", cZaznamu));

			conn.commit();
				
			System.out.println(String.format(""));
			System.out.println(String.format("Data vložena do tabulek..."));
		} 
		catch(Exception exc) {
			conn.rollback();
			throw exc;
		} finally {
			conn.close();
			dbConn.close();
		}

	}

}
