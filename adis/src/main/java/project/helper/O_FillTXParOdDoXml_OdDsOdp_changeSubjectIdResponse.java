package project.helper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.UUID;


/**
 * @author valenta
 *
 *	pro generovani zaznamu changeSubjectIdResponse
 */
public class O_FillTXParOdDoXml_OdDsOdp_changeSubjectIdResponse {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		int cZaznamu = 81;
		int pocetZaznamuTXParOdDsOdp = 50;
		
		DbConnection dbConn = new DbConnection();
		dbConn.connect();
		Connection conn = dbConn.get_conn();
		
		try {
			
			if (pocetZaznamuTXParOdDsOdp < 1) {
				pocetZaznamuTXParOdDsOdp = 1;
			}
			
			conn.setAutoCommit(false);
	
			//mazani hodnot pro c_zaznamu v tabulce t_x_par_od_ds_odp
			PreparedStatement stmt = conn.prepareStatement("delete from t_x_par_od_ds_odp where c_zaznamu = ?;");
			stmt.setInt(1, cZaznamu);
			stmt.execute();

			//mazani hodnot pro c_zaznamu v tabulce t_x_par_od_do_xml
			stmt = conn.prepareStatement("delete from t_x_par_oddo_xml where c_zaznamu = ?;");
			stmt.setInt(1, cZaznamu);
			stmt.execute();

			//TXParOdDoXml
			String TXParOdDoXmlInsert = 
					String.format(
							" insert into t_x_par_oddo_xml " +
							" (c_zaznamu, typ_zazn, typ_soub, d_vznik, c_soubor, correlation_id, c_fu, kod_prijodes)" +
							" values " +
							" (%s, 'O', 'Z', current, %s, '%s', %s, %s); ", 
							cZaznamu, //c_zaznamu
							((int)(Math.random()*100000)), //c_soubor
							UUID.randomUUID().toString(), //correlation_id
							((int)(Math.random()*999)), //c_fu
							((int)(Math.random()*2)) == 0 ? "'Z'" : "'S'" //kod_prijodes
							);

			PreparedStatement stmtTXParOdDoXml = conn.prepareStatement(TXParOdDoXmlInsert);
			stmtTXParOdDoXml.execute();

			System.out.println(String.format("Zaznam t_x_par_oddo_xml: %s vložen...", cZaznamu));
			
			String messageIdSecondPart = UUID.randomUUID().toString().substring(8,36);
			for(int i=1; i<=pocetZaznamuTXParOdDsOdp; i++) {
	
				String messageIdFirstPart = String.format("%s%s", "00000000", i);
				messageIdFirstPart = messageIdFirstPart.substring(messageIdFirstPart.length()-8);
				String messageId = String.format("%s%s", messageIdFirstPart, messageIdSecondPart);
				
				//TXParOdDsOdp
				String TXParOdDsOdpInsert = 
						String.format(
								" insert into t_x_par_od_ds_odp " +
								" (c_zaznamu, message_id, kod_odp, duvod_odp, popis_odp)" +
								" values " +
								" (%s, '%s', %s, %s, %s); ", 
								cZaznamu, 
								messageId, 
								((int)(Math.random()*2)) == 0 ? "'A'" : "'N'", //kod_odp
								((int)(Math.random()*2)) == 0 ? String.format("'duvod_%s'", messageId) : "null", //duvod_odp
								((int)(Math.random()*2)) == 0 ? String.format("'popis_%s'", messageId) : "null" //popis_odp
								);
	
				PreparedStatement stmtTXParOdDsOdp = conn.prepareStatement(TXParOdDsOdpInsert);
				stmtTXParOdDsOdp.execute();

				System.out.println(String.format("Zaznam t_x_par_od_ds_odp: %s::%s vložen...", cZaznamu, i));

			}
			
			conn.commit();
				
			System.out.println(String.format(""));
			System.out.println(String.format("Data vložena do tabulek..."));
		} 
		catch(Exception exc) {
			conn.rollback();
			throw exc;
		} finally {
			conn.close();
			dbConn.close();
		}

	}

}
