package project.helper;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;



/**
 * @author valenta
 *
 * pro generovani zaznamu providePaymentDetailsRequest
 */
public class O_FillTXParOdDoXml_OdDs_checkSelfEmplRequest {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		String typZazn = "O";
		String typSoub = "O";
		
		int cZaznamu = 1;
		int pocetZaznamuTXParOdDs = 20;
		
		DbConnection dbConn = new DbConnection();
		dbConn.connect();
		Connection conn = dbConn.get_conn();
		
		try {

			conn.setAutoCommit(false);
	
			PreparedStatement stmt = conn.prepareStatement("delete from t_x_par_od_ds where c_zaznamu = ?;");
			stmt.setInt(1, cZaznamu);
			stmt.execute();

			stmt = conn.prepareStatement("delete from t_x_par_oddo_xml where c_zaznamu = ?;");
			stmt.setInt(1, cZaznamu);
			stmt.execute();

			//TXParOdDoXml
			String TXParOdDoXmlInsert = 
					String.format(
							" insert into t_x_par_oddo_xml " +
							" (c_zaznamu, typ_zazn, typ_soub, d_vznik, c_soubor, correlation_id, c_fu)" +
							" values " +
							" (%s, '%s', '%s', current, %s, '%s', %s); ", 
							cZaznamu, //c_zaznamu
							typZazn, //typ_zazn
							typSoub, //typ_soub
							((int)(Math.random()*100000)), //c_soubor
							UUID.randomUUID().toString(), //correlation_id
							((int)(Math.random()*999)) //c_fu
							);
		
			stmt = conn.prepareStatement(TXParOdDoXmlInsert);
			stmt.execute();
			
			for(int i=1; i<=pocetZaznamuTXParOdDs; i++) {
	
				String messageIdFirstPart = String.format("%s%s", "00000000", i);
				messageIdFirstPart = messageIdFirstPart.substring(messageIdFirstPart.length()-8);
				String messageId = String.format("%s-8c14-471c-9701-f3f3ab532cc3", messageIdFirstPart);
				
				//TXParOdDs
				
				int cAdrbodOrAdresa = (int)(Math.random()*2);
				String TXParOdDsInsert = 
						String.format(
								" insert into t_x_par_od_ds " +
								" (c_zaznamu, message_id, aifo, rodne_cislo, c_poj, ecp, " +
								"  jmeno, prijmeni, rodnepr, d_naroz, ke_dni, " +
								"  c_adrbod, adresa)" +
								" values " +
								" (%s, '%s', %s, %s, %s, %s, " +
								"  'Jan%s', 'Novák%s', %s, DATE('6/%s/1990'), DATE('7/%s/2020'), " + 
								"  %s, %s); ", 
								cZaznamu, //c_zaznamu
								messageId, //message_id
								(int)(Math.random()*2) == 0 ? String.format("'bafe64fcbfcfabfa1666666%s'", (int)(Math.random()*9)) : "null" , //aifo
								(int)(Math.random()*2) == 0 ? String.format("'166666666%s'", (int)(Math.random()*9)) : "null", //rodne_cislo
								String.format("'160000000%s'", (int)(Math.random()*9)), //c_poj
								(int)(Math.random()*2) == 0 ? String.format("'161111111%s'", (int)(Math.random()*9)) : "null", //ecp
								i, //jmeno
								i, //prijmeni
								(int)(Math.random()*2) == 0 ? String.format("'Novák rodné%s'", i) : "null", //rodnepr
								((int)(Math.random()*11))+1, //d_naroz
								((int)(Math.random()*11))+1, //ke_dni
								cAdrbodOrAdresa == 0 ? ((int)(Math.random()*10000000)) : "null", //c_adrbod
								cAdrbodOrAdresa == 1 ? String.format("'Adresa_%s'", i) : "null" //adresa
								);
	
				PreparedStatement stmtTXParOdDs = conn.prepareStatement(TXParOdDsInsert);
				stmtTXParOdDs.execute();

				System.out.println(String.format("Zaznam t_x_par_od_ds: %s::%s vložen...", cZaznamu, i));
				
			}
			
			conn.commit();
				
			System.out.println(String.format(""));
			System.out.println(String.format("Data vložena do tabulek..."));
		} 
		catch(Exception exc) {
			conn.rollback();
			throw exc;
		} finally {
			conn.close();
			dbConn.close();
		}

	}

}
