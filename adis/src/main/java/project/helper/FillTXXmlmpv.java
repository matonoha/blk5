package project.helper;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FillTXXmlmpv {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		class Tuple<X, Y> { 
			  public final X x; 
			  public final Y y; 
			  public Tuple(X x, Y y) { 
			    this.x = x; 
			    this.y = y; 
			  } 
			} 
		
		DbConnection dbConn = new DbConnection();
		dbConn.connect();
		Connection conn = dbConn.get_conn();
		
		try {
			conn.setAutoCommit(false);
			

			List<Tuple<String, Integer>> list = new ArrayList<Tuple<String, Integer>>();
////			list.add(
////					new Tuple<String, Integer>(
////							"e:/work/GIST/ADIS/Balik06/2021-02-08 Test XML/MojePres_eForms/CZ_AT_aaaa_rrrrrrrrrrrr_20210209_c_RI.xml",
////							10
////					)
////			);
//			list.add(
//					new Tuple<String, Integer>(
//							"e:/work/GIST/ADIS/Balik06/2021-02-08 Test XML/MojePres_eForms/v02.07.00_CZ_BE_bbbb_rrrrrrrrrrrr_20210209_de_RR.xml",
//							11
//					)
//			);
////			list.add(
//// 					new Tuple<String, Integer>(
////							"e:/work/GIST/ADIS/Balik06/2021-02-08 Test XML/MojePres_eForms/CZ_CY_aaaa_rrrrrrrrrrrr_20210209_c_RN.xml",
////							12
////					)
////			);
////			list.add( 
////					new Tuple<String, Integer>(
////							"e:/work/GIST/ADIS/Balik06/2021-02-08 Test XML/PřikladyXML-newXSD/CZ_AT_954_435_20180328_d_RR.xml",
////							13
////					)
////			);
////			list.add( 
////					new Tuple<String, Integer>(
////							"e:/work/GIST/ADIS/Balik06/2021-02-08 Test XML/PřikladyXML-newXSD/CZ_AT_954_435_20180328_d_RR_valid.xml",
////							14
////					)
////			);
////			list.add( 
////					new Tuple<String, Integer>(
////							"e:/work/GIST/ADIS/Balik06/2021-02-08 Test XML/PřikladyXML-newXSD/CZ_AT_954_rrrrrrrrrrrr_20180201_d_RR.xml",
////							15
////					)
////			);
////			list.add( 
////					new Tuple<String, Integer>(
////							"e:/work/GIST/ADIS/Balik06/2021-02-08 Test XML/PřikladyXML-XSD v4/v02.04.02_CZ_FR_aaaaaaaaaaaa_rrrrrrrrrrrr_20180328_h_RR.xml",
////							16
////					)
////			);
////			list.add( 
////					new Tuple<String, Integer>(
////							"e:/work/GIST/ADIS/Balik06/2021-02-08 Test XML/PřikladyXML-XSD v4/v02.04.00_CZ_AT_aaaaaaaaaaaa_rrrrrrrrrrrr_20171206__RI.xml",
////							17
////					)
////			);
////			list.add( 
////					new Tuple<String, Integer>(
////							"e:/work/GIST/ADIS/Balik06/2021-02-08 Test XML/PřikladyXML-XSD v4/v02.04.00_CZ_AT_aaaaaaaaaaaa_rrrrrrrrrrrr_20171206__RI_valid.xml",
////							18
////					)
////			);
//			list.add( 
//					new Tuple<String, Integer>(
//							"e:/work/GIST/ADIS/Balik06/2021-02-08 Test XML/2021-02-10RealnaData/210105c_DE_CZ_S1316CZ2000322_809572030133013511_20210105_bd_RR.xml",
//							19
//					)
//			);
//			list.add( 
//					new Tuple<String, Integer>(
//							"e:/work/GIST/ADIS/Balik06/2021-02-08 Test XML/2021-02-10RealnaData/SK_CZ_1207552019LNG_991771930131010590_20210202_b_RR.xml",
//							20
//					)
//			);
//			list.add( 
//					new Tuple<String, Integer>(
//							"e:/work/GIST/ADIS/Balik06/2021-02-08 Test XML/2021-02-10RealnaData/210115_DE_CZ_S1316CZ2000327_11252130133013511_20210104_b_RR.xml",
//							21
//					)
//			);
//			list.add( 
//					new Tuple<String, Integer>(
//							"e:/work/GIST/ADIS/Balik06/2021-02-08 Test XML/2021-02-10RealnaData/210126_DE_CZ_S1316CZ2100008_74342130133010590_20210112_bd_RR.xml",
//							22
//					)
//			);
//					list.add( 
//					new Tuple<String, Integer>(
//							"e:/work/GIST/ADIS/Balik06/2021-02-08 Test XML/MojePres_eForms/v02.07.00_CZ_PL_aaaa_cccc_20210216_abcdefghijklm_RR.xml",
//							23
//					)
//			);
//			list.add( 
//					new Tuple<String, Integer>(
//							"e:/work/GIST/ADIS/Balik06/2021-02-08 Test XML/2021-02-10RealnaData/DE_CZ_S1316CZ2000170_408142030133013511_20201229_b_RR.xml",
//							24
//					)
//			);
//			list.add( 
//					new Tuple<String, Integer>(
//							"e:/work/GIST/ADIS/Balik06/2021-03-24 Parizek/XmlEForms/CZ_BE_AA1_rrrrrrrrrrrr_20210324_abcf_RR.xml",
//							25
//					)
//			);

//			list.add( 
//			new Tuple<String, Integer>(
//					"e:/work/GIST/ADIS/Balik06/2021-03-25 Test XML/v02.07.00_eForms/v02.07.00_CZ_BE_AA1_rrrrrrrrrrrr_20210324_abcf_RR.xml",
//					30
//					)
//			);
//			list.add( 
//			new Tuple<String, Integer>(
//					"e:/work/GIST/ADIS/Balik06/2021-03-25 Test XML/v02.07.00_eForms/v02.07.00_CZ_BE_bbbb_rrrrrrrrrrrr_20210209_de_RR.xml",
//					31
//					)
//			);
//			list.add( 
//			new Tuple<String, Integer>(
//					"e:/work/GIST/ADIS/Balik06/2021-03-25 Test XML/v02.07.00_eForms/v02.07.00_CZ_PL_aaaa_cccc_20210216_abcdefghijklm_RR.xml",
//					32
//					)
//			);
//			list.add( 
//			new Tuple<String, Integer>(
//					"e:/work/GIST/ADIS/Balik06/2021-03-25 Test XML/v02.07.00_eForms/v02.07.00_CZ_GB_AA256_rrrrrrrrrrrr_20210326_abcf_RN.xml",
//					33
//					)
//			);
//
//			list.add( 
//			new Tuple<String, Integer>(
//					"e:/work/GIST/ADIS/Balik06/2021-03-25 Test XML/v02.07.00_eForms/v02.07.00_CZ_CZ_test_maxc_20210326_generatedjklm_RI.xml",
//					40
//					)
//			);
//			list.add( 
//			new Tuple<String, Integer>(
//					"e:/work/GIST/ADIS/Balik06/2021-03-25 Test XML/v02.07.00_eForms/v02.07.00_CZ_CZ_test_maxc_20210326_generatedjklm_RN.xml",
//					41
//					)
//			);
//			list.add( 
//			new Tuple<String, Integer>(
//					"e:/work/GIST/ADIS/Balik06/2021-03-25 Test XML/v02.07.00_eForms/v02.07.00_CZ_CZ_test_maxc_20210326_generatedjklm_RR.xml",
//					42
//					)
//			);
//			list.add( 
//			new Tuple<String, Integer>(
//					"e:/work/GIST/ADIS/Balik06/2021-03-25 Test XML/v02.07.00_eForms/v02.07.00_CZ_CZ_test_minc_20210326_generatedjklm_RI.xml",
//					43
//					)
//			);
//			list.add( 
//			new Tuple<String, Integer>(
//					"e:/work/GIST/ADIS/Balik06/2021-03-25 Test XML/v02.07.00_eForms/v02.07.00_CZ_CZ_test_minc_20210326_generatedjklm_RN.xml",
//					44
//					)
//			);
//			list.add( 
//			new Tuple<String, Integer>(
//					"e:/work/GIST/ADIS/Balik06/2021-03-25 Test XML/v02.07.00_eForms/v02.07.00_CZ_CZ_test_minc_20210326_generatedjklm_RR.xml",
//					45
//					)
//			);
//			
//			list.add( 
//			new Tuple<String, Integer>(
//					"e:/work/GIST/ADIS/Balik06/2021-03-25 Test XML/v02.08.00_eForms/v02.08.00_CZ_CZ_test_maxc_20210326_generatedjklm_RI.xml",
//					50
//					)
//			);
//			list.add( 
//			new Tuple<String, Integer>(
//					"e:/work/GIST/ADIS/Balik06/2021-03-25 Test XML/v02.08.00_eForms/v02.08.00_CZ_CZ_test_maxc_20210326_generatedjklm_RN.xml",
//					51
//					)
//			);
//			list.add( 
//			new Tuple<String, Integer>(
//					"e:/work/GIST/ADIS/Balik06/2021-03-25 Test XML/v02.08.00_eForms/v02.08.00_CZ_CZ_test_maxc_20210326_generatedjklm_RR.xml",
//					52
//					)
//			);
			
//			list.add( 
//			new Tuple<String, Integer>(
//					"e:/work/GIST/ADIS/Balik06/2021-09-14 UpravyVerzi/V7/TestXmlFiles/v02.07.00_CZ_CZ_test_maxc_20210326_generatedjklm_RI.xml",
//					60
//					)
//			);
//			list.add( 
//			new Tuple<String, Integer>(
//					"e:/work/GIST/ADIS/Balik06/2021-09-14 UpravyVerzi/V7/TestXmlFiles/v02.07.00_CZ_CZ_test_maxc_20210326_generatedjklm_RN.xml",
//					61
//					)
//			);
//			list.add( 
//			new Tuple<String, Integer>(
//					"e:/work/GIST/ADIS/Balik06/2021-09-14 UpravyVerzi/V7/TestXmlFiles/v02.07.00_CZ_CZ_test_maxc_20210326_generatedjklm_RR.xml",
//					62
//					)
//			);
//			list.add( 
//			new Tuple<String, Integer>(
//					"e:/work/GIST/ADIS/Balik06/2021-09-14 UpravyVerzi/V8/TestXmlFiles/v02.08.00_CZ_CZ_test_maxc_20210326_generatedjklm_RI.xml",
//					70
//					)
//			);
//			list.add( 
//			new Tuple<String, Integer>(
//					"e:/work/GIST/ADIS/Balik06/2021-09-14 UpravyVerzi/V8/TestXmlFiles/v02.08.00_CZ_CZ_test_maxc_20210326_generatedjklm_RN.xml",
//					71
//					)
//			);
			list.add( 
			new Tuple<String, Integer>(
					"e:/work/GIST/ADIS/Balik06/2021-09-14 UpravyVerzi/V8/TestXmlFiles/v02.08.00_CZ_CZ_test_maxc_20210326_generatedjklm_RR.xml",
					72
					)
			);
//			list.add( 
//			new Tuple<String, Integer>(
//					"e:/work/GIST/ADIS/Balik06/2021-09-14 UpravyVerzi/V8/TestXmlFiles/test_RF.xml",
//					73
//					)
//			);
//			list.add( 
//			new Tuple<String, Integer>(
//					"e:/work/GIST/ADIS/Balik06/2021-09-14 UpravyVerzi/V8/TestXmlFiles/70_77_FollowUp_DataChanged__NaturalPerson.xml",
//					75
//					)
//			);
//			list.add( 
//			new Tuple<String, Integer>(
//					"e:/work/GIST/ADIS/Balik06/2021-09-14 UpravyVerzi/V8/TestXmlFiles/70_78_FollowUp_DataChanged__LegalEntity.xml",
//					76
//					)
//			);
//			list.add( 
//			new Tuple<String, Integer>(
//					"e:/work/GIST/ADIS/Balik06/2021-09-14 UpravyVerzi/V8/TestXmlFiles/70_79_LegalEntity_FollowUp_DataChanged__LegalEntity.xml",
//					77
//					)
//			);
			
			for(Tuple<String, Integer> item : list) {
				File xmlFile = new File(item.x);
				byte[] bytesPrilSoub = Helper.readFileToByteArray(xmlFile);

				PreparedStatement stmtExists = conn.prepareStatement("select 1 from t_x_xmlmpv where c_zadostmpv=?;");
				stmtExists.setInt(1, item.y);
				boolean exists = stmtExists.executeQuery().next();
				
				boolean result;
				PreparedStatement stmt;
				if (exists) {
					stmt = conn.prepareStatement("update t_x_xmlmpv set xmlsoub=? where c_zadostmpv=?;");
					stmt.setBytes(1, bytesPrilSoub);
					stmt.setInt(2, item.y);
					result = stmt.execute();
					
					System.out.println(String.format("Data %s UPDATována do tabulky s identifikátorem %s...", item.x, item.y));
				} else {
					stmt = conn.prepareStatement("insert into t_x_xmlmpv (c_zadostmpv, xmlsoub) values (?, ?);");
					stmt.setInt(1, item.y);
					stmt.setBytes(2, bytesPrilSoub);
					result = stmt.execute();
					
					System.out.println(String.format("Data %s INSERTována do tabulky s identifikátorem %s...", item.x, item.y));
				}
		
				conn.commit();
				
				stmt = conn.prepareStatement("select xmlsoub from t_x_xmlmpv where c_zadostmpv = ?;");
				stmt.setInt(1, item.y);
				ResultSet resultSet = stmt.executeQuery();
				
				byte[] byteArray = null;
				while(resultSet.next()) {
					byteArray = resultSet.getBytes("xmlsoub");
				}
		
				if (byteArray != null) {
					Helper.writeFile("e:/temp/byteArray.xml", byteArray);
				}
		
				
			}
		} 
		catch(Exception exc) {
			conn.rollback();
			throw exc;
		} finally {
			conn.close();
			dbConn.close();
		}
	}

}

