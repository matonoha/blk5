package project.helper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.UUID;


/**
 * @author valenta
 *
 *	ro generovani zaznamu terminateFlatRateRequest
 */
public class O_FillTXParOdDs_OdDuvukon_terminateFlatRateRequest {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		String typZazn = "O";
		String typSoub = "U";

		int cZaznamu = 61;
		int pocetZaznamuTXParOdDs = 10;
		int maxPocetZaznamuTXParOdDuvukon = 4; //0 > maxPocetZaznamuTXParOdDuvukon <= 9
		
		DbConnection dbConn = new DbConnection();
		dbConn.connect();
		Connection conn = dbConn.get_conn();
		
		try {
			
			if (maxPocetZaznamuTXParOdDuvukon < 1 || maxPocetZaznamuTXParOdDuvukon > 9) {
				maxPocetZaznamuTXParOdDuvukon = 1;
			}
			
			conn.setAutoCommit(false);
	
			PreparedStatement stmt = conn.prepareStatement("delete from t_x_par_od_ds where c_zaznamu = ?;");
			stmt.setInt(1, cZaznamu);
			stmt.execute();

			stmt = conn.prepareStatement("delete from t_x_par_od_duvukon where c_zaznamu = ?;");
			stmt.setInt(1, cZaznamu);
			stmt.execute();

			stmt = conn.prepareStatement("delete from t_x_par_oddo_xml where c_zaznamu = ?;");
			stmt.setInt(1, cZaznamu);
			stmt.execute();

			//TXParOdDoXml
			String TXParOdDoXmlInsert = 
					String.format(
							" insert into t_x_par_oddo_xml " +
							" (c_zaznamu, typ_zazn, typ_soub, d_vznik, c_soubor, correlation_id, c_fu)" +
							" values " +
							" (%s, '%s', '%s', current, %s, '%s', %s); ", 
							cZaznamu, //c_zaznamu
							typZazn, //typ_zazn
							typSoub, //typ_soub
							((int)(Math.random()*100000)), //c_soubor
							UUID.randomUUID().toString(), //correlation_id
							((int)(Math.random()*999)) //c_fu
							);
		
			stmt = conn.prepareStatement(TXParOdDoXmlInsert);
			stmt.execute();
			
			
			for(int i=1; i<=pocetZaznamuTXParOdDs; i++) {
	
				String messageIdFirstPart = String.format("%s%s", "00000000", i);
				messageIdFirstPart = messageIdFirstPart.substring(messageIdFirstPart.length()-8);
				String messageId = String.format("%s-8c14-471c-9701-f3f3ab532cc3", messageIdFirstPart);
				
				//TXParOdDs
				String TXParOdDsInsert = 
						String.format(
								" insert into t_x_par_od_ds " +
								" (c_zaznamu, message_id, aifo, rodne_cislo, c_poj, ecp, " +
								"  jmeno, prijmeni, rodnepr, d_naroz, ke_dni, " +
								"  id_zahajeni, id_zahajeni_o, id_zahajeni_orig, d_zahajeni, d_zahsvc, " +
								"  id_ukonceni, id_ukonceni_o, pref_cssz)" +
								" values " +
								" (%s, '%s', 'bafe64fcbfcfabfa1666666%s', '166666666%s', '160000000%s', '161111111%s', " +
								"  'Jan%s', 'Novák%s', 'Novák rodné%s', DATE('6/%s/1990'), DATE('5/%s/2021'), " + 
								"  null, null, %s, DATE('4/%s/2021'), null, " + 
								"  %s, null, '%s'); ", 
								cZaznamu, 
								messageId, 
								((int)(Math.random()*9)), //aifo
								((int)(Math.random()*9)), //rodne_cislo
								((int)(Math.random()*9)), //c_poj
								((int)(Math.random()*9)), //ecp
								i, //jmeno 
								i, //prijmeni
								i, //rodnepr
								((int)(Math.random()*11))+1, //d_naroz
								((int)(Math.random()*11))+1, //ke_dni
								((int)(Math.random()*999999999)), //id_zahajeni_orig
								((int)(Math.random()*11))+1, //d_zahajeni
								((int)(Math.random()*999999999)), //id_ukonceni
								(int)((Math.random()*2)) == 0 ? "rc" : "ecp" //pref_cssz
								);
	
				PreparedStatement stmtTXParOdDs = conn.prepareStatement(TXParOdDsInsert);
				stmtTXParOdDs.execute();

				System.out.println(String.format("Zaznam t_x_par_od_ds: %s::%s vložen...", cZaznamu, i));
				
				String duvodKon = "E";
				switch ((int)(Math.random()*4)) {
					case 0:
						duvodKon = "E";
						break;
					case 1:
						duvodKon = "F";
						break;
					case 2:
						duvodKon = "G";
						break;
					case 3:
						duvodKon = "S";
						break;
					default:
						break;
				}

				int maxPocetZaznamuTXParOdDuvukonConcrete = (int)(Math.random()*maxPocetZaznamuTXParOdDuvukon);
				if (maxPocetZaznamuTXParOdDuvukonConcrete < 1) {
					maxPocetZaznamuTXParOdDuvukonConcrete = 1;
				}
				
				for(int j=1; j<=maxPocetZaznamuTXParOdDuvukonConcrete; j++) {
					
					String duvod = ((Integer)j).toString();
					
					//TXParOdDuvukon
					String TXParOdDuvukonInsert = 
							String.format(
									" insert into t_x_par_od_duvukon " +
									" (c_zaznamu, message_id, duvod_kon, duvod) " +
									" values " +
									" (%s, '%s', '%s', '%s'); ",
									cZaznamu, messageId, duvodKon, duvod);
	
					PreparedStatement stmtTXParDoDuvukon = conn.prepareStatement(TXParOdDuvukonInsert);
					stmtTXParDoDuvukon.execute();
					
					System.out.println(String.format("	Zaznam t_x_par_od_duvukon: %s::%s::%s vložen...", cZaznamu, i, j));
					
				}

			}
			
			conn.commit();
				
			System.out.println(String.format(""));
			System.out.println(String.format("Data vložena do tabulek..."));
		} 
		catch(Exception exc) {
			conn.rollback();
			throw exc;
		} finally {
			conn.close();
			dbConn.close();
		}

	}

}
