package project.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;

public class Helper {

	public static byte[] readFileToByteArray(File file) {
		FileInputStream fis = null;
		// Creating a byte array using the length of the file
		// file.length returns long which is cast to int
		byte[] bArray = new byte[(int) file.length()];
		try {
			fis = new FileInputStream(file);
			fis.read(bArray);
			fis.close();

		} catch (IOException ioExp) {
			ioExp.printStackTrace();
		}
		return bArray;
	}
	
	public static String writeFile(String path, byte[] byteArray) {
		
		while (true) {
			File tempFile = new File(path);
			boolean exists = tempFile.exists();
			
			if (!exists) {
				break;
			}
			
			String parent = tempFile.getParent();
		    String name = tempFile.getName();
		    int lastIndexOf = name.lastIndexOf(".");
		    String extension = (lastIndexOf != -1) ? name.substring(lastIndexOf+1) : "";
		    String pureName = (lastIndexOf != -1) ? name.substring(0, name.length()-extension.length()-1) : name;
		    String rozlisovaciDodatek = convertFromDateToFormattedString(new java.util.Date());
			
			path = String.format("%s/%s_%s.%s", parent, pureName, rozlisovaciDodatek, extension);
		}
		
		try (FileOutputStream stream = new FileOutputStream(path)) {
		    stream.write(byteArray);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return path;
	}
	
	protected static String convertFromDateToFormattedString(java.util.Date d)
	{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
		return sdf.format(d).replaceAll("(\\d\\d)(\\d\\d)$", "$1:$2");
	}

}

