package project.helper;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class D_FillTXParOdDoXml_enteredFlatRateResponse {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		String typZazn = "D";
		String typSoub = "V";

		int cZaznamu = 31;
		String pathFileName = "e:/work/GIST/ADIS/Balik05/DatovyModel F9b/V01/TestXmlFiles/IDR-INT_IA_EnteredFlatRate-P2_Response_v1.6_help_50.xml";
		
		
		DbConnection dbConn = new DbConnection();
		dbConn.connect();
		Connection conn = dbConn.get_conn();
		
		try {
			conn.setAutoCommit(false);

			//mazani hodnot pro c_zaznamu v tabulce t_x_par_do_ds
			PreparedStatement stmt = conn.prepareStatement("delete from t_x_par_do_ds where c_zaznamu = ?;");
			stmt.setInt(1, cZaznamu);
			stmt.execute();

			//mazani hodnot pro c_zaznamu v tabulce t_x_par_od_do_xml
			stmt = conn.prepareStatement("delete from t_x_par_oddo_xml where c_zaznamu = ?;");
			stmt.setInt(1, cZaznamu);
			stmt.execute();

			//insert TXParOdDoXml
			String TXParOdDoXmlInsert = 
					String.format(
							" insert into t_x_par_oddo_xml " +
							" (c_zaznamu, typ_zazn, typ_soub, d_vznik, c_soubor, correlation_id, c_fu)" +
							" values " +
							" (%s, '%s', '%s', current, %s, '%s', %s); ", 
							cZaznamu, //c_zaznamu
							typZazn, //typ_zazn
							typSoub, //typ_soub
							((int)(Math.random()*100000)), //c_soubor
							null, //correlation_id
							((int)(Math.random()*999)) //c_fu
							);

			PreparedStatement stmtTXParOdDoXml = conn.prepareStatement(TXParOdDoXmlInsert);
			stmtTXParOdDoXml.execute();

			//nacteni file
			File xmlFile = new File(pathFileName);
			byte[] bytesPrilSoub = Helper.readFileToByteArray(xmlFile);
			
			//update TXParOdDoXml
			stmtTXParOdDoXml = conn.prepareStatement("update t_x_par_oddo_xml set xml_dosly=? where c_zaznamu = ?;");
			stmtTXParOdDoXml.setBytes(1, bytesPrilSoub);
			stmtTXParOdDoXml.setInt(2, cZaznamu);
			boolean result = stmtTXParOdDoXml.execute();
			
			conn.commit();

			System.out.println(String.format("Zaznam t_x_par_oddo_xml: %s vložen...", cZaznamu));
			
		} 
		catch(Exception exc) {
			conn.rollback();
			throw exc;
		} finally {
			conn.close();
			dbConn.close();
		}

	}

}
