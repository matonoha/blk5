package project.helper;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DownloadFileFromDbTable {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		class Tuple<X, Y> { 
			  public final X x; 
			  public final Y y; 
			  public Tuple(X x, Y y) { 
			    this.x = x; 
			    this.y = y; 
			  } 
			} 
		
		DbConnection dbConn = new DbConnection();
		dbConn.connect();
		Connection conn = dbConn.get_conn();

//		String fullpathFileName = String.format("e:/temp/downloadedfiles/%s", "testDownloadFile.xml");
//		String sourceColumn = "xml_odesl"; 
//		String sqlSelect = String.format("select %s from t_x_par_oddo_xml where c_zaznamu = %s;", sourceColumn, 43);

		String fullpathFileName = String.format("e:/work/GIST/ADIS/Balik06/2021-09-14 UpravyVerzi/V8/TestXmlFiles/%s", "72_RR.xml");
		String sourceColumn = "xmlsoub"; 
		String sqlSelect = String.format("select %s from t_x_xmlmpv where c_zadostmpv = %s;", sourceColumn, 72);

		try {
			conn.setAutoCommit(false);

			System.out.println(String.format("Connection-URL    : %s", conn.getMetaData().getURL()));
			System.out.println(String.format("Connection-Catalog: %s", conn.getCatalog()));
			System.out.println();

			PreparedStatement stmt = conn.prepareStatement(sqlSelect);
			ResultSet resultSet = stmt.executeQuery();
			
			byte[] byteArray = null;
			while(resultSet.next()) {
				byteArray = resultSet.getBytes(sourceColumn);
			}
	
			if (byteArray != null) {
				fullpathFileName = Helper.writeFile(fullpathFileName, byteArray);
				System.out.println(String.format("Data stažena a uložena do souboru %s...", fullpathFileName));
			} else {
				System.out.println(String.format("Obsah souboru je prázdný..."));
			}
				
		} 
		catch(Exception exc) {
			conn.rollback();
			throw exc;
		} finally {
			conn.close();
			dbConn.close();
		}

	}

}
