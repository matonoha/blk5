package project.junit;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TXParOddoXmlTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testTXParOddoXml() {
	    String expected = "Baeldung";
	    String actual = "Baeldung";

	    assertEquals(expected, actual);
	}

	@Test
	public final void testUpdateCorrelationId() {
	    char[] expected = {'J','u','n','i','t'};
	    char[] actual = "Junit".toCharArray();
	    
	    assertArrayEquals(expected, actual);
	}

	@Test
	public final void testSetCZaznamu() {
	    Object car = null;
	    
	    assertNull("The car should be null", car);
	}

	@Test
	public final void testGetCZaznamu() {
	    Object cat = new Object();
	    Object dog = new Object();

	    assertNotSame(cat, dog);
	}

}
