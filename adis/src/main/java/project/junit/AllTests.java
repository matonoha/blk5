package project.junit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TXParOddoXmlTest.class, TXParOdDsTest.class })
public class AllTests {

}
