/**
 * 
 */
package project.junit;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author valenta
 *
 */
public class TXParOdDsTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link adis.bnk.par.db.TXParOdDs#TXParOdDs(java.sql.Connection)}.
	 */
	@Test
	public final void testTXParOdDsConnection() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link adis.bnk.par.db.TXParOdDs#TXParOdDs(java.sql.Connection, java.lang.Integer, java.lang.String)}.
	 */
	@Test
	public final void testTXParOdDsConnectionIntegerString() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link adis.bnk.par.db.TXParOdDs#setCZaznamu(java.lang.Integer)}.
	 */
	@Test
	public final void testSetCZaznamu() {
	    assertTrue("6 is greater then 5", 5 < 6);
	    assertFalse("5 is not greater then 6", 5 > 6);	}

	/**
	 * Test method for {@link adis.bnk.par.db.TXParOdDs#getCZaznamu()}.
	 */
	@Test
	public final void testGetCZaznamu() {
	    try {
	        methodThatShouldThrowException(true);
	        fail("Exception not thrown");
	    } catch (UnsupportedOperationException e) {
	        assertEquals("Operation Not Supported", e.getMessage());
	    }
	}
	
	private void methodThatShouldThrowException(boolean throwException) throws UnsupportedOperationException {
		if (throwException) {
			throw new UnsupportedOperationException("Operation Not Supported");
		}
	}

}
