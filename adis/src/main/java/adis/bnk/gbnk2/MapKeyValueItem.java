package adis.bnk.gbnk2;

import java.util.Map.Entry;

public class MapKeyValueItem {
	
    public static <K, V> Entry<K, V> entry(K k, V v) {
        return new KeyValueItem<>(k, v);
    }
}
