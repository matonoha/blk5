/**
 * Interface
 */
package adis.bnk.gbnk2;

/**
 * @author Jiri Valenta
 */
public interface IExistsRecord {

	/**
	 * @return
	 */
	Boolean isExistsRecord();

}
