/**
 * Pomocna trida pro implementaci tridy NamedPreparedStatement
 */
package adis.bnk.gbnk2;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.NClob;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;

/**
 * @author DY
 */
class PreparedStatementImpl implements PreparedStatement {

	private final PreparedStatement preparedStatement;

	protected PreparedStatement getPreparedStatement() {
		return this.preparedStatement;
	}

	public PreparedStatementImpl(PreparedStatement preparedStatement) {
		this.preparedStatement = preparedStatement;
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
		return this.preparedStatement.unwrap(iface);
	}

	@Override
	public ResultSet executeQuery(String sql) throws SQLException {
		return this.preparedStatement.executeQuery(sql);
	}

	@Override
	public ResultSet executeQuery() throws SQLException {
		return this.preparedStatement.executeQuery();
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		return this.preparedStatement.isWrapperFor(iface);
	}

	@Override
	public int executeUpdate(String sql) throws SQLException {
		return this.preparedStatement.executeUpdate(sql);
	}

	@Override
	public int executeUpdate() throws SQLException {
		return this.preparedStatement.executeUpdate();
	}

	@Override
	public void setNull(int parameterIndex, int sqlType) throws SQLException {
		this.preparedStatement.setNull(parameterIndex, sqlType);
	}

	@Override
	public void close() throws SQLException {
		this.preparedStatement.close();
	}

	@Override
	public int getMaxFieldSize() throws SQLException {
		return this.preparedStatement.getMaxFieldSize();
	}

	@Override
	public void setBoolean(int parameterIndex, boolean x) throws SQLException {
		this.preparedStatement.setBoolean(parameterIndex, x);
	}

	@Override
	public void setByte(int parameterIndex, byte x) throws SQLException {
		this.preparedStatement.setByte(parameterIndex, x);
	}

	@Override
	public void setMaxFieldSize(int max) throws SQLException {
		this.preparedStatement.setMaxFieldSize(max);
	}

	@Override
	public void setShort(int parameterIndex, short x) throws SQLException {
		this.preparedStatement.setShort(parameterIndex, x);
	}

	@Override
	public int getMaxRows() throws SQLException {
		return this.preparedStatement.getMaxRows();
	}

	@Override
	public void setInt(int parameterIndex, int x) throws SQLException {
		this.preparedStatement.setInt(parameterIndex, x);
	}

	@Override
	public void setMaxRows(int max) throws SQLException {
		this.preparedStatement.setMaxRows(max);
	}

	@Override
	public void setLong(int parameterIndex, long x) throws SQLException {
		this.preparedStatement.setLong(parameterIndex, x);
	}

	@Override
	public void setEscapeProcessing(boolean enable) throws SQLException {
		this.preparedStatement.setEscapeProcessing(enable);
	}

	@Override
	public void setFloat(int parameterIndex, float x) throws SQLException {
		this.preparedStatement.setFloat(parameterIndex, x);
	}

	@Override
	public void setDouble(int parameterIndex, double x) throws SQLException {
		this.preparedStatement.setDouble(parameterIndex, x);
	}

	@Override
	public int getQueryTimeout() throws SQLException {
		return this.preparedStatement.getQueryTimeout();
	}

	@Override
	public void setQueryTimeout(int seconds) throws SQLException {
		this.preparedStatement.setQueryTimeout(seconds);
	}

	@Override
	public void setBigDecimal(int parameterIndex, BigDecimal x) throws SQLException {
		this.preparedStatement.setBigDecimal(parameterIndex, x);
	}

	@Override
	public void setString(int parameterIndex, String x) throws SQLException {
		this.preparedStatement.setString(parameterIndex, x);
	}

	@Override
	public void setBytes(int parameterIndex, byte[] x) throws SQLException {
		this.preparedStatement.setBytes(parameterIndex, x);
	}

	@Override
	public void cancel() throws SQLException {
		this.preparedStatement.cancel();
	}

	@Override
	public SQLWarning getWarnings() throws SQLException {
		return this.preparedStatement.getWarnings();
	}

	@Override
	public void setDate(int parameterIndex, Date x) throws SQLException {
		this.preparedStatement.setDate(parameterIndex, x);
	}

	@Override
	public void setTime(int parameterIndex, Time x) throws SQLException {
		this.preparedStatement.setTime(parameterIndex, x);
	}

	@Override
	public void clearWarnings() throws SQLException {
		this.preparedStatement.clearWarnings();
	}

	@Override
	public void setCursorName(String name) throws SQLException {
		this.preparedStatement.setCursorName(name);
	}

	@Override
	public void setTimestamp(int parameterIndex, Timestamp x) throws SQLException {
		this.preparedStatement.setTimestamp(parameterIndex, x);
	}

	@Override
	public void setAsciiStream(int parameterIndex, InputStream x, int length) throws SQLException {
		this.preparedStatement.setAsciiStream(parameterIndex, x, length);
	}

	@Override
	public boolean execute(String sql) throws SQLException {
		return this.preparedStatement.execute(sql);
	}

	@Deprecated
	@Override
	public void setUnicodeStream(int parameterIndex, InputStream x, int length)
			throws SQLException {
		this.preparedStatement.setUnicodeStream(parameterIndex, x, length);
	}

	@Override
	public ResultSet getResultSet() throws SQLException {
		return this.preparedStatement.getResultSet();
	}

	@Override
	public void setBinaryStream(int parameterIndex, InputStream x, int length) throws SQLException {
		this.preparedStatement.setBinaryStream(parameterIndex, x, length);
	}

	@Override
	public int getUpdateCount() throws SQLException {
		return this.preparedStatement.getUpdateCount();
	}

	@Override
	public boolean getMoreResults() throws SQLException {
		return this.preparedStatement.getMoreResults();
	}

	@Override
	public void clearParameters() throws SQLException {
		this.preparedStatement.clearParameters();
	}

	@Override
	public void setObject(int parameterIndex, Object x, int targetSqlType) throws SQLException {
		this.preparedStatement.setObject(parameterIndex, x, targetSqlType);
	}

	@Override
	public void setFetchDirection(int direction) throws SQLException {
		this.preparedStatement.setFetchDirection(direction);
	}

	@Override
	public int getFetchDirection() throws SQLException {
		return this.preparedStatement.getFetchDirection();
	}

	@Override
	public void setObject(int parameterIndex, Object x) throws SQLException {
		this.preparedStatement.setObject(parameterIndex, x);
	}

	@Override
	public void setFetchSize(int rows) throws SQLException {
		this.preparedStatement.setFetchSize(rows);
	}

	@Override
	public int getFetchSize() throws SQLException {
		return this.preparedStatement.getFetchSize();
	}

	@Override
	public int getResultSetConcurrency() throws SQLException {
		return this.preparedStatement.getResultSetConcurrency();
	}

	@Override
	public boolean execute() throws SQLException {
		return this.preparedStatement.execute();
	}

	@Override
	public int getResultSetType() throws SQLException {
		return this.preparedStatement.getResultSetType();
	}

	@Override
	public void addBatch(String sql) throws SQLException {
		this.preparedStatement.addBatch(sql);
	}

	@Override
	public void clearBatch() throws SQLException {
		this.preparedStatement.clearBatch();
	}

	@Override
	public void addBatch() throws SQLException {
		this.preparedStatement.addBatch();
	}

	@Override
	public int[] executeBatch() throws SQLException {
		return this.preparedStatement.executeBatch();
	}

	@Override
	public void setCharacterStream(int parameterIndex, Reader reader, int length)
			throws SQLException {
		this.preparedStatement.setCharacterStream(parameterIndex, reader, length);
	}

	@Override
	public void setRef(int parameterIndex, Ref x) throws SQLException {
		this.preparedStatement.setRef(parameterIndex, x);
	}

	@Override
	public void setBlob(int parameterIndex, Blob x) throws SQLException {
		this.preparedStatement.setBlob(parameterIndex, x);
	}

	@Override
	public void setClob(int parameterIndex, Clob x) throws SQLException {
		this.preparedStatement.setClob(parameterIndex, x);
	}

	@Override
	public Connection getConnection() throws SQLException {
		return this.preparedStatement.getConnection();
	}

	@Override
	public void setArray(int parameterIndex, Array x) throws SQLException {
		this.preparedStatement.setArray(parameterIndex, x);
	}

	@Override
	public ResultSetMetaData getMetaData() throws SQLException {
		return this.preparedStatement.getMetaData();
	}

	@Override
	public boolean getMoreResults(int current) throws SQLException {
		return this.preparedStatement.getMoreResults(current);
	}

	@Override
	public void setDate(int parameterIndex, Date x, Calendar cal) throws SQLException {
		this.preparedStatement.setDate(parameterIndex, x, cal);
	}

	@Override
	public ResultSet getGeneratedKeys() throws SQLException {
		return this.preparedStatement.getGeneratedKeys();
	}

	@Override
	public void setTime(int parameterIndex, Time x, Calendar cal) throws SQLException {
		this.preparedStatement.setTime(parameterIndex, x, cal);
	}

	@Override
	public int executeUpdate(String sql, int autoGeneratedKeys) throws SQLException {
		return this.preparedStatement.executeUpdate(sql, autoGeneratedKeys);
	}

	@Override
	public void setTimestamp(int parameterIndex, Timestamp x, Calendar cal) throws SQLException {
		this.preparedStatement.setTimestamp(parameterIndex, x, cal);
	}

	@Override
	public void setNull(int parameterIndex, int sqlType, String typeName) throws SQLException {
		this.preparedStatement.setNull(parameterIndex, sqlType, typeName);
	}

	@Override
	public int executeUpdate(String sql, int[] columnIndexes) throws SQLException {
		return this.preparedStatement.executeUpdate(sql, columnIndexes);
	}

	@Override
	public void setURL(int parameterIndex, URL x) throws SQLException {
		this.preparedStatement.setURL(parameterIndex, x);
	}

	@Override
	public int executeUpdate(String sql, String[] columnNames) throws SQLException {
		return this.preparedStatement.executeUpdate(sql, columnNames);
	}

	@Override
	public ParameterMetaData getParameterMetaData() throws SQLException {
		return this.preparedStatement.getParameterMetaData();
	}

	@Override
	public void setRowId(int parameterIndex, RowId x) throws SQLException {
		this.preparedStatement.setRowId(parameterIndex, x);
	}

	@Override
	public void setNString(int parameterIndex, String value) throws SQLException {
		this.preparedStatement.setNString(parameterIndex, value);
	}

	@Override
	public boolean execute(String sql, int autoGeneratedKeys) throws SQLException {
		return this.preparedStatement.execute(sql, autoGeneratedKeys);
	}

	@Override
	public void setNCharacterStream(int parameterIndex, Reader value, long length)
			throws SQLException {
		this.preparedStatement.setNCharacterStream(parameterIndex, value, length);
	}

	@Override
	public void setNClob(int parameterIndex, NClob value) throws SQLException {
		this.preparedStatement.setNClob(parameterIndex, value);
	}

	@Override
	public void setClob(int parameterIndex, Reader reader, long length) throws SQLException {
		this.preparedStatement.setClob(parameterIndex, reader, length);
	}

	@Override
	public boolean execute(String sql, int[] columnIndexes) throws SQLException {
		return this.preparedStatement.execute(sql, columnIndexes);
	}

	@Override
	public void setBlob(int parameterIndex, InputStream inputStream, long length)
			throws SQLException {
		this.preparedStatement.setBlob(parameterIndex, inputStream, length);
	}

	@Override
	public void setNClob(int parameterIndex, Reader reader, long length) throws SQLException {
		this.preparedStatement.setNClob(parameterIndex, reader, length);
	}

	@Override
	public boolean execute(String sql, String[] columnNames) throws SQLException {
		return this.preparedStatement.execute(sql, columnNames);
	}

	@Override
	public void setSQLXML(int parameterIndex, SQLXML xmlObject) throws SQLException {
		this.preparedStatement.setSQLXML(parameterIndex, xmlObject);
	}

	@Override
	public void setObject(int parameterIndex, Object x, int targetSqlType, int scaleOrLength)
			throws SQLException {
		this.preparedStatement.setObject(parameterIndex, x, targetSqlType, scaleOrLength);
	}

	@Override
	public int getResultSetHoldability() throws SQLException {
		return this.preparedStatement.getResultSetHoldability();
	}

	@Override
	public boolean isClosed() throws SQLException {
		return this.preparedStatement.isClosed();
	}

	@Override
	public void setPoolable(boolean poolable) throws SQLException {
		this.preparedStatement.setPoolable(poolable);
	}

	@Override
	public boolean isPoolable() throws SQLException {
		return this.preparedStatement.isPoolable();
	}

	@Override
	public void closeOnCompletion() throws SQLException {
		this.preparedStatement.closeOnCompletion();
	}

	@Override
	public void setAsciiStream(int parameterIndex, InputStream x, long length) throws SQLException {
		this.preparedStatement.setAsciiStream(parameterIndex, x, length);
	}

	@Override
	public boolean isCloseOnCompletion() throws SQLException {
		return this.preparedStatement.isCloseOnCompletion();
	}

	@Override
	public void setBinaryStream(int parameterIndex, InputStream x, long length)
			throws SQLException {
		this.preparedStatement.setBinaryStream(parameterIndex, x, length);
	}

	@Override
	public void setCharacterStream(int parameterIndex, Reader reader, long length)
			throws SQLException {
		this.preparedStatement.setCharacterStream(parameterIndex, reader, length);
	}

	@Override
	public void setAsciiStream(int parameterIndex, InputStream x) throws SQLException {
		this.preparedStatement.setAsciiStream(parameterIndex, x);
	}

	@Override
	public void setBinaryStream(int parameterIndex, InputStream x) throws SQLException {
		this.preparedStatement.setBinaryStream(parameterIndex, x);
	}

	@Override
	public void setCharacterStream(int parameterIndex, Reader reader) throws SQLException {
		this.preparedStatement.setCharacterStream(parameterIndex, reader);
	}

	@Override
	public void setNCharacterStream(int parameterIndex, Reader value) throws SQLException {
		this.preparedStatement.setNCharacterStream(parameterIndex, value);
	}

	@Override
	public void setClob(int parameterIndex, Reader reader) throws SQLException {
		this.preparedStatement.setClob(parameterIndex, reader);
	}

	@Override
	public void setBlob(int parameterIndex, InputStream inputStream) throws SQLException {
		this.preparedStatement.setBlob(parameterIndex, inputStream);
	}

	@Override
	public void setNClob(int parameterIndex, Reader reader) throws SQLException {
		this.preparedStatement.setNClob(parameterIndex, reader);
	}

}