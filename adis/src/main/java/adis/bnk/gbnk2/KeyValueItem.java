package adis.bnk.gbnk2;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

public final class KeyValueItem<V, K> implements Entry<K, V> {

    final K key;
    final V value;

    KeyValueItem(K k, V v) {
        key = Objects.requireNonNull(k);
        value = Objects.requireNonNull(v);
    }

	@Override
	public K getKey() {
		return this.key;
	}

	@Override
	public V getValue() {
		return this.value;
	}

	@Override
	public V setValue(V value) {
		throw new UnsupportedOperationException("not supported");
	}

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Map.Entry))
            return false;
        Map.Entry<?,?> e = (Map.Entry<?,?>)o;
        return key.equals(e.getKey()) && value.equals(e.getValue());
    }

    @Override
    public int hashCode() {
        return key.hashCode() ^ value.hashCode();
    }

    @Override
    public String toString() {
        return String.format("%s=%s", key, value);
    }
}
