package adis.bnk.gbnk;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import project.helper.DbConnection;

public class sDavky {
	private final static String _url = "http://localhost:44198?file=";
	private static TransformerFactory _transFactory = null;
	private static DbConnection _conn = null;
	private static String[] files = {
			"file1.xml", 
			"file2.xml", 
			"file3.xml"
	};

	public static void main(String[] args) {
		_transFactory = TransformerFactory.newInstance();
		_conn = new DbConnection();
		_conn.connect();
		
		for (String file : files) {
			InputStream input = getInputStreamXML(_url + file);
			// TODO: prace se soubory
			// unmarshal s JAXB
			
		}
		
		_conn.close();
	}
	
	// helpers
	private static InputStream getInputStreamXML(String url) {
		try {
			Document xml = getDocumentFromUrl(url);
			return getInputStream(xml);
		} catch (Exception e) {
			System.out.println("Could not get document from URL: " + url + " - " + e);
		}
		return null;
	}
	
	private static InputStream getInputStream(Document document) {
		ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();
		Source xmlSource = new DOMSource(document);
		Result result = new StreamResult(byteOutputStream);
		
		try {
			Transformer transformer = _transFactory.newTransformer();
			transformer.transform(xmlSource, result);
			return new ByteArrayInputStream(byteOutputStream.toByteArray());
		} catch (TransformerException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	private static Document getDocumentFromUrl(String url) throws IOException, ParserConfigurationException, SAXException {
		URL link = new URL(url);
		HttpURLConnection connection = (HttpURLConnection) link.openConnection();
		connection.setRequestMethod("GET");
		connection.setRequestProperty("Accept", "application/xml");

		InputStream xml = connection.getInputStream();

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(xml);
		connection.disconnect();
		
		return doc;

	}

}
