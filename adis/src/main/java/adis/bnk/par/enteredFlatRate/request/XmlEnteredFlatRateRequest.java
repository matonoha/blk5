package adis.bnk.par.enteredFlatRate.request;

import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;

import org.xml.sax.SAXException;

import adis.bnk.par.XmlBase;
import adis.bnk.par.db.TXParOdDs;
import adis.bnk.par.db.TXParOdDsCollection;
import adis.bnk.par.db.TXParOddoXml;
import adis.bnk.par.enteredFlatRate.request.EnteredFlatRate.Subjekty.Subjekt;

/**
 * @author matonoha
 */
public class XmlEnteredFlatRateRequest extends XmlBase<EnteredFlatRate> {
	private int cZaznamu;
	private TXParOddoXml _tXParOddoXml;
	private ObjectFactory _objectFactory = new ObjectFactory();
	
	public XmlEnteredFlatRateRequest(Connection conn, int cZaznamu) {
		super(conn);
		this.cZaznamu = cZaznamu;
		
		this.set_xsdSchemaURL(this.getClass().getResource(
				"/adis/bnk/par/schema/IDR-INT_IA_EnteredFlatRate-P1_Request_v1-9.xsd"));
	}
	
	/**
	 * @return
	 * @throws SQLException
	 * @throws DatatypeConfigurationException
	 */
	public EnteredFlatRate getRequest() throws SQLException, DatatypeConfigurationException {
		this._tXParOddoXml = new TXParOddoXml(this._conn, this.cZaznamu);
		this._tXParOddoXml.setPropertiesForCZaznamu();
		
		TXParOdDsCollection tXParOdDsCollection = new TXParOdDsCollection(this._conn);
		tXParOdDsCollection.fillForCZaznamu(this._tXParOddoXml.getCZaznamu());
		
		List<Subjekt> listSubjekt = new ArrayList<>();
		for (TXParOdDs item : tXParOdDsCollection) {
			//<enteredFlatRate><Subjekty><Subjekt>
			Subjekt subjekt = this._objectFactory.createEnteredFlatRateSubjektySubjekt();

			subjekt.setMessageId(item.getMessageId());
			subjekt.setAifo(item.getAifo());
			subjekt.setRodneCislo(item.getRodneCislo());
			subjekt.setCisloPojistence(item.getCPoj());
			subjekt.setEvidencniCisloPojistence(item.getEcp());
			subjekt.setPreferovanyIdentifikatorCSSZ(item.getPrefCssz());
			subjekt.setZahajeniOsvcpp(this.createZahajeniOsvcpp(item));
			listSubjekt.add(subjekt);
		}
		
		//<enteredFlatRate><Subjekty>
		EnteredFlatRate.Subjekty subjekty = this._objectFactory.createEnteredFlatRateSubjekty();
		subjekty.subjekt = listSubjekt;

		//<enteredFlatRate>
		EnteredFlatRate flatRate = this._objectFactory.createEnteredFlatRate();
		flatRate.setCorrelationId(this._tXParOddoXml.getCorrelationId());
		flatRate.subjekty = subjekty;
		
		return flatRate;
	}
	
	/**
	 * @param item
	 * @return
	 * @throws DatatypeConfigurationException
	 */
	public Subjekt.ZahajeniOsvcpp createZahajeniOsvcpp(TXParOdDs item) 
			throws DatatypeConfigurationException {

		//povinne polozky v ramci nepovinneho node <ZahajeniOsvcpp>
		if (item.getIdZahajeni() == null ||
				item.getJmeno() == null ||
				item.getPrijmeni() == null ||
				item.getDNaroz() == null ||
				item.getKeDni() == null) {
			return null;
		}

		Subjekt.ZahajeniOsvcpp zahajeniOsvcpp = this._objectFactory.createEnteredFlatRateSubjektySubjektZahajeniOsvcpp();
		
		zahajeniOsvcpp.setIdZahajeni(item.getIdZahajeni().toString());

		if(item.getIdZahajeniOrig() != null) {
			zahajeniOsvcpp.setIdZahajeniOriginal(item.getIdZahajeniOrig().toString());
		}
		
		if(item.getIdZahajeniO() != null) {
			zahajeniOsvcpp.setIdZahajeniPosledniOprava(item.getIdZahajeniO().toString());
		}
		
		if(item.getDZahsvc() != null) {
			zahajeniOsvcpp.setDatumZahajeniSvcOd(this.convertFromDateToDate(item.getDZahsvc()));
		}

		zahajeniOsvcpp.setJmeno(item.getJmeno());

		zahajeniOsvcpp.setPrijmeni(item.getPrijmeni());

		zahajeniOsvcpp.setRodnePrijmeni(item.getRodnepr());
		
		zahajeniOsvcpp.setDatumNarozeni(this.convertFromDateToDate(item.getDNaroz()));

		if(item.getKeDni() != null) {
			zahajeniOsvcpp.setOSVCPPod(this.convertFromDateToDate(item.getKeDni()));
		}
		
		return zahajeniOsvcpp;
	}

	/**
	 * @return
	 * @throws SQLException
	 * @throws DatatypeConfigurationException
	 * @throws JAXBException
	 * @throws SAXException
	 */
	public boolean createOrUpdateXmlToDb() 
			throws SQLException, DatatypeConfigurationException, JAXBException, SAXException {
		EnteredFlatRate flatRate = this.getRequest();
		ByteArrayOutputStream baos = this.convertObjectToXmlOutputStream(flatRate);
		
		//Helper.writeFile("./target/pokus2.xml", baos.toByteArray());
		
		return this._tXParOddoXml.updateXmlOdesl(baos);
	}
}
