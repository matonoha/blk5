package adis.bnk.par.enteredFlatRate.response;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.xml.bind.JAXBException;

import org.xml.sax.SAXException;

import adis.bnk.par.XmlBase;
import adis.bnk.par.db.TXParOdDs;
import adis.bnk.par.db.TXParOddoXml;
import adis.bnk.par.db.TXParDoDs.VysledekZpracovaniEnum;
import adis.bnk.par.enteredFlatRate.request.EnteredFlatRate;
import adis.bnk.par.enteredFlatRate.response.EnteredFlatRateResponse.Subjekty.Subjekt;

/**
 * @author matonoha
 *
 */
public class HelperXmlEnteredFlatRateResponse extends XmlBase<EnteredFlatRateResponse> {

	private ObjectFactory _objectFactory = new ObjectFactory();
	private final String version = "v1.6_";


	public HelperXmlEnteredFlatRateResponse(Connection conn) {
		super(conn);
		
		this.set_xsdSchemaURL(this.getClass().getResource("/adis/bnk/par/schema/IDR-INT_IA_EnteredFlatRate-P2_Response_v1.6.xsd"));
	}

	/**
	 * @return
	 * @throws SQLException
	 * @throws JAXBException
	 * @throws SAXException
	 */
	
	public EnteredFlatRateResponse getTestResponse( int pocetSubjektu) {
		
		List<Subjekt> listSubjekt = new ArrayList<Subjekt>();
		for (int sub = 1; sub <=  pocetSubjektu; sub++) {
			//<enteredFlatRate><Subjekty><Subjekt>
			Subjekt subjekt = this._objectFactory.createEnteredFlatRateResponseSubjektySubjekt();

			subjekt.setMessageId(UUID.randomUUID().toString());
			
			String vysledekZpracovani = VysledekZpracovaniEnum.values()[(int)(Math.random()*4)].getHodnota();
			subjekt.setVysledekZpracovani(vysledekZpracovani);
			listSubjekt.add(subjekt);
		}
		
		//<enteredFlatRate><Subjekty>
		EnteredFlatRateResponse.Subjekty subjekty = this._objectFactory.createEnteredFlatRateResponseSubjekty();
		subjekty.subjekt = listSubjekt;

		//<enteredFlatRate>
		EnteredFlatRateResponse flatRateResponse = this._objectFactory.createEnteredFlatRateResponse();
		flatRateResponse.setCorrelationId(UUID.randomUUID().toString());
		flatRateResponse.subjekty = subjekty;
		
		return flatRateResponse;
	}
	
}
