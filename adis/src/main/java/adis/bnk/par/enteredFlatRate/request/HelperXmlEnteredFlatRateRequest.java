package adis.bnk.par.enteredFlatRate.request;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;

import org.xml.sax.SAXException;

import adis.bnk.par.XmlBase;
import adis.bnk.par.db.TXParOdDs;
import adis.bnk.par.db.TXParOddoXml;
import adis.bnk.par.db.TXParDoDs.VysledekZpracovaniEnum;
import adis.bnk.par.enteredFlatRate.request.EnteredFlatRate;
import adis.bnk.par.enteredFlatRate.request.EnteredFlatRate.Subjekty.Subjekt;
import adis.bnk.par.enteredFlatRate.request.EnteredFlatRate.Subjekty.Subjekt.ZahajeniOsvcpp;

/**
 * @author matonoha
 *
 */
public class HelperXmlEnteredFlatRateRequest extends XmlBase<EnteredFlatRate> {

	private ObjectFactory _objectFactory = new ObjectFactory();
	private final String version = "v1.7_";


	public HelperXmlEnteredFlatRateRequest(Connection conn) {
		super(conn);
		
		this.set_xsdSchemaURL(this.getClass().getResource("/adis/bnk/par/schema/IDR-INT_IA_EnteredFlatRate-P1_Request_v1.7.xsd"));
	}

	/**
	 * @return
	 * @throws DatatypeConfigurationException 
	 * @throws SQLException
	 * @throws JAXBException
	 * @throws SAXException
	 */
	
	public EnteredFlatRate getTestRequest(int pocetSubjektu) throws DatatypeConfigurationException {
		
		List<Subjekt> listSubjekt = new ArrayList<Subjekt>();
		for (int sub = 1; sub <=  pocetSubjektu; sub++) {
			//<enteredFlatRate><Subjekty><Subjekt>
			Subjekt subjekt = this._objectFactory.createEnteredFlatRateSubjektySubjekt();

			subjekt.setMessageId(UUID.randomUUID().toString());
			
			String aifo = String.format("aaaaaaaaaaaaaaaaaaaaaaaa%s", sub);
			subjekt.setAifo(aifo.substring(aifo.length() - 24));
			
			String rodneCislo = String.format("0000000000%s", sub); 
			subjekt.setRodneCislo(rodneCislo.substring(rodneCislo.length() - 10));
			
			String cisloPojistence = String.format("%s0000000000", sub);
			subjekt.setCisloPojistence(cisloPojistence.substring(0,10));
			
			String evidencniCisloPojistence = String.format("0%s000000000", sub);
			subjekt.setEvidencniCisloPojistence(evidencniCisloPojistence.substring(0,10));

			switch ((int)(Math.random()*2)) {
				case 0:
					subjekt.setPreferovanyIdentifikatorCSSZ("rc");;
					break;
				case 1:
					subjekt.setPreferovanyIdentifikatorCSSZ("ecp");;
					break;
				default:
					break;
			}

			switch ((int)(Math.random()*2)) {
				case 0:
					ZahajeniOsvcpp zahajeniOsvcpp = this._objectFactory.createEnteredFlatRateSubjektySubjektZahajeniOsvcpp();

					Long idZahajeni = (long)(Math.random()*999999999999999d);
					zahajeniOsvcpp.setIdZahajeni(idZahajeni.toString());

					switch ((int)(Math.random()*2)) {
						case 0:
							Long idZahajeniOriginal = (long)(Math.random()*999999999999999d);
							zahajeniOsvcpp.setIdZahajeniOriginal(idZahajeniOriginal.toString());
							
							break;
						case 1:
						default:
							break;
					}
					
					switch ((int)(Math.random()*2)) {
						case 0:
							Long idZahajeniPosledniOprava = (long)(Math.random()*999999999999999d);
							zahajeniOsvcpp.setIdZahajeniPosledniOprava(idZahajeniPosledniOprava.toString());
							
							break;
						case 1:
						default:
							break;
					}
					
					switch ((int)(Math.random()*2)) {
						case 0:
							zahajeniOsvcpp.setDatumZahajeniSvcOd(this.convertFromDateToDate(new java.util.Date()));
							
							break;
						case 1:
						default:
							break;
					}
					
					
					zahajeniOsvcpp.setJmeno(String.format("%sJméno%s", version, sub));
					
					zahajeniOsvcpp.setPrijmeni(String.format("%sPříjmení%s", version, sub));
					
					switch ((int)(Math.random()*2)) {
						case 0:
							zahajeniOsvcpp.setRodnePrijmeni(String.format("%sRodnéPříjmení%s", version, sub));
							break;
						case 1:
						default:
							break;
					}

					zahajeniOsvcpp.setDatumNarozeni(this.convertFromDateToDate(new java.util.Date()));

					zahajeniOsvcpp.setOSVCPPod(this.convertFromDateToDate(new java.util.Date()));
					
					subjekt.setZahajeniOsvcpp(zahajeniOsvcpp);
					
					break;
				case 1:
				default:
					break;
			}
			
			listSubjekt.add(subjekt);
		}
		
		//<enteredFlatRate><Subjekty>
		EnteredFlatRate.Subjekty subjekty = this._objectFactory.createEnteredFlatRateSubjekty();
		subjekty.subjekt = listSubjekt;

		//<enteredFlatRate>
		EnteredFlatRate flatRateRequest = this._objectFactory.createEnteredFlatRate();
		flatRateRequest.setCorrelationId(UUID.randomUUID().toString());
		flatRateRequest.subjekty = subjekty;
		
		return flatRateRequest;
	}
	
}
