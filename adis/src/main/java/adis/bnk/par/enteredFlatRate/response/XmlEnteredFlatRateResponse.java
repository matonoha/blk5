			package adis.bnk.par.enteredFlatRate.response;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.xml.bind.JAXBException;

import org.xml.sax.SAXException;

import adis.bnk.par.XmlBase;
import adis.bnk.par.db.TXParDoDs;
import adis.bnk.par.db.TXParOddoXml;
import adis.bnk.par.db.TpParTXParDoDs;
import adis.bnk.par.db.TXParDoDs.VysledekZpracovaniEnum;

/**
 * @author matonoha
 *
 */
public class XmlEnteredFlatRateResponse extends XmlBase<EnteredFlatRateResponse> {
	private int _cZaznamu;
	private ObjectFactory _objectFactory = new ObjectFactory();
	private TXParOddoXml _tXParOddoXml;

	public XmlEnteredFlatRateResponse(Connection conn, int cZaznamu) {
		super(conn);
		this._cZaznamu = cZaznamu;
		
		this.set_xsdSchemaURL(this.getClass().getResource("/adis/bnk/par/schema/IDR-INT_IA_EnteredFlatRate-P2_Response_v1.6.xsd"));
	}

	/**
	 * @return
	 * @throws Exception
	 */
	public boolean createOrUpdateXmlToDb() throws Exception {
		boolean result = false;

		long startTime;
		long elapsedTime;

//		startTime = System.nanoTime();
//		result = this.processResponse();
//		elapsedTime = System.nanoTime() - startTime;
//		System.out.println(String.format("By statement. Total execution time in millis: %s", elapsedTime/1000000));

		startTime = System.nanoTime();
		result = this.processResponseDeleteInsertBatch();
		elapsedTime = System.nanoTime() - startTime;
		System.out.println(String.format("By deleteInsertBatch. Total execution time in millis: %s", elapsedTime/1000000));
		
		return result;
	}
	
	/**
	 * @return
	 * @throws SQLException
	 * @throws JAXBException
	 * @throws SAXException
	 */
	public EnteredFlatRateResponse getResponse() throws SQLException, JAXBException, SAXException {
		_tXParOddoXml = new TXParOddoXml(_conn, _cZaznamu);
		_tXParOddoXml.setPropertiesForCZaznamu();
		if (_tXParOddoXml.getXmlDosly() == null) {
			throw new NullPointerException("xml_dosly");
		}
		
		Blob blob = _tXParOddoXml.getXmlDosly();
		int length = (int)blob.length();
		byte[] blobBytes = blob.getBytes(1, length);
		//blob.free();
		
		//Helper.writeFile("./target/pokus1.xml", blobBytes);
		
		EnteredFlatRateResponse response = this._objectFactory.createEnteredFlatRateResponse();
		response = this.convertBytesToObject(blobBytes, response);
		return response;
	}
	
	public boolean processResponse() throws Exception {
		boolean result = false;
		EnteredFlatRateResponse flatRateResponse = this.getResponse();

		int cZaznamu = this._tXParOddoXml.getCZaznamu();
		this._tXParOddoXml.updateCorrelationId(flatRateResponse.getCorrelationId());
		
		for(EnteredFlatRateResponse.Subjekty.Subjekt subjekt : flatRateResponse.getSubjekty().getSubjekt()) {
			TXParDoDs tXParDoDs = new TXParDoDs(this._conn, cZaznamu, subjekt);
			result = tXParDoDs.insertOrUpdate();
		}
		
		return result;
	}	
	
	private boolean processResponseDeleteInsertBatch() throws Exception {

		EnteredFlatRateResponse flatRateResponse = this.getResponse();

		boolean result = false;
		boolean resultAll = true;

		int cZaznamu = this._tXParOddoXml.getCZaznamu();
		this._tXParOddoXml.updateCorrelationId(flatRateResponse.getCorrelationId());
		
//		long startTime;
//		long elapsedTime;
		
		int pocetZaznamuVDavceTXParDoDs = 1;
		int maxPocetZaznamuVDavceTXParDoDs = 10000;

		ArrayList<TpParTXParDoDs> dataDoDs = new ArrayList<TpParTXParDoDs>();
		
		for(EnteredFlatRateResponse.Subjekty.Subjekt subjekt : flatRateResponse.getSubjekty().getSubjekt()) {
//			TXParDoDs tXParDoDs = new TXParDoDs(this._conn, cZaznamu, subjekt);
//			result = tXParDoDs.insertOrUpdate();
			
	    	dataDoDs.add(
					new TpParTXParDoDs(
							cZaznamu, 
							subjekt.getMessageId(), 
					    	subjekt.getVysledekZpracovani() != null
					    		? VysledekZpracovaniEnum.getByHodnota(subjekt.getVysledekZpracovani()).toString()
					    		: null));
			pocetZaznamuVDavceTXParDoDs++;

			if(pocetZaznamuVDavceTXParDoDs > maxPocetZaznamuVDavceTXParDoDs) {
//				startTime = System.nanoTime();
				result = TXParDoDs.insertOrUpdateByDeleteInsertBatch(this._conn, dataDoDs);
//				elapsedTime = System.nanoTime() - startTime;
//				System.out.println(String.format("1. Processed %s records. Total execution time in millis: %s", data.size(), elapsedTime/1000000));
				
				dataDoDs.clear();
				pocetZaznamuVDavceTXParDoDs = 1;
				
				if (!result) {
					resultAll = result;
				}
			}
		}
		
		if (dataDoDs.size() > 0) {
//			startTime = System.nanoTime();
			result = TXParDoDs.insertOrUpdateByDeleteInsertBatch(this._conn, dataDoDs);
//			elapsedTime = System.nanoTime() - startTime;
//			System.out.println(String.format("2. Processed %s records. Total execution time in millis: %s", data.size(), elapsedTime/1000000));

			if (!result) {
				resultAll = result;
			}
		}
		
		return resultAll;
		
	}
	
}
