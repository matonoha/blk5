package adis.bnk.par.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import adis.bnk.gbnk2.NamedPreparedStatement;

public class TXParDoZcCollection extends TXParCollectionBase<TXParOdDs> {

	public TXParDoZcCollection(Connection conn) {
		super(conn);
		
        this.set_sqlSelect(
        		String.join("", 
        				" select ",
        				"	c_zaznamu ",
        				"	,message_id ",
        				"	,c_por ",
        				"	,zam_od ",
        				"	,zam_do ",
        				"	,d_podprihl ",
        				"	,d_pododhl ",
        				"	,t_pracpom ",
        				"	,zamestnavatel ",
        				"	,ico ",
        				"	,adresa ",
        				" from ",
        				"	t_x_par_do_zc "));

        this.set_sqlDelete(
				String.join("",
						" delete from t_x_par_do_zc "));
	}
	
	public boolean deleteForCZaznamuAndMessageId(int cZaznamu, String messageId) throws SQLException {

		NamedPreparedStatement stmt = NamedPreparedStatement.prepareStatement(this._conn,
				String.join("", 
						this.get_sqlDelete(),
						" where ",
						"	c_zaznamu = :c_zaznamu",
						"	and message_id = :message_id; "));
		stmt.setInt("c_zaznamu", cZaznamu);
		stmt.setString("message_id", messageId);
    	
		boolean result = stmt.execute();
		stmt.close();
		
		return result;
		
	}

}
