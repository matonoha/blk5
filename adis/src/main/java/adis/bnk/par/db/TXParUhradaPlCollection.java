package adis.bnk.par.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import adis.bnk.gbnk2.NamedPreparedStatement;

public class TXParUhradaPlCollection extends TXParCollectionBase<TXParUhradaPl> {

	public TXParUhradaPlCollection(Connection conn) {
		super(conn);
		
        this.set_sqlSelect(
        		String.join("", 
        				" select ",
        				"	c_zaznamu ",
        				"	,message_id ",
        				"	,c_uhrady ",
        				"	,c_puv_uhrady ",
        				"	,typ_uhrady ",
        				"	,obdobi ",
        				"	,c_popl ",
        				"	,n_titul ",
        				"	,d_uhrady ",
        				"	,kc_uhrady ",        				
        				"	,k_zprac_cssz ",        				
        				"	,p_zprac_cssz ",        				
        				" from ",
        				"	t_x_par_uhrada_pl "));
        
	}
	
	public void fillForCZaznamu(int cZaznamu) throws SQLException {

		NamedPreparedStatement stmt = NamedPreparedStatement.prepareStatement(this._conn,
				String.join("", 
						this.get_sqlSelect(),
						" where ",
						"	c_zaznamu=:c_zaznamu;"));
		stmt.setInt("c_zaznamu", cZaznamu);
    	
		ResultSet resultSet = stmt.executeQuery();
    	while (resultSet.next()) {
    		TXParUhradaPl item = new TXParUhradaPl(_conn);
    		item.setProperties(resultSet);
    		this.add(item);
    	}
		stmt.close();
	}

	public void fillForCZaznamuAndMessageId(int cZaznamu, String messageId) throws SQLException {

		NamedPreparedStatement stmt = NamedPreparedStatement.prepareStatement(this._conn,
				String.join("", 
						this.get_sqlSelect(),
						" where ",
						"	c_zaznamu = :c_zaznamu",
						"	and message_id = :message_id; "));
		stmt.setInt("c_zaznamu", cZaznamu);
		stmt.setString("message_id", messageId);
    	
		ResultSet resultSet = stmt.executeQuery();
    	while (resultSet.next()) {
    		TXParUhradaPl item = new TXParUhradaPl(_conn);
    		item.setProperties(resultSet);
    		this.add(item);
    	}
		stmt.close();
	}

}
