package adis.bnk.par.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import adis.bnk.gbnk2.NamedPreparedStatement;

public class TXParOdDsOdpCollection extends TXParCollectionBase<TXParOdDsOdp> {

	public TXParOdDsOdpCollection(Connection conn) {
		super(conn);
		
        this.set_sqlSelect(
        		String.join("", 
        				" select ",
        				"	c_zaznamu ",
        				"	,message_id ",
        				"	,kod_odp ",
        				"	,duvod_odp ",
        				"	,popis_odp ",
        				" from ",
        				"	t_x_par_od_ds_odp "));
	}
	
	public void fillForCZaznamu(int cZaznamu) throws SQLException {

		NamedPreparedStatement stmt = NamedPreparedStatement.prepareStatement(this._conn,
				String.join("", 
						this.get_sqlSelect(),
						" where ",
						"	c_zaznamu=:c_zaznamu;"));
		stmt.setInt("c_zaznamu", cZaznamu);
    	
		ResultSet resultSet = stmt.executeQuery();
    	while (resultSet.next()) {
    		TXParOdDsOdp item = new TXParOdDsOdp(_conn);
    		item.setProperties(resultSet);
    		this.add(item);
    	}
		stmt.close();
    	
	}

}
