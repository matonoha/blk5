package adis.bnk.par.db;

import java.math.BigDecimal;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.Date;

import adis.ref.aref_lib.konverze.MvieUTF8;

public class TpParTXParUhradaPl implements SQLData
{
  public static final String SQL_TYPE = "tp_par_t_x_par_uhrada_pl";

  private Integer    cZaznamu     ; // Id or Primary Key
  private BigDecimal cUhrady      ; // Id or Primary Key

  private String     messageId    ;
  private BigDecimal cPuvUhrady   ;
  private String     typUhrady    ;
  private String     obdobi       ;
  private String     cPopl        ;
  private String     nTitul       ;
  private Date       dUhrady      ;
  private BigDecimal kcUhrady     ;
  private String     kZpracCssz   ;
  private String     pZpracCssz   ;
  
  public TpParTXParUhradaPl(
      final Integer cZaznamu,
      final BigDecimal cUhrady,
      final String messageId,
      final BigDecimal cPuvUhrady,
      final String typUhrady,
      final String obdobi,
      final String cPopl,
      final String nTitul, 
      final Date dUhrady, 
      final BigDecimal kcUhrady, 
      final String kZpracCssz, 
      final String pZpracCssz 
      )
  {
	  this.setCZaznamu(cZaznamu);
	  this.setCUhrady(cUhrady);
	  this.setMessageId(messageId);
	  this.setCPuvUhrady(cPuvUhrady);
	  this.setTypUhrady(typUhrady);
	  this.setObdobi(obdobi);
	  this.setCPopl(cPopl);
	  this.setNTitul(nTitul);
	  this.setDUhrady(dUhrady);
	  this.setKcUhrady(kcUhrady);
	  this.setKZpracCssz(kZpracCssz);
	  this.setPZpracCssz(pZpracCssz);
  }

  @Override
  public String getSQLTypeName() throws SQLException
  {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(SQLInput stream, String typeName ) throws SQLException
  {
//    start    = stream.readTimestamp();
//    end      = stream.readTimestamp();
//    type     = stream.readBigDecimal().toBigInteger();
//    duration = stream.readBigDecimal().toBigInteger();
  }

  @Override
  public void writeSQL(SQLOutput stream) throws SQLException
  {
		stream.writeInt(this.getCZaznamu());
		stream.writeString(this.getMessageId());
		stream.writeBigDecimal(this.getCUhrady());
		stream.writeBigDecimal(this.getCPuvUhrady() != null ? this.getCPuvUhrady() : null);
		stream.writeString(this.getTypUhrady());
		stream.writeString(this.getObdobi());
		stream.writeString(this.getCPopl());
		stream.writeString(this.getNTitul());
		stream.writeDate(this.getDUhrady() != null ? new java.sql.Date(this.getDUhrady().getTime()) : null);
		stream.writeBigDecimal(this.getKcUhrady() != null ? this.getKcUhrady() : null);
		stream.writeString(this.getKZpracCssz());
		stream.writeString(this.getPZpracCssz());
  }

  //----------------------------------------------------------------------
  // toString METHOD
  //----------------------------------------------------------------------
  @Override
  public String toString() { 
      StringBuffer sb = new StringBuffer(); 
      sb.append(cZaznamu);
      sb.append("|");
      sb.append(messageId);
      sb.append("|");
      sb.append(cUhrady);
      sb.append("|");
      sb.append(cPuvUhrady);
      sb.append("|");
      sb.append(typUhrady);
      sb.append("|");
      sb.append(obdobi);
      sb.append("|");
      sb.append(cPopl);
      sb.append("|");
      sb.append(nTitul);
      sb.append("|");
      sb.append(dUhrady);
      sb.append("|");
      sb.append(kcUhrady);
      sb.append("|");
      sb.append(kZpracCssz);
      sb.append("|");
      sb.append(pZpracCssz);
      return sb.toString(); 
  } 
  
  //----------------------------------------------------------------------
  // GETTER(S) & SETTER(S) FOR ID OR PRIMARY KEY 
  //----------------------------------------------------------------------
  /**
   * Set the "cZaznamu" field value
   * This field is mapped on the database column "c_zaznamu" ( type "serial", NotNull : true ) 
   * @param cZaznamu
   */
	private void setCZaznamu( Integer cZaznamu ) {
      this.cZaznamu = cZaznamu ;
  }
  /**
   * Get the "cZaznamu" field value
   * This field is mapped on the database column "c_zaznamu" ( type "serial", NotNull : true ) 
   * @return the field value
   */
	public Integer getCZaznamu() {
      return this.cZaznamu;
  }
  /**
   * Set the "cUhrady" field value
   * This field is mapped on the database column "c_uhrady" ( type "decimal", NotNull : true ) 
   * @param cUhrady
   */
	private void setCUhrady( BigDecimal cUhrady ) {
      this.cUhrady = cUhrady ;
  }
  /**
   * Get the "cUhrady" field value
   * This field is mapped on the database column "c_uhrady" ( type "decimal", NotNull : true ) 
   * @return the field value
   */
	public BigDecimal getCUhrady() {
      return this.cUhrady;
  }

  //----------------------------------------------------------------------
  // GETTER(S) & SETTER(S) FOR OTHER DATA FIELDS
  //----------------------------------------------------------------------
  /**
   * Set the "messageId" field value
   * This field is mapped on the database column "message_id" ( type "nchar", NotNull : true ) 
   * @param messageId
   */
  private void setMessageId( String messageId ) {
      this.messageId = messageId;
  }
  /**
   * Get the "messageId" field value
   * This field is mapped on the database column "message_id" ( type "nchar", NotNull : true ) 
   * @return the field value
   */
  public String getMessageId() {
      return this.messageId;
  }

  /**
   * Set the "cPuvUhrady" field value
   * This field is mapped on the database column "c_puv_uhrady" ( type "decimal", NotNull : false ) 
   * @param cPuvUhrady
   */
  private void setCPuvUhrady( BigDecimal cPuvUhrady ) {
      this.cPuvUhrady = cPuvUhrady;
  }
  /**
   * Get the "cPuvUhrady" field value
   * This field is mapped on the database column "c_puv_uhrady" ( type "decimal", NotNull : false ) 
   * @return the field value
   */
  public BigDecimal getCPuvUhrady() {
      return this.cPuvUhrady;
  }

  /**
   * Set the "typUhrady" field value
   * This field is mapped on the database column "typ_uhrady" ( type "nchar", NotNull : false ) 
   * @param typUhrady
   */
  private void setTypUhrady( String typUhrady ) {
      this.typUhrady = typUhrady;
  }
  /**
   * Get the "typUhrady" field value
   * This field is mapped on the database column "typ_uhrady" ( type "nchar", NotNull : false ) 
   * @return the field value
   */
  public String getTypUhrady() {
      return this.typUhrady;
  }

  /**
   * Set the "obdobi" field value
   * This field is mapped on the database column "obdobi" ( type "nchar", NotNull : false ) 
   * @param obdobi
   */
  private void setObdobi( String obdobi ) {
      this.obdobi = obdobi;
  }
  /**
   * Get the "obdobi" field value
   * This field is mapped on the database column "obdobi" ( type "nchar", NotNull : false ) 
   * @return the field value
   */
  public String getObdobi() {
      return this.obdobi;
  }

  /**
   * Set the "cPopl" field value
   * This field is mapped on the database column "c_popl" ( type "nchar", NotNull : false ) 
   * @param cPopl
   */
  private void setCPopl( String cPopl ) {
      this.cPopl = cPopl;
  }
  /**
   * Get the "cPopl" field value
   * This field is mapped on the database column "c_popl" ( type "nchar", NotNull : false ) 
   * @return the field value
   */
  public String getCPopl() {
      return this.cPopl;
  }

  /**
   * Set the "nTitul" field value
   * This field is mapped on the database column "n_titul" ( type "nchar", NotNull : false ) 
   * @param nTitul
   */
  private void setNTitul( String nTitul ) {
      this.nTitul = nTitul;
  }
  /**
   * Get the "nTitul" field value
   * This field is mapped on the database column "n_titul" ( type "nchar", NotNull : false ) 
   * @return the field value
   */
  public String getNTitul() {
      return this.nTitul;
  }

  /**
   * Set the "dUhrady" field value
   * This field is mapped on the database column "d_uhrady" ( type "date", NotNull : false ) 
   * @param dUhrady
   */
  private void setDUhrady( Date dUhrady ) {
      this.dUhrady = dUhrady;
  }
  /**
   * Get the "dUhrady" field value
   * This field is mapped on the database column "d_uhrady" ( type "date", NotNull : false ) 
   * @return the field value
   */
  public Date getDUhrady() {
      return this.dUhrady;
  }

  /**
   * Set the "kcUhrady" field value
   * This field is mapped on the database column "kc_uhrady" ( type "decimal", NotNull : false ) 
   * @param kcUhrady
   */
  private void setKcUhrady( BigDecimal kcUhrady ) {
      this.kcUhrady = kcUhrady;
  }
  /**
   * Get the "kcUhrady" field value
   * This field is mapped on the database column "kc_uhrady" ( type "decimal", NotNull : false ) 
   * @return the field value
   */
  public BigDecimal getKcUhrady() {
      return this.kcUhrady;
  }

  /**
   * Set the "kZpracCssz" field value
   * This field is mapped on the database column "k_zprac_cssz" ( type "nchar", NotNull : false ) 
   * @param kZpracCssz
   */
  private void setKZpracCssz( String kZpracCssz ) {
      this.kZpracCssz = kZpracCssz;
  }
  /**
   * Get the "kZpracCssz" field value
   * This field is mapped on the database column "k_zprac_cssz" ( type "nchar", NotNull : false ) 
   * @return the field value
   */
  public String getKZpracCssz() {
      return this.kZpracCssz;
  }

  /**
   * Set the "pZpracCssz" field value
   * This field is mapped on the database column "p_zprac_cssz" ( type "nvarchar", NotNull : false ) 
   * @param pZpracCssz
   */
  private void setPZpracCssz( String pZpracCssz ) {
  	// prevod non ISO 8859-2 znaku
  	pZpracCssz = MvieUTF8.filterTOiso2(pZpracCssz);

  	this.pZpracCssz = pZpracCssz;
  }
  /**
   * Get the "pZpracCssz" field value
   * This field is mapped on the database column "p_zprac_cssz" ( type "nvarchar", NotNull : false ) 
   * @return the field value
   */
  public String getPZpracCssz() {
      return this.pZpracCssz;
  }
}