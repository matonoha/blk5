package adis.bnk.par.db;

import java.sql.Connection;

import adis.bnk.gbnk2.IExistsRecord;

public abstract class TXParBase implements IExistsRecord {

	private String _sqlSelectTestExists;
	private String _sqlSelect;
	private String _sqlInsert;
	private String _sqlUpdate;
	private String _sqlDelete;

	/**
	 * overeni existence recordu v tabulce
	 */
	protected Boolean existsRecord;

	/*
	 * @see adis.bnk.gbnk2.IExistsRecord#isExistsRecord()
	 */
	@Override
	public Boolean isExistsRecord() {
		return this.existsRecord;
	}

	/**
	 * @param existsRecord
	 */
	protected void setExistsRecord(boolean existsRecord) {
		this.existsRecord = existsRecord;
	}
	
	protected final void set_sqlSelectTestExists(String _sqlSelectTestExists) {
		this._sqlSelectTestExists = _sqlSelectTestExists;
	}

	protected final void set_sqlSelect(String _sqlSelect) {
		this._sqlSelect = _sqlSelect;
	}

	protected final void set_sqlInsert(String _sqlInsert) {
		this._sqlInsert = _sqlInsert;
	}

	protected final void set_sqlUpdate(String _sqlUpdate) {
		this._sqlUpdate = _sqlUpdate;
	}

	protected final void set_sqlDelete(String _sqlDelete) {
		this._sqlDelete = _sqlDelete;
	}

	public String get_sqlSelectTestExists() {
		return _sqlSelectTestExists;
	}

	public String get_sqlSelect() {
		return _sqlSelect;
	}

	public String get_sqlInsert() {
		return _sqlInsert;
	}

	public String get_sqlUpdate() {
		return _sqlUpdate;
	}

	public String get_sqlDelete() {
		return _sqlDelete;
	}

	protected Connection _conn;
	
	public TXParBase(Connection conn) {
		this._conn = conn;
	}
}
