package adis.bnk.par.db;

import java.sql.Connection;
import java.util.ArrayList;

public abstract class TXParCollectionBase<T> extends ArrayList<T> {

	private String _sqlSelect;
	private String _sqlDelete;

	protected final void set_sqlSelect(String _sqlSelect) {
		this._sqlSelect = _sqlSelect;
	}

	public String get_sqlSelect() {
		return _sqlSelect;
	}

	protected final void set_sqlDelete(String _sqlDelete) {
		this._sqlDelete = _sqlDelete;
	}

	public String get_sqlDelete() {
		return _sqlDelete;
	}
	
	protected Connection _conn;
	
	public TXParCollectionBase(Connection conn) {
		this._conn = conn;
	}
	
}
