package adis.bnk.par.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import adis.bnk.gbnk2.NamedPreparedStatement;

public class TXParOdDsCollection extends TXParCollectionBase<TXParOdDs> {

	public TXParOdDsCollection(Connection conn) {
		super(conn);
		
        this.set_sqlSelect(
        		String.join("", 
        				" select ",
        				"	c_zaznamu ",
        				"	,message_id ",
        				"	,aifo ",
        				"	,rodne_cislo ",
        				"	,c_poj ",
        				"	,ecp ",
        				"	,jmeno ",
        				"	,prijmeni ",
        				"	,rodnepr ",
        				"	,d_naroz ",
        				"	,c_adrbod ",
        				"	,adresa ",
        				"	,ke_dni ",
        				"	,id_zahajeni ",
        				"	,id_zahajeni_o ",
        				"	,id_zahajeni_orig ", 
        				"	,d_zahajeni ", 
        				"	,d_zahsvc ", 
        				"	,id_ukonceni ", 
        				"	,id_ukonceni_o ", 
        				"	,pref_cssz ",
        				" from ",
        				"	t_x_par_od_ds "));
	}
	
	public void fillForCZaznamu(int cZaznamu) throws SQLException {

		NamedPreparedStatement stmt = NamedPreparedStatement.prepareStatement(this._conn,
				String.join("", 
						this.get_sqlSelect(),
						" where ",
						"	c_zaznamu=:c_zaznamu;"));
		stmt.setInt("c_zaznamu", cZaznamu);
    	
		ResultSet resultSet = stmt.executeQuery();
    	while (resultSet.next()) {
    		TXParOdDs item = new TXParOdDs(_conn);
    		item.setProperties(resultSet);
    		this.add(item);
    	}
		stmt.close();
    	
	}

}
