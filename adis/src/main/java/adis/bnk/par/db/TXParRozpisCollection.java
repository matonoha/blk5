package adis.bnk.par.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import adis.bnk.gbnk2.NamedPreparedStatement;

public class TXParRozpisCollection extends TXParCollectionBase<TXParRozpis> {

	public TXParRozpisCollection(Connection conn) {
		super(conn);
		
        this.set_sqlSelect(
        		String.join("", 
        				" select ",
        				"	c_zaznamu ",
        				"	,kod_prijemce ",
        				"	,c_popl_prijemce ",
        				"	,spec_symb ",
        				"	,obdobi ",
        				"	,d_platby ",
        				"	,pocet_celk ",
        				"	,kc_celk ",
        				"	,n_dan ",
        				"	,kc_dan ",
        				"	,pocet_dan ",
        				"	,n_prisl ",
        				"	,kc_prisl ",
        				"	,pocet_prisl ",
        				"	,status_zprac ",
        				" from ",
        				"	t_x_par_rozpis "));
	}
	
	public void fillForCZaznamu(int cZaznamu) throws SQLException {

		NamedPreparedStatement stmt = NamedPreparedStatement.prepareStatement(this._conn,
				String.join("", 
						this.get_sqlSelect(),
						" where ",
						"	c_zaznamu=:c_zaznamu;"));
		stmt.setInt("c_zaznamu", cZaznamu);
    	
		ResultSet resultSet = stmt.executeQuery();
    	while (resultSet.next()) {
    		TXParRozpis item = new TXParRozpis(_conn);
    		item.setProperties(resultSet);
    		this.add(item);
    	}
		stmt.close();
    	
	}

}
