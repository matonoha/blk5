package adis.bnk.par.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import adis.bnk.gbnk2.NamedPreparedStatement;

public class TXParDoSvcCollection extends TXParCollectionBase<TXParOdDs> {

	public TXParDoSvcCollection(Connection conn) {
		super(conn);
		
        this.set_sqlSelect(
        		String.join("", 
        				" select ",
        				"	c_zaznamu ",
        				"	,message_id ",
        				"	,c_por ",
        				"	,d_podozn ",
        				"	,d_zahajeni ",
        				"	,d_ukonceni ",
        				"	,duv_ukonceni ",
        				" from ",
        				"	t_x_par_do_svc "));

        this.set_sqlDelete(
				String.join("",
						" delete from t_x_par_do_svc "));
	}
	
	public boolean deleteForCZaznamuAndMessageId(int cZaznamu, String messageId) throws SQLException {

		NamedPreparedStatement stmt = NamedPreparedStatement.prepareStatement(this._conn,
				String.join("", 
						this.get_sqlDelete(),
						" where ",
						"	c_zaznamu = :c_zaznamu",
						"	and message_id = :message_id; "));
		stmt.setInt("c_zaznamu", cZaznamu);
		stmt.setString("message_id", messageId);
    	
		boolean result = stmt.execute();
		stmt.close();
		
		return result;
		
	}

}
