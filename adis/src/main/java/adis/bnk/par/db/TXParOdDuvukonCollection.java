package adis.bnk.par.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import adis.bnk.gbnk2.NamedPreparedStatement;

public class TXParOdDuvukonCollection extends TXParCollectionBase<TXParOdDuvukon> {

	public TXParOdDuvukonCollection(Connection conn) {
		super(conn);
		
        this.set_sqlSelect(
        		String.join("", 
        				" select ",
        				"	c_zaznamu ",
        				"	,message_id ",
        				"	,duvod_kon ",
        				"	,duvod ",
        				" from ",
        				"	t_x_par_od_duvukon "));
	}
	
	public void fillForCZaznamu(int cZaznamu) throws SQLException {

		NamedPreparedStatement stmt = NamedPreparedStatement.prepareStatement(this._conn,
				String.join("", 
						this.get_sqlSelect(),
						" where ",
						"	c_zaznamu=:c_zaznamu;"));
		stmt.setInt("c_zaznamu", cZaznamu);
    	
		ResultSet resultSet = stmt.executeQuery();
    	while (resultSet.next()) {
    		TXParOdDuvukon item = new TXParOdDuvukon(_conn);
    		item.setProperties(resultSet);
    		this.add(item);
    	}
		stmt.close();
	}

	public void fillForCZaznamuAndMessageId(int cZaznamu, String messageId) throws SQLException {

		NamedPreparedStatement stmt = NamedPreparedStatement.prepareStatement(this._conn,
				String.join("", 
						this.get_sqlSelect(),
						" where ",
						"	c_zaznamu = :c_zaznamu",
						"	and message_id = :message_id; "));
		stmt.setInt("c_zaznamu", cZaznamu);
		stmt.setString("message_id", messageId);
    	
		ResultSet resultSet = stmt.executeQuery();
    	while (resultSet.next()) {
    		TXParOdDuvukon item = new TXParOdDuvukon(_conn);
    		item.setProperties(resultSet);
    		this.add(item);
    	}
		stmt.close();
	}

}
