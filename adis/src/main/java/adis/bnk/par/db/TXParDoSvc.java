/*
 * Java bean class for entity "t_x_par_do_svc" 
 * Created on 2020-12-10 ( Date ISO 2020-12-10 - Time 10:00:07 )
 * Generated by Telosys Tools Generator ( version 3.1.2 )
 */

package adis.bnk.par.db;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import adis.bnk.gbnk2.MapKeyValueItem;
import adis.bnk.gbnk2.NamedPreparedStatement;
import adis.bnk.par.checkSelfEmpl.response.CheckSelfEmplResponse;

/**
 * Java bean for entity "t_x_par_do_svc"
 * 
 * @author Telosys Tools Generator
 *
 */
public class TXParDoSvc extends TXParBase implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer    cZaznamu     ; // Id or Primary Key
    private String     messageId    ; // Id or Primary Key
    private Short      cPor         ; // Id or Primary Key

    private Date       dPodozn      ;
    private Date       dZahajeni    ;
    private Date       dUkonceni    ;
    private String     duvUkonceni  ;

    public TXParDoSvc(Connection conn, int cZaznamu, String messageId, short cPor, 
    		CheckSelfEmplResponse.Subjekty.Subjekt.IntervalySvc intervalSvc) throws Exception {
    	this(conn);
    	
    	this.setCZaznamu(cZaznamu);
    	this.setMessageId(messageId);
    	this.setCPor(cPor);
    	
    	this.setDPodozn(
    			intervalSvc.getOznameniZahajeniSvc() != null
    			? intervalSvc.getOznameniZahajeniSvc().toGregorianCalendar().getTime()
    			: null);
    	this.setDZahajeni(
    			intervalSvc.getZahajeniSvc() != null
    			? intervalSvc.getZahajeniSvc().toGregorianCalendar().getTime()
    			: null);
    	this.setDUkonceni(
    			intervalSvc.getUkonceniSvc() != null
    			? intervalSvc.getUkonceniSvc().toGregorianCalendar().getTime()
    			: null);
    	this.setDuvUkonceni(intervalSvc.getDuvodUkonceniSvc());
    	
    }
    
    /**
     * Default constructor
     */
    public TXParDoSvc(Connection conn) {
        super(conn);
        
        this.set_sqlSelectTestExists(
				String.join("",
						" select 1 from t_x_par_do_svc",
						" where ",
						" 	c_zaznamu = :c_zaznamu ",
						" 	and message_id = :message_id ",
						"	and c_por = :c_por; "));

        this.set_sqlSelect(
        		String.join("", 
        				" select ",
        				"	c_zaznamu ",
        				"	,message_id ",
        				"	,c_por ",
        				"	,d_podozn ",
        				"	,d_zahajeni ",
        				"	,d_ukonceni ",
        				"	,duv_ukonceni ",
        				" from ",
        				"	t_x_par_do_svc "));

        this.set_sqlInsert(
        		String.join("", 
        				" insert into t_x_par_do_svc ",
        				"	(c_zaznamu, message_id, c_por, d_podozn, d_zahajeni, d_ukonceni, duv_ukonceni) ",
        				" 	values ",
        				"	(:c_zaznamu, :message_id, :c_por, :d_podozn, :d_zahajeni, :d_ukonceni, :duv_ukonceni); "));

        this.set_sqlDelete(
				String.join("",
						" delete from t_x_par_do_svc",
						" where ",
						" 	c_zaznamu = :c_zaznamu ",
						" 	and message_id = :message_id ",
						"	and c_por = :c_por; "));
        
    }
    
    public boolean insert()  throws SQLException {

		// Create a test statement
    	NamedPreparedStatement testStmt = NamedPreparedStatement.prepareStatement(this._conn, 
    			this.get_sqlSelectTestExists());
		testStmt.setInt("c_zaznamu", this.getCZaznamu());
		testStmt.setString("message_id", this.getMessageId());
		testStmt.setShort("c_por", this.getCPor());
		this.setExistsRecord(testStmt.executeQuery().next());
		testStmt.close();

    	if (this.isExistsRecord()) {
    		throw new SQLException(
    				String.format("Zaznam pro PK jiz v tabulce existuje: %s,%s,%s", 
    						this.getCZaznamu(),
    						this.getMessageId(),
    						this.getCPor()));
    	}

		NamedPreparedStatement stmt = NamedPreparedStatement.prepareStatement(this._conn, 
				this.get_sqlInsert());

		stmt.setInt("c_zaznamu", this.getCZaznamu());
		stmt.setString("message_id", this.getMessageId());
		stmt.setShort("c_por", this.getCPor());
		stmt.setDate(
				"d_podozn", 
				this.getDPodozn() != null 
					? new java.sql.Date(this.getDPodozn().getTime())
					: null);
		stmt.setDate(
				"d_zahajeni", 
				this.getDZahajeni() != null
					? new java.sql.Date(this.getDZahajeni().getTime())
					: null);
		stmt.setDate(
				"d_ukonceni",
				this.getDUkonceni() != null
					? new java.sql.Date(this.getDUkonceni().getTime())
					: null);
		stmt.setString("duv_ukonceni", this.getDuvUkonceni());

		boolean result = stmt.execute();
		stmt.close();
    	
    	return result;
    }
    
    public boolean delete()  throws SQLException {

		NamedPreparedStatement stmt = NamedPreparedStatement.prepareStatement(this._conn, 
				this.get_sqlDelete());

		stmt.setInt("c_zaznamu", this.getCZaznamu());
		stmt.setString("message_id", this.getMessageId());
		stmt.setShort("c_por", this.getCPor());

		boolean result = stmt.execute();
		stmt.close();
    	
    	return result;
    }
    
	public static boolean insertByDeleteInsertBatch(Connection conn, List<TpParTXParDoSvc> data)  
			throws SQLException {

		TXParDoSvc tXParDoSvc = new TXParDoSvc(conn);
		
		NamedPreparedStatement stmtDelete = 
				NamedPreparedStatement.prepareStatement(
						conn, 
						String.format(
								"%s",
								String.join("",
										" delete from t_x_par_do_svc",
										" where ",
										" 	c_zaznamu = :c_zaznamu ",
										" 	and message_id = :message_id;")));

		NamedPreparedStatement stmtInsert = 
				NamedPreparedStatement.prepareStatement(
						conn, 
						String.format("%s", tXParDoSvc.get_sqlInsert()));

		//protoze polozka c_por je tvorena umele, mazeme hodnoty pro c_zaznamu a message_id aby bylo zajisteno vycisteni tabulky
		//pro tuto kombinaci
		for(Map.Entry<Integer, String> item : data.stream().map(p -> MapKeyValueItem.entry(p.getCZaznamu(),p.getMessageId())).distinct().collect(Collectors.toList())) {
//			System.out.println(String.format("SVC> c_zaznamu=%s,message_id=%s", item.getKey(), item.getValue()));

			stmtDelete.setInt("c_zaznamu", item.getKey());
			stmtDelete.setString("message_id", item.getValue());
			stmtDelete.addBatch();
		}

		stmtDelete.executeBatch();
		stmtDelete.close();
		
		//nasledne do tabulky jenom vkladame
		for(TpParTXParDoSvc item : data) {
			
			stmtInsert.setInt("c_zaznamu", item.getCZaznamu());
			stmtInsert.setString("message_id", item.getMessageId());
			stmtInsert.setShort("c_por", item.getCPor());
			stmtInsert.setDate(
					"d_podozn", 
					item.getDPodozn() != null 
						? new java.sql.Date(item.getDPodozn().getTime())
						: null);
			stmtInsert.setDate(
					"d_zahajeni", 
					item.getDZahajeni() != null
						? new java.sql.Date(item.getDZahajeni().getTime())
						: null);
			stmtInsert.setDate(
					"d_ukonceni",
					item.getDUkonceni() != null
						? new java.sql.Date(item.getDUkonceni().getTime())
						: null);
			stmtInsert.setString("duv_ukonceni", item.getDuvUkonceni());
			stmtInsert.addBatch();
			
		}

		int[] result = stmtInsert.executeBatch();
		stmtInsert.close();
		
    	return data.size() == result.length;
    }
    
    
    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR ID OR PRIMARY KEY 
    //----------------------------------------------------------------------
    /**
     * Set the "cZaznamu" field value
     * This field is mapped on the database column "c_zaznamu" ( type "serial", NotNull : true ) 
     * @param cZaznamu
     */
	public void setCZaznamu( Integer cZaznamu ) {
        this.cZaznamu = cZaznamu ;
    }
    /**
     * Get the "cZaznamu" field value
     * This field is mapped on the database column "c_zaznamu" ( type "serial", NotNull : true ) 
     * @return the field value
     */
	public Integer getCZaznamu() {
        return this.cZaznamu;
    }
    /**
     * Set the "messageId" field value
     * This field is mapped on the database column "message_id" ( type "nchar", NotNull : true ) 
     * @param messageId
     */
	public void setMessageId( String messageId ) {
        this.messageId = messageId ;
    }
    /**
     * Get the "messageId" field value
     * This field is mapped on the database column "message_id" ( type "nchar", NotNull : true ) 
     * @return the field value
     */
	public String getMessageId() {
        return this.messageId;
    }
    /**
     * Set the "cPor" field value
     * This field is mapped on the database column "c_por" ( type "smallint", NotNull : true ) 
     * @param cPor
     */
	public void setCPor( Short cPor ) {
        this.cPor = cPor ;
    }
    /**
     * Get the "cPor" field value
     * This field is mapped on the database column "c_por" ( type "smallint", NotNull : true ) 
     * @return the field value
     */
	public Short getCPor() {
        return this.cPor;
    }

    //----------------------------------------------------------------------
    // GETTER(S) & SETTER(S) FOR OTHER DATA FIELDS
    //----------------------------------------------------------------------
    /**
     * Set the "dPodozn" field value
     * This field is mapped on the database column "d_podozn" ( type "date", NotNull : false ) 
     * @param dPodozn
     */
    public void setDPodozn( Date dPodozn ) {
        this.dPodozn = dPodozn;
    }
    /**
     * Get the "dPodozn" field value
     * This field is mapped on the database column "d_podozn" ( type "date", NotNull : false ) 
     * @return the field value
     */
    public Date getDPodozn() {
        return this.dPodozn;
    }

    /**
     * Set the "dZahajeni" field value
     * This field is mapped on the database column "d_zahajeni" ( type "date", NotNull : false ) 
     * @param dZahajeni
     */
    public void setDZahajeni( Date dZahajeni ) {
        this.dZahajeni = dZahajeni;
    }
    /**
     * Get the "dZahajeni" field value
     * This field is mapped on the database column "d_zahajeni" ( type "date", NotNull : false ) 
     * @return the field value
     */
    public Date getDZahajeni() {
        return this.dZahajeni;
    }

    /**
     * Set the "dUkonceni" field value
     * This field is mapped on the database column "d_ukonceni" ( type "date", NotNull : false ) 
     * @param dUkonceni
     */
    public void setDUkonceni( Date dUkonceni ) {
        this.dUkonceni = dUkonceni;
    }
    /**
     * Get the "dUkonceni" field value
     * This field is mapped on the database column "d_ukonceni" ( type "date", NotNull : false ) 
     * @return the field value
     */
    public Date getDUkonceni() {
        return this.dUkonceni;
    }

    /**
     * Set the "duvUkonceni" field value
     * This field is mapped on the database column "duv_ukonceni" ( type "nvarchar", NotNull : false ) 
     * @param duvUkonceni
     */
    public void setDuvUkonceni( String duvUkonceni ) {
        this.duvUkonceni = duvUkonceni;
    }
    /**
     * Get the "duvUkonceni" field value
     * This field is mapped on the database column "duv_ukonceni" ( type "nvarchar", NotNull : false ) 
     * @return the field value
     */
    public String getDuvUkonceni() {
        return this.duvUkonceni;
    }

    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    @Override
    public String toString() { 
        StringBuffer sb = new StringBuffer(); 
        sb.append(cZaznamu);
        sb.append("|");
        sb.append(messageId);
        sb.append("|");
        sb.append(cPor);
        sb.append("|");
        sb.append(dPodozn);
        sb.append("|");
        sb.append(dZahajeni);
        sb.append("|");
        sb.append(dUkonceni);
        sb.append("|");
        sb.append(duvUkonceni);
        return sb.toString(); 
    } 

}
