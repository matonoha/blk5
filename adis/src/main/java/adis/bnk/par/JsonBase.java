package adis.bnk.par;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.google.gson.Gson;

import project.helper.Helper;


/**
 * @author valenta
 *
 * @param <T>
 */
/**
 * @author valenta
 *
 * @param <T>
 */
public abstract class JsonBase<T> {

	private String _jsonSchemaFile;
	
	/**
	 * 
	 */
	protected Connection _conn;
	
	/**
	 * @param conn
	 */
	public JsonBase(Connection conn) {
		this._conn = conn;
	}

	/**
	 * @param _jsonSchemaFile
	 */
	protected final void set_jsonSchemaFile(String _jsonSchemaFile) {
		this._jsonSchemaFile = _jsonSchemaFile;
	}

	/**
	 * @return
	 */
	public String get_jsonSchemaFile() {
		return _jsonSchemaFile;
	}

	private URL _xsdSchemaURL;

	/**
	 * @param _xsdSchemaURL
	 */
	protected final void set_xsdSchemaURL(URL _xsdSchemaURL) {
		this._xsdSchemaURL = _xsdSchemaURL;
	}

	/**
	 * @return
	 */
	public URL get_xsdSchemaURL() {
		return _xsdSchemaURL;
	}
	
	/**
	 * 
	 */
	protected void ValidateDuties() {
		
	}
	
	/**
	 * @param date
	 * @return
	 * @throws DatatypeConfigurationException
	 */
	protected XMLGregorianCalendar ConvertFromDateToDatetime(java.util.Date date) throws DatatypeConfigurationException {

		if (date == null) {
			return null;
		}
		
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		return DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
		
	}

	/**
	 * @param date
	 * @return
	 * @throws DatatypeConfigurationException
	 */
	protected XMLGregorianCalendar convertFromDateToDate(java.util.Date date) throws DatatypeConfigurationException {

		if (date == null) {
			return null;
		}
		
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		return DatatypeFactory.newInstance().newXMLGregorianCalendarDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH)+1, cal.get(Calendar.DAY_OF_MONTH), DatatypeConstants.FIELD_UNDEFINED);
	}

	/**
	 * @param objectForCreateJson
	 * @return
	 * @throws IOException
	 */
	public ByteArrayOutputStream convertObjectToJsonOutputStream(T objectForCreateJson) throws IOException {

		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

		Gson gson = new Gson();
		String result = gson.toJson(objectForCreateJson);
		
		byteArrayOutputStream.write(result.getBytes());
		
		// Write to System.out - zobrazeni v Console
		System.out.println(result);
		// Write to System.out - zapsani do File
		Helper.writeFile("testFile.json", result.getBytes());

		return byteArrayOutputStream;
	}
	
	/**
	 * @param byteArr
	 * @param objectForUnmarshallJson
	 * @return
	 * @throws IOException
	 */
	public T convertBytesToObject(byte[] byteArr, T objectForUnmarshallJson) throws IOException {
		InputStream inputStream = new ByteArrayInputStream(byteArr);
		return convertInputStreamToObject(inputStream, objectForUnmarshallJson);
	}
	
	/**
	 * @param inputStream
	 * @param objectForUnmarshallJson
	 * @return
	 * @throws IOException
	 */
	public T convertInputStreamToObject(InputStream inputStream, T objectForUnmarshallJson) throws IOException {
		
		ByteArrayOutputStream result = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int length;
		while ((length = inputStream.read(buffer)) != -1) {
		    result.write(buffer, 0, length);
		}
		// StandardCharsets.UTF_8.name() > JDK 7
		String jsonString = result.toString("UTF-8");
		
		Gson gson = new Gson();
		objectForUnmarshallJson = (T) gson.fromJson(jsonString, objectForUnmarshallJson.getClass());


		return objectForUnmarshallJson;
	}	

	
}
