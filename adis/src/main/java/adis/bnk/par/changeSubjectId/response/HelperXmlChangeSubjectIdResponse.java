package adis.bnk.par.changeSubjectId.response;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.xml.datatype.DatatypeConfigurationException;

import adis.bnk.par.XmlBase;
import adis.bnk.par.changeSubjectId.response.ChangeSubjectIdResponse.Header;
import adis.bnk.par.changeSubjectId.response.ChangeSubjectIdResponse.Header.Parameters;
import adis.bnk.par.changeSubjectId.response.ChangeSubjectIdResponse.Header.Parameters.Parameter;
import adis.bnk.par.changeSubjectId.response.ChangeSubjectIdResponse.Polozky.Polozka;

/**
 * @author valenta
 *
 */
public class HelperXmlChangeSubjectIdResponse extends XmlBase<ChangeSubjectIdResponse> {

	private final static String CONSUMER_ID = "IDS";
	private final static String PROVIDER_ID = "ADIS";
	private final static int PROCESSING_PRIORITY = 0;
	private final static String VERSION = "1.2";
	private final static String PARAMETER_KEY_FU = "FU";
	private final static String PARAMETER_VALUE_FU = "230";
	private final static String PARAMETER_KEY_ORIGINATOR = "ORIGINATOR";

	private ObjectFactory _objectFactory = new ObjectFactory();
	private final String version = String.format("v%s_", VERSION) ;

	
    public enum OriginatorEnum {
    	Z ("ZP"),
    	S ("CSSZ");
    	
    	private final String _hodnota;
    	
    	public String getHodnota() {
    		return _hodnota;
    	}
    	
    	OriginatorEnum (String hodnota) {
    		this._hodnota = hodnota;
    	}
    	
    	public static OriginatorEnum getByHodnota(String hodnota) throws Exception {
    		switch(hodnota) {
    		case "ZP":
    			return Z;
    		case "CSSZ":
    			return S;
    		default:
    			throw new Exception("Neznamý typ hodnoty.");
    		}
    	}
	}
	
    public enum ProcessingResultEnum {
    	A ("A"),
    	N ("N");
    	
    	private final String _hodnota;
    	
    	public String getHodnota() {
    		return _hodnota;
    	}
    	
    	ProcessingResultEnum (String hodnota) {
    		this._hodnota = hodnota;
    	}
    	
    	public static ProcessingResultEnum getByHodnota(String hodnota) throws Exception {
    		switch(hodnota) {
    		case "A":
    			return A;
    		case "N":
    			return N;
    		default:
    			throw new Exception("Neznamý typ hodnoty.");
    		}
    	}
    }

	/**
	 * @param conn
	 * @param cZaznamu
	 */
	public HelperXmlChangeSubjectIdResponse(Connection conn) {
		super(conn);
		
		this.set_xsdSchemaURL(
				this.getClass().getResource(
						"/adis/bnk/par/schema/ADIS_IA_ChangeSubjectId-P2_Response_v1.2.xsd"));
	}
	
	public ChangeSubjectIdResponse getTestResponse(int pocetPolozek) throws DatatypeConfigurationException {
		
		//<changeSubjectIdResponse><header>
		Header header = this._objectFactory.createChangeSubjectIdResponseHeader();
		header.setMessageId(UUID.randomUUID().toString());
		header.setTimestamp(this.convertFromDateToDatetime(new java.util.Date()));
		header.setCorrelationId(UUID.randomUUID().toString());
		header.setConsumerId(CONSUMER_ID);
		header.setProviderId(PROVIDER_ID);
		header.setProcessingPriority(PROCESSING_PRIORITY);
		header.setVersion(VERSION);
		
		//<changeSubjectIdResponse><header><parameters><parameter>
		List<Parameter> listParameter = new ArrayList<Parameter>();
		Parameter parameter1 = this._objectFactory.createChangeSubjectIdResponseHeaderParametersParameter();
		parameter1.setKey(PARAMETER_KEY_FU);
		parameter1.setValue(PARAMETER_VALUE_FU);
		listParameter.add(parameter1);

		Parameter parameter2 = this._objectFactory.createChangeSubjectIdResponseHeaderParametersParameter();
		parameter2.setKey(PARAMETER_KEY_ORIGINATOR);
		switch ((int)(Math.random()*2)) {
			case 0:
				parameter2.setValue(OriginatorEnum.S.getHodnota());
				break;
			case 1:
				parameter2.setValue(OriginatorEnum.Z.getHodnota());
				break;
			default:
				break;
		}
		listParameter.add(parameter2);
		
		//<changeSubjectIdResponse><header><parameters>
		Parameters parameters = this._objectFactory.createChangeSubjectIdResponseHeaderParameters();
		parameters.parameter = listParameter;
		
		header.setParameters(parameters);

		List<Polozka> listPolozka = new ArrayList<Polozka>();
		for (int pol = 1; pol <=  pocetPolozek; pol++) {
			
			//<changeSubjectIdResponse><polozky><polozka>
			Polozka polozka = this._objectFactory.createChangeSubjectIdResponsePolozkyPolozka();
			polozka.setId(UUID.randomUUID().toString());

			switch ((int)(Math.random()*2)) {
				case 0:
					polozka.setProcessingResult(ProcessingResultEnum.A.toString());
					break;
				case 1:
					polozka.setProcessingResult(ProcessingResultEnum.N.toString());
					break;
				default:
					break;
			}
			
			listPolozka.add(polozka);
		}
		
		//<changeSubjectIdResponse><Polozky>
		ChangeSubjectIdResponse.Polozky polozky = this._objectFactory.createChangeSubjectIdResponsePolozky();
		polozky.polozka = listPolozka;
		
		//<changeSubjectIdResponse>
		ChangeSubjectIdResponse changeSubjectIdResponse = this._objectFactory.createChangeSubjectIdResponse();
		changeSubjectIdResponse.setHeader(header);
		changeSubjectIdResponse.setPolozky(polozky);
		
		return changeSubjectIdResponse;
	
	}

	
}
