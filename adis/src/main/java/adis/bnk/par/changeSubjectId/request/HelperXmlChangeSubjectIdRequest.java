package adis.bnk.par.changeSubjectId.request;

import java.math.BigDecimal;
import java.sql.Connection;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.xml.datatype.DatatypeConfigurationException;

import adis.bnk.par.XmlBase;
import adis.bnk.par.changeSubjectId.request.ChangeSubjectIdRequest.Header;
import adis.bnk.par.changeSubjectId.request.ChangeSubjectIdRequest.Header.Parameters;
import adis.bnk.par.changeSubjectId.request.ChangeSubjectIdRequest.Header.Parameters.Parameter;
import adis.bnk.par.changeSubjectId.request.ChangeSubjectIdRequest.Polozky.Polozka;
import adis.bnk.par.changeSubjectId.request.ChangeSubjectIdRequest.Polozky.Polozka.CisloPojistence;
import adis.bnk.par.changeSubjectId.request.ChangeSubjectIdRequest.Polozky.Polozka.EvidencniCisloPojistence;
import adis.bnk.par.changeSubjectId.request.ChangeSubjectIdRequest.Polozky.Polozka.RodneCislo;

/**
 * @author valenta
 *
 */
public class HelperXmlChangeSubjectIdRequest extends XmlBase<ChangeSubjectIdRequest> {

	private final static String CONSUMER_ID = "IDR";
	private final static String PROVIDER_ID = "ADIS";
	private final static int PROCESSING_PRIORITY = 0;
	private final static String VERSION = "1.16";
	private final static String PARAMETER_KEY_FU = "FU";
	private final static String PARAMETER_VALUE_FU = "230";
	private final static String PARAMETER_KEY_ORIGINATOR = "ORIGINATOR";


	private ObjectFactory _objectFactory = new ObjectFactory();
	private final String version = String.format("v%s_", VERSION) ;

    public enum PreferovanyIdentifikatorCSSZEnum {
    	R ("rc"),
    	E ("ecp");
    	
    	private final String _hodnota;
    	
    	public String getHodnota() {
    		return _hodnota;
    	}
    	
    	PreferovanyIdentifikatorCSSZEnum (String hodnota) {
    		this._hodnota = hodnota;
    	}
    	
    	public static PreferovanyIdentifikatorCSSZEnum getByHodnota(String hodnota) throws Exception {
    		switch(hodnota) {
    		case "rc":
    			return R;
    		case "ecp":
    			return E;
    		default:
    			throw new Exception("Neznamý typ hodnoty.");
    		}
    	}
	}

    public enum OriginatorEnum {
    	Z ("ZP"),
    	S ("CSSZ");
    	
    	private final String _hodnota;
    	
    	public String getHodnota() {
    		return _hodnota;
    	}
    	
    	OriginatorEnum (String hodnota) {
    		this._hodnota = hodnota;
    	}
    	
    	public static OriginatorEnum getByHodnota(String hodnota) throws Exception {
    		switch(hodnota) {
    		case "ZP":
    			return Z;
    		case "CSSZ":
    			return S;
    		default:
    			throw new Exception("Neznamý typ hodnoty.");
    		}
    	}
	}


	/**
	 * @param conn
	 * @param cZaznamu
	 */
	public HelperXmlChangeSubjectIdRequest(Connection conn) {
		super(conn);
		
		this.set_xsdSchemaURL(
				this.getClass().getResource(
						"/adis/bnk/par/schema/ADIS_IA_ChangeSubjectId-P1_Request_v1.16.xsd"));
	}
	
	public ChangeSubjectIdRequest getTestRequest(int pocetPolozek) throws DatatypeConfigurationException {
		
		//<changeSubjectIdRequest><header>
		Header header = this._objectFactory.createChangeSubjectIdRequestHeader();
		header.setMessageId(UUID.randomUUID().toString());
		header.setTimestamp(this.convertFromDateToDatetime(new java.util.Date()));
		header.setCorrelationId(UUID.randomUUID().toString());
		header.setConsumerId(CONSUMER_ID);
		header.setProviderId(PROVIDER_ID);
		header.setProcessingPriority(PROCESSING_PRIORITY);
		header.setVersion(VERSION);
		
		//<changeSubjectIdRequest><header><parameters><parameter>
		List<Parameter> listParameter = new ArrayList<Parameter>();
		Parameter parameter1 = this._objectFactory.createChangeSubjectIdRequestHeaderParametersParameter();
		parameter1.setKey(PARAMETER_KEY_FU);
		parameter1.setValue(PARAMETER_VALUE_FU);
		listParameter.add(parameter1);

		Parameter parameter2 = this._objectFactory.createChangeSubjectIdRequestHeaderParametersParameter();
		parameter2.setKey(PARAMETER_KEY_ORIGINATOR);
		switch ((int)(Math.random()*2)) {
			case 0:
				parameter2.setValue(OriginatorEnum.S.getHodnota());
				break;
			case 1:
				parameter2.setValue(OriginatorEnum.Z.getHodnota());
				break;
			default:
				break;
		}
		listParameter.add(parameter2);
		
		//<changeSubjectIdRequest><header><parameters>
		Parameters parameters = this._objectFactory.createChangeSubjectIdRequestHeaderParameters();
		parameters.parameter = listParameter;
		
		header.setParameters(parameters);

		List<Polozka> listPolozka = new ArrayList<Polozka>();
		for (int pol = 1; pol <=  pocetPolozek; pol++) {
			
			//<changeSubjectIdRequest><polozky><polozka>
			Polozka polozka = this._objectFactory.createChangeSubjectIdRequestPolozkyPolozka();
			polozka.setId(UUID.randomUUID().toString());
			
			BigDecimal cds = new BigDecimal(Math.random()*99999999999d);
			polozka.setCds(cds.toBigInteger().toString());
			
			int posunVDatu = (int)(Math.random()*10) * ((int)(Math.random()*2) == 0 ? -1 : 1);
			LocalDateTime today = LocalDateTime.now();
			polozka.setDatumZmeny(
					this.convertFromDateToDatetime(
							this.convertFromLocalDateTimeToDate(
									today.plusDays(posunVDatu))));

//			polozka.setDatumZmeny(
//					this.convertFromDateToDatetime(
//							new java.util.Date()));
			
			switch ((int)(Math.random()*2)) {
				case 0:
					CisloPojistence cisloPojistence = this._objectFactory.createChangeSubjectIdRequestPolozkyPolozkaCisloPojistence();

					switch ((int)(Math.random()*2)) {
						case 0:
							String puvodniHodnota = String.format("%s0100000000", pol);
							cisloPojistence.setPuvodniHodnota(puvodniHodnota.substring(0,10));
							break;
						case 1:
						default:
							break;
					}

					switch ((int)(Math.random()*2)) {
						case 0:
							String novaHodnota = String.format("%s0200000000", pol);
							cisloPojistence.setNovaHodnota(novaHodnota.substring(0,10));
							break;
						case 1:
						default:
							break;
					}
					
					polozka.setCisloPojistence(cisloPojistence);
					break;
				case 1:
				default:
					break;
			}

			switch ((int)(Math.random()*2)) {
				case 0:
					RodneCislo rodneCislo = this._objectFactory.createChangeSubjectIdRequestPolozkyPolozkaRodneCislo();
	
					switch ((int)(Math.random()*2)) {
						case 0:
							String puvodniHodnota = String.format("%s0300000000", pol);
							rodneCislo.setPuvodniHodnota(puvodniHodnota.substring(0,10));
							break;
						case 1:
						default:
							break;
					}
	
					switch ((int)(Math.random()*2)) {
						case 0:
							String novaHodnota = String.format("%s0400000000", pol);
							rodneCislo.setNovaHodnota(novaHodnota.substring(0,10));
							break;
						case 1:
						default:
							break;
					}
					
					polozka.setRodneCislo(rodneCislo);
					break;
				case 1:
				default:
					break;
			}

			switch ((int)(Math.random()*2)) {
				case 0:
					EvidencniCisloPojistence evidencniCisloPojistence = this._objectFactory.createChangeSubjectIdRequestPolozkyPolozkaEvidencniCisloPojistence();
	
					switch ((int)(Math.random()*2)) {
						case 0:
							String puvodniHodnota = String.format("%s0100000000", pol);
							evidencniCisloPojistence.setPuvodniHodnota(puvodniHodnota.substring(0,10));
							break;
						case 1:
						default:
							break;
					}
	
					switch ((int)(Math.random()*2)) {
						case 0:
							String novaHodnota = String.format("%s0200000000", pol);
							evidencniCisloPojistence.setNovaHodnota(novaHodnota.substring(0,10));
							break;
						case 1:
						default:
							break;
					}
					
					polozka.setEvidencniCisloPojistence(evidencniCisloPojistence);
					break;
				case 1:
				default:
					break;
			}
			
			switch ((int)(Math.random()*2)) {
				case 0:
					switch ((int)(Math.random()*2)) {
						case 0:
							polozka.setPreferovanyIdentifikatorCSSZ(PreferovanyIdentifikatorCSSZEnum.R.getHodnota());
							break;
						case 1:
							polozka.setPreferovanyIdentifikatorCSSZ(PreferovanyIdentifikatorCSSZEnum.E.getHodnota());
							break;
						default:
							break;
					}
					break;
				case 1:
				default:
					break;
			}

			listPolozka.add(polozka);
		}
		
		//<changeSubjectIdRequest><Polozky>
		ChangeSubjectIdRequest.Polozky polozky = this._objectFactory.createChangeSubjectIdRequestPolozky();
		polozky.polozka = listPolozka;
		
		//<changeSubjectIdRequest>
		ChangeSubjectIdRequest changeSubjectIdRequest = this._objectFactory.createChangeSubjectIdRequest();
		changeSubjectIdRequest.setHeader(header);
		changeSubjectIdRequest.setPolozky(polozky);
		
		return changeSubjectIdRequest;
	
	}

	
}
