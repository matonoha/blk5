package adis.bnk.par.changeSubjectId.response;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;

import org.xml.sax.SAXException;

import adis.bnk.par.XmlBase;
import adis.bnk.par.changeSubjectId.response.ChangeSubjectIdResponse.Header;
import adis.bnk.par.changeSubjectId.response.ChangeSubjectIdResponse.Header.Parameters;
import adis.bnk.par.changeSubjectId.response.ChangeSubjectIdResponse.Header.Parameters.Parameter;
import adis.bnk.par.changeSubjectId.response.ChangeSubjectIdResponse.Polozky.Polozka;
import adis.bnk.par.db.TXParOdDsOdp;
import adis.bnk.par.db.TXParOdDsOdpCollection;
import adis.bnk.par.db.TXParOddoXml;

/**
 * @author valenta
 */
public class XmlChangeSubjectIdResponse extends XmlBase<ChangeSubjectIdResponse> {

	private final static String CONSUMER_ID = "IDR";
	private final static String PROVIDER_ID = "ADIS";
	private final static int PROCESSING_PRIORITY = 0;
	private final static String VERSION = "1.2";
	private final static String PARAMETER_KEY_FU = "FU";
	private final static String PARAMETER_KEY_ORIGINATOR = "ORIGINATOR";
	
	private ObjectFactory _objectFactory = new ObjectFactory();
	private int _cZaznamu;
	private TXParOddoXml _tXParOddoXml;
	
    public enum OriginatorEnum {
    	Z ("ZP"),
    	S ("CSSZ");
    	
    	private final String _hodnota;
    	
    	public String getHodnota() {
    		return _hodnota;
    	}
    	
    	OriginatorEnum (String hodnota) {
    		this._hodnota = hodnota;
    	}
    	
    	public static OriginatorEnum getByHodnota(String hodnota) throws Exception {
    		switch(hodnota) {
    		case "ZP":
    			return Z;
    		case "CSSZ":
    			return S;
    		default:
    			throw new Exception("Neznamý typ hodnoty.");
    		}
    	}
	}
	
	
	/**
	 * @param conn
	 * @param cZaznamu
	 */
	public XmlChangeSubjectIdResponse(Connection conn, int cZaznamu) {
		super(conn);
		this._cZaznamu = cZaznamu;
		
		this.set_xsdSchemaURL(
				this.getClass().getResource(
						"/adis/bnk/par/schema/ADIS_IA_ChangeSubjectId-P2_Response_v1.2.xsd"));
		
	}
	
	/**
	 * verze metody, ktera v kazdem cyklu nacita hodnoty TXParOdDuvukon pro c_zaznamu a message_id
	 * 
	 * @return
	 * @throws SQLException
	 * @throws DatatypeConfigurationException
	 */
	public ChangeSubjectIdResponse getResponseOld() 
			throws SQLException, DatatypeConfigurationException {
		
		this._tXParOddoXml = new TXParOddoXml(this._conn, this._cZaznamu);
		this._tXParOddoXml.setPropertiesForCZaznamu();
		
		//<changeSubjectIdResponse><header>
		Header header = this._objectFactory.createChangeSubjectIdResponseHeader();
		header.setMessageId(UUID.randomUUID().toString());
		header.setTimestamp(this.convertFromDateToDatetime(new java.util.Date()));
		header.setCorrelationId(this._tXParOddoXml.getCorrelationId());
		header.setConsumerId(CONSUMER_ID);
		header.setProviderId(PROVIDER_ID);
		header.setProcessingPriority(PROCESSING_PRIORITY);
		header.setVersion(VERSION);
		
		//<changeSubjectIdResponse><header><parameters><parameter>
		List<Parameter> listParameter = new ArrayList<>();
		Parameter parameter1 = this._objectFactory.createChangeSubjectIdResponseHeaderParametersParameter();
		parameter1.setKey(PARAMETER_KEY_FU);
		parameter1.setValue(this._tXParOddoXml.getCFu());
		listParameter.add(parameter1);

		Parameter parameter2 = this._objectFactory.createChangeSubjectIdResponseHeaderParametersParameter();
		parameter2.setKey(PARAMETER_KEY_ORIGINATOR);
		parameter2.setValue(OriginatorEnum.valueOf(this._tXParOddoXml.getKodPrijodes()).getHodnota());
		listParameter.add(parameter2);
		
		//<changeSubjectIdResponse><header><parameters>
		Parameters parameters = this._objectFactory.createChangeSubjectIdResponseHeaderParameters();
		parameters.parameter = listParameter;
		
		header.setParameters(parameters);
		
		TXParOdDsOdpCollection tXParOdDsOdpCollection = new TXParOdDsOdpCollection(this._conn);
		tXParOdDsOdpCollection.fillForCZaznamu(this._cZaznamu);
		
		List<Polozka> listPolozka = new ArrayList<>();
		for (TXParOdDsOdp itemPolozka : tXParOdDsOdpCollection) {
			//<changeSubjectIdResponse><polozky><polozka>
			Polozka polozka = this._objectFactory.createChangeSubjectIdResponsePolozkyPolozka();
			
			polozka.setId(itemPolozka.getMessageId());
			polozka.setProcessingResult(itemPolozka.getKodOdp());

			listPolozka.add(polozka);
		}
		
		//<changeSubjectIdResponse><Polozky>
		ChangeSubjectIdResponse.Polozky polozky = this._objectFactory.createChangeSubjectIdResponsePolozky();
		polozky.polozka = listPolozka;
		
		//<changeSubjectIdResponse>
		ChangeSubjectIdResponse changeSubjectIdResponse = this._objectFactory.createChangeSubjectIdResponse();
		changeSubjectIdResponse.setHeader(header);
		changeSubjectIdResponse.setPolozky(polozky);
		
		return changeSubjectIdResponse;
	}

	/**
	 * verze metody, ktera nacita hodnoty z TXParOdDuvukon jednou pro c_zaznamu a provadi filtrovani 
	 * kolekce
	 * 
	 * @return
	 * @throws SQLException
	 * @throws DatatypeConfigurationException
	 */
	public ChangeSubjectIdResponse getResponse() 
			throws SQLException, DatatypeConfigurationException {
		
		this._tXParOddoXml = new TXParOddoXml(this._conn, this._cZaznamu);
		this._tXParOddoXml.setPropertiesForCZaznamu();

		//nacteni vsech hodnot z tabulky TXParOdDsOdp pro _cZaznamu
		TXParOdDsOdpCollection tXParOdDsOdpCollection = new TXParOdDsOdpCollection(this._conn);
		tXParOdDsOdpCollection.fillForCZaznamu(this._cZaznamu);

		//<changeSubjectIdResponse><header>
		Header header = this._objectFactory.createChangeSubjectIdResponseHeader();
		header.setMessageId(UUID.randomUUID().toString());
		header.setTimestamp(this.convertFromDateToDatetime(new java.util.Date()));
		header.setCorrelationId(this._tXParOddoXml.getCorrelationId());
		header.setConsumerId(CONSUMER_ID);
		header.setProviderId(PROVIDER_ID);
		header.setProcessingPriority(PROCESSING_PRIORITY);
		header.setVersion(VERSION);
		
		//<changeSubjectIdResponse><header><parameters><parameter>
		List<Parameter> listParameter = new ArrayList<>();
		Parameter parameter1 = this._objectFactory.createChangeSubjectIdResponseHeaderParametersParameter();
		parameter1.setKey(PARAMETER_KEY_FU);
		parameter1.setValue(this._tXParOddoXml.getCFu());
		listParameter.add(parameter1);

		Parameter parameter2 = this._objectFactory.createChangeSubjectIdResponseHeaderParametersParameter();
		parameter2.setKey(PARAMETER_KEY_ORIGINATOR);
		parameter2.setValue(OriginatorEnum.valueOf(this._tXParOddoXml.getKodPrijodes()).getHodnota());
		listParameter.add(parameter2);
		
		//<changeSubjectIdResponse><header><parameters>
		Parameters parameters = this._objectFactory.createChangeSubjectIdResponseHeaderParameters();
		parameters.parameter = listParameter;
		
		header.setParameters(parameters);
		
		List<Polozka> listPolozka = new ArrayList<>();
		for (TXParOdDsOdp itemPolozka : tXParOdDsOdpCollection) {
			//<changeSubjectIdResponse><polozky><polozka>
			Polozka polozka = this._objectFactory.createChangeSubjectIdResponsePolozkyPolozka();
			
			polozka.setId(itemPolozka.getMessageId());
			polozka.setProcessingResult(itemPolozka.getKodOdp());
			
			listPolozka.add(polozka);
		}
		
		//<changeSubjectIdResponse><Polozky>
		ChangeSubjectIdResponse.Polozky polozky = this._objectFactory.createChangeSubjectIdResponsePolozky();
		polozky.polozka = listPolozka;
		
		//<changeSubjectIdResponse>
		ChangeSubjectIdResponse changeSubjectIdResponse = this._objectFactory.createChangeSubjectIdResponse();
		changeSubjectIdResponse.setHeader(header);
		changeSubjectIdResponse.setPolozky(polozky);
		
		return changeSubjectIdResponse;
	}
	
	/**
	 * @return
	 * @throws SQLException
	 * @throws DatatypeConfigurationException
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws JAXBException 
	 */
	public boolean createOrUpdateXmlToDb() throws SQLException, DatatypeConfigurationException, 
					IOException, JAXBException, SAXException {
		
		long startTime;
		long elapsedTime;

		//startTime = System.nanoTime();
		//ChangeSubjectIdResponse re = this.getResponseOld();
		//ByteArrayOutputStream baos = this.convertObjectToXmlOutputStream(re);
		//boolean result = this._tXParOddoXml.updateXmlOdesl(baos);
		//elapsedTime = System.nanoTime() - startTime;
		//System.out.println(String.format("By getResponseOld(). Total execution time in millis: %s", elapsedTime/1000000));
		
		startTime = System.nanoTime();
		ChangeSubjectIdResponse re = this.getResponse();
		ByteArrayOutputStream baos = this.convertObjectToXmlOutputStream(re);
		boolean result = this._tXParOddoXml.updateXmlOdesl(baos);
		elapsedTime = System.nanoTime() - startTime;
		System.out.println(String.format("By getResponse(). Total execution time in millis: %s", elapsedTime/1000000));

		baos.close();
		
		return result;
	}
	
	@Override
	protected void ValidateDuties() {
		
	}
}
