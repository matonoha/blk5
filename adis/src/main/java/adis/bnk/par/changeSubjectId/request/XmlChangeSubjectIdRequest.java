package adis.bnk.par.changeSubjectId.request;

import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;

import org.xml.sax.SAXException;

import adis.bnk.par.XmlBase;
import adis.bnk.par.db.TXParDoInf;
import adis.bnk.par.db.TXParOddoXml;
import adis.bnk.par.db.TpParTXParDoInf;

/**
 * @author valenta
 */
public class XmlChangeSubjectIdRequest extends XmlBase<ChangeSubjectIdRequest> {

	private final static String PARAMETER_KEY_FU = "FU";
	private final static String PARAMETER_KEY_ORIGINATOR = "ORIGINATOR";

	private ObjectFactory _objectFactory = new ObjectFactory();
	private int _cZaznamu;
	private TXParOddoXml _tXParOddoXml;
	
    public enum PreferovanyIdentifikatorCSSZEnum {
    	R ("rc"),
    	E ("ecp");
    	
    	private final String _hodnota;
    	
    	public String getHodnota() {
    		return _hodnota;
    	}
    	
    	PreferovanyIdentifikatorCSSZEnum (String hodnota) {
    		this._hodnota = hodnota;
    	}
    	
    	public static PreferovanyIdentifikatorCSSZEnum getByHodnota(String hodnota) throws Exception {
    		switch(hodnota) {
    		case "rc":
    			return R;
    		case "ecp":
    			return E;
    		default:
    			throw new Exception("Neznamý typ hodnoty.");
    		}
    	}
	}

    public enum OriginatorEnum {
    	Z ("ZP"),
    	S ("CSSZ");
    	
    	private final String _hodnota;
    	
    	public String getHodnota() {
    		return _hodnota;
    	}
    	
    	OriginatorEnum (String hodnota) {
    		this._hodnota = hodnota;
    	}
    	
    	public static OriginatorEnum getByHodnota(String hodnota) throws Exception {
    		switch(hodnota) {
    		case "ZP":
    			return Z;
    		case "CSSZ":
    			return S;
    		default:
    			throw new Exception("Neznamý typ hodnoty.");
    		}
    	}
	}
    
	
	/**
	 * @param conn
	 * @param cZaznamu
	 */
	public XmlChangeSubjectIdRequest(Connection conn, int cZaznamu) {
		super(conn);
		this._cZaznamu = cZaznamu;
		
		this.set_xsdSchemaURL(
				this.getClass().getResource(
						"/adis/bnk/par/schema/ADIS_IA_ChangeSubjectId-P1_Request_v1.16.xsd"));
		
	}
	
	/**
	 * @return
	 * @throws SQLException
	 * @throws DatatypeConfigurationException
	 * @throws SAXException 
	 * @throws JAXBException 
	 */
	public ChangeSubjectIdRequest getRequest() throws SQLException, DatatypeConfigurationException, JAXBException, SAXException {
		
		_tXParOddoXml = new TXParOddoXml(_conn, _cZaznamu);
		_tXParOddoXml.setPropertiesForCZaznamu();
		if (_tXParOddoXml.getXmlDosly() == null) {
			throw new NullPointerException("xml_dosly");
		}
		
		Blob blob = _tXParOddoXml.getXmlDosly();
		int blobLength = (int) blob.length();  
		byte[] blobAsBytes = blob.getBytes(1, blobLength);
		//blob.free();
		
		//Helper.writeFile("e:/temp/pokus2.xml", blobAsBytes);
		
		ChangeSubjectIdRequest changeSubjectIdRequest = this._objectFactory.createChangeSubjectIdRequest();
		changeSubjectIdRequest = this.convertBytesToObject(blobAsBytes, changeSubjectIdRequest);
		return changeSubjectIdRequest;
	}

	/**
	 * @return
	 * @throws Exception 
	 */
	public boolean createOrUpdateXmlToDb() throws Exception {
		
		boolean result = false;
		
		long startTime;
		long elapsedTime;

//		startTime = System.nanoTime();
//		result = this.processRequest();
//		elapsedTime = System.nanoTime() - startTime;
//		System.out.println(String.format("By statement. Total execution time in millis: %s", elapsedTime/1000000));

		startTime = System.nanoTime();
		result = this.processRequestDeleteInsertBatch();
		elapsedTime = System.nanoTime() - startTime;
		System.out.println(String.format("By deleteInsertBatch. Total execution time in millis: %s", elapsedTime/1000000));
		
		return result;
	}

	
	private boolean processRequest() throws Exception {
		
		ChangeSubjectIdRequest changeSubjectIdRequest = this.getRequest();

		boolean result = false;

		this._tXParOddoXml.updateCorrelationIdCFuKodPrijodes(
				getCorrelationId(changeSubjectIdRequest),
				getCFu(changeSubjectIdRequest),
				getKodPrijodes(changeSubjectIdRequest)
				);
		for(ChangeSubjectIdRequest.Polozky.Polozka polozka : changeSubjectIdRequest.getPolozky().getPolozka()) {
			
			int cZaznamu = this._tXParOddoXml.getCZaznamu();
			
			TXParDoInf tXParDoInf = new TXParDoInf(this._conn, cZaznamu, polozka);
			result = tXParDoInf.insertOrUpdate();
			
		}
		
		return result;
		
	}
	
	private boolean processRequestDeleteInsertBatch() throws Exception {

		ChangeSubjectIdRequest changeSubjectIdRequest = this.getRequest();
		
		boolean result = false;
		boolean resultAll = true;

		int cZaznamu = this._tXParOddoXml.getCZaznamu();
		this._tXParOddoXml.updateCorrelationIdCFuKodPrijodes(
				getCorrelationId(changeSubjectIdRequest),
				getCFu(changeSubjectIdRequest),
				getKodPrijodes(changeSubjectIdRequest)
				);

//		long startTime;
//		long elapsedTime;
		
		int pocetZaznamuVDavceTXParDoInf = 1;
		int maxPocetZaznamuVDavceTXParDoInf = 10000;

		ArrayList<TpParTXParDoInf> dataDoInf = new ArrayList<TpParTXParDoInf>();
		for(ChangeSubjectIdRequest.Polozky.Polozka polozka : changeSubjectIdRequest.getPolozky().getPolozka()) {
			
			String messageId = polozka.getId();
			BigDecimal cDsIdr = polozka.getCds() != null 
					? new BigDecimal(polozka.getCds()) 
					: null;
			String cPoj = polozka.getCisloPojistence() != null && polozka.getCisloPojistence().getNovaHodnota() != null
					? polozka.getCisloPojistence().getNovaHodnota() 
					: null;
			String rodneCislo = polozka.getRodneCislo() != null && polozka.getRodneCislo().getNovaHodnota() != null
					? polozka.getRodneCislo().getNovaHodnota() 
					: null;
			String ecp = polozka.getEvidencniCisloPojistence() != null && polozka.getEvidencniCisloPojistence().getNovaHodnota() != null
					? polozka.getEvidencniCisloPojistence().getNovaHodnota() 
					: null;
			String cPojPuv = polozka.getCisloPojistence() != null && polozka.getCisloPojistence().getPuvodniHodnota() != null
					? polozka.getCisloPojistence().getPuvodniHodnota() 
					: null;
			String rodneCisloPuv = polozka.getRodneCislo() != null && polozka.getRodneCislo().getPuvodniHodnota() != null
					? polozka.getRodneCislo().getPuvodniHodnota() 
					: null;
			String ecpPuv = polozka.getEvidencniCisloPojistence() != null && polozka.getEvidencniCisloPojistence().getPuvodniHodnota() != null
					? polozka.getEvidencniCisloPojistence().getPuvodniHodnota() 
					: null;
			Date dZmena = polozka.getDatumZmeny() != null
					? polozka.getDatumZmeny().toGregorianCalendar().getTime() 
					: null;
			String prefCssz = polozka.getPreferovanyIdentifikatorCSSZ() != null
					? PreferovanyIdentifikatorCSSZEnum.getByHodnota(polozka.getPreferovanyIdentifikatorCSSZ()).toString()
					: null;
			
	    	dataDoInf.add(
					new TpParTXParDoInf(
							cZaznamu, 
							messageId, 
							cDsIdr, 
							cPoj, 
							rodneCislo, 
							ecp, 
							cPojPuv, 
							rodneCisloPuv, 
							ecpPuv, 
							dZmena,
							prefCssz,
				    		null,
				    		null,
				    		null,
							null,
				    		null));

	    	pocetZaznamuVDavceTXParDoInf++;

			if(pocetZaznamuVDavceTXParDoInf > maxPocetZaznamuVDavceTXParDoInf) {
//				startTime = System.nanoTime();
				result = TXParDoInf.insertOrUpdateByDeleteInsertBatch(this._conn, dataDoInf);
//				elapsedTime = System.nanoTime() - startTime;
//				System.out.println(String.format("1. Processed %s records. Total execution time in millis: %s", data.size(), elapsedTime/1000000));
				
				dataDoInf.clear();
				pocetZaznamuVDavceTXParDoInf = 1;
				
				if (!result) {
					resultAll = result;
				}
			}
			
		}

		if (dataDoInf.size() > 0) {
//			startTime = System.nanoTime();
			result = TXParDoInf.insertOrUpdateByDeleteInsertBatch(this._conn, dataDoInf);
//			elapsedTime = System.nanoTime() - startTime;
//			System.out.println(String.format("2. Processed %s records. Total execution time in millis: %s", data.size(), elapsedTime/1000000));

			if (!result) {
				resultAll = result;
			}
		}
			
		return resultAll; 
	}
	
	private String getCorrelationId(final ChangeSubjectIdRequest changeSubjectIdRequest) {
		return changeSubjectIdRequest.getHeader().getCorrelationId();
	}

	private String getCFu(final ChangeSubjectIdRequest changeSubjectIdRequest) {
		String cFu = null;
		if (changeSubjectIdRequest.getHeader().getParameters() != null ) {
			cFu = 
					changeSubjectIdRequest.getHeader().getParameters().getParameter()
					.stream()
					.filter(x -> x.key.equals(PARAMETER_KEY_FU))
					.findFirst().get().getValue();
		}

		return cFu;
	}

	private String getKodPrijodes(final ChangeSubjectIdRequest changeSubjectIdRequest) throws Exception {
		String kodPrijodes = null;
		if (changeSubjectIdRequest.getHeader().getParameters() != null ) {
			String originator = 
					changeSubjectIdRequest.getHeader().getParameters().getParameter()
					.stream()
					.filter(x -> x.key.equals(PARAMETER_KEY_ORIGINATOR))
					.findFirst().get().getValue();
			
			if (originator != null) {
				kodPrijodes = OriginatorEnum.getByHodnota(originator).toString();
			}
		}

		return kodPrijodes;
	}
	
	
	@Override
	protected void ValidateDuties() {
		
	}
	
}
