package adis.bnk.par;

import java.sql.Connection;

import adis.bnk.par.changeSubjectId.request.XmlChangeSubjectIdRequest;
import adis.bnk.par.changeSubjectId.response.XmlChangeSubjectIdResponse;
import adis.bnk.par.checkSelfEmpl.request.JsonCheckSelfEmplRequest;
import adis.bnk.par.checkSelfEmpl.request.XmlCheckSelfEmplRequest;
import adis.bnk.par.checkSelfEmpl.response.XmlCheckSelfEmplResponse;
import adis.bnk.par.db.TXParOddoXml;
import adis.bnk.par.enteredFlatRate.request.XmlEnteredFlatRateRequest;
import adis.bnk.par.enteredFlatRate.response.XmlEnteredFlatRateResponse;
import adis.bnk.par.providePaymentDetails.request.XmlProvidePaymentDetailsRequest;
import adis.bnk.par.providePaymentDetails.response.XmlProvidePaymentDetailsResponse;
import adis.bnk.par.terminateFlatRate.request.XmlTerminateFlatRateRequest;
import adis.bnk.par.terminateFlatRate.response.XmlTerminateFlatRateResponse;



/**
 * @author valenta Zpracovani volani pro ulohu PAR - volani zpracovani jednotlivych ukolu ulohy se
 *         ridi paramatery v tabulce t_x_oddo_xml (typ_zazn, typ_soub)
 */
public class ZpracovaniPAR {

//	private final static org.apache.commons.logging.Log log = adis.slib.aslib_lib1.LogUtils.getLog(
//			ZpracovaniPAR.class.getName());

	/**
	 * @author valenta Na zaklade vlozeni parametru pro typZaznamu a typSouboru (t_x_oddo_xml
	 *         (typ_zazn, typ_soub)) vraci parametr pro volani prislusneho zpracovani
	 */
    public enum ProcessedSchemaEnum {
    	/**
    	 * 
    	 */
    	CHECK_SELF_EMPL_REQUEST ("O", "O"), //IDR-INT_IA_
    	/**
    	 * 
    	 */
    	CHECK_SELF_EMPL_RESPONSE ("D", "O"), //IDR-INT_IA_
    	/**
    	 * 
    	 */
    	ENTERED_FLAT_RATE_REQUEST ("O", "V"), //IDR-INT_IA_
    	/**
    	 * 
    	 */
    	ENTERED_FLAT_RATE_RESPONSE ("D", "V"), //IDR-INT_IA_
    	/**
    	 * 
    	 */
    	PROVIDE_PAYMENT_DETAILS_REQUEST ("O", "R"), //IDR-INT_IA_
    	/**
    	 * 
    	 */
    	PROVIDE_PAYMENT_DETAILS_RESPONSE ("D", "R"), //IDR-INT_IA_
    	/**
    	 * 
    	 */
    	TERMINATE_FLAT_RATE_REQUEST ("O", "U"), //IDR-INT_IA_
    	/**
    	 * 
    	 */
    	TERMINATE_FLAT_RATE_RESPONSE ("D", "U"), //IDR-INT_IA_
    	/**
    	 * 
    	 */
    	CHANGE_SUBJECT_ID_REQUEST ("D", "Z"), //ADIS_IA_
    	/**
    	 * 
    	 */
    	CHANGE_SUBJECT_ID_RESPONSE ("O", "Z"); //ADIS_IA_
    	
    	private final String _typZaznamu;
    	private final String _typSouboru;
    	
    	String getTypZaznamu() {
    		return this._typZaznamu;
    	}

    	String getTypSouboru() {
    		return this._typSouboru;
    	}
    	
    	ProcessedSchemaEnum (String typZaznamu, String typSouboru) {
    		this._typZaznamu = typZaznamu;
    		this._typSouboru = typSouboru;
    	}
    	
		/**
		 * @param typZaznamu
		 * @param typSouboru
		 * @return ProcessedSchemaEnum
		 * @throws Exception
		 */
    	public static ProcessedSchemaEnum getByParam(String typZaznamu, String typSouboru) throws Exception {
    		if (typZaznamu.equals("O") && typSouboru.equals("O")) {
    			return CHECK_SELF_EMPL_REQUEST;
    		} else if (typZaznamu.equals("D") && typSouboru.equals("O")) {
    			return CHECK_SELF_EMPL_RESPONSE;
    		} else if (typZaznamu.equals("O") && typSouboru.equals("V")) {
    			return ENTERED_FLAT_RATE_REQUEST;
    		} else if (typZaznamu.equals("D") && typSouboru.equals("V")) {
    			return ENTERED_FLAT_RATE_RESPONSE;
    		} else if (typZaznamu.equals("O") && typSouboru.equals("R")) {
    			return PROVIDE_PAYMENT_DETAILS_REQUEST;
    		} else if (typZaznamu.equals("D") && typSouboru.equals("R")) {
    			return PROVIDE_PAYMENT_DETAILS_RESPONSE;
    		} else if (typZaznamu.equals("O") && typSouboru.equals("U")) {
    			return TERMINATE_FLAT_RATE_REQUEST;
    		} else if (typZaznamu.equals("D") && typSouboru.equals("U")) {
    			return TERMINATE_FLAT_RATE_RESPONSE;
    		} else if (typZaznamu.equals("D") && typSouboru.equals("Z")) {
    			return CHANGE_SUBJECT_ID_REQUEST;
    		} else if (typZaznamu.equals("O") && typSouboru.equals("Z")) {
    			return CHANGE_SUBJECT_ID_RESPONSE;
    		} else {
    			throw new Exception("Neznamy typ hodnoty.");
    		}
    	}
    }
	
	private Connection _conn;
	private ProcessedSchemaEnum _zpracovavaneSchema;
	private int _cZaznamu;
	
	/**
	 * Vraci schema pro ktere je zpracovavano volani
	 * 
	 * @return ProcessedSchemaEnum
	 */
	public ProcessedSchemaEnum getZpracovavaneSchema() {
		return _zpracovavaneSchema;
	}

	/**
	 * Vraci c_zaznamu
	 * 
	 * @return cZaznamu
	 */
	public int getCZaznamu() {
		return _cZaznamu;
	}
	
	
//	public pracovaniPAR(SpojeniInformix ro_conn) {
//		this(ro_conn.VratConn());
//	}

	/**
	 * Konstruktor tridy
	 * 
	 * @param conn
	 * @param cZaznamu
	 * @throws Exception
	 */
	public ZpracovaniPAR(Connection conn, int cZaznamu) throws Exception {
		this._conn = conn;
		this._cZaznamu = cZaznamu;
		
		TXParOddoXml tXParOddoXml = new TXParOddoXml(this._conn, this._cZaznamu);
		tXParOddoXml.setPropertiesForCZaznamu();
		this._zpracovavaneSchema = ProcessedSchemaEnum.getByParam(tXParOddoXml.getTypZazn(), tXParOddoXml.getTypSoub());
	}
	
	/**
	 * Zpracovani volani pro ZpracovaniPAR na zaklade parametru ziskanych z tabulky t_x_par_oddo_xml
	 * 
	 * @throws Exception
	 */
	public void zpracovani() throws Exception {
		
		boolean autoCommit = this._conn.getAutoCommit();

		try {
			this._conn.setAutoCommit(false);
			
			switch(this._zpracovavaneSchema) {
				//IDR-INT_IA_
				case CHECK_SELF_EMPL_REQUEST:
					XmlCheckSelfEmplRequest checkSelfEmplRequest = new XmlCheckSelfEmplRequest(this._conn, this._cZaznamu);
					checkSelfEmplRequest.createOrUpdateXmlToDb();
					
					System.out.println(String.format("Zpracovan c_zaznam: %s pro %s.", this.getCZaznamu(), this._zpracovavaneSchema));
//					if (log.isDebugEnabled()) {
//						log.debug(String.format("Zpracovan c_zaznam: %s pro %s.",
//								this.getCZaznamu(), this._zpracovavaneSchema));
//					}
					
					break;
				//IDR-INT_IA_
				case CHECK_SELF_EMPL_RESPONSE:
					XmlCheckSelfEmplResponse checkSelfEmplResponse = new XmlCheckSelfEmplResponse(this._conn, this._cZaznamu);
					checkSelfEmplResponse.createOrUpdateXmlToDb();
					
					System.out.println(String.format("Zpracovan c_zaznam: %s pro %s.", this.getCZaznamu(), this._zpracovavaneSchema));
//					if (log.isDebugEnabled()) {
//					log.debug(String.format("Zpracovan c_zaznam: %s pro %s.",
//							this.getCZaznamu(), this._zpracovavaneSchema));
//				}
					break;
				//IDR-INT_IA_
				case ENTERED_FLAT_RATE_REQUEST:
					XmlEnteredFlatRateRequest flatRateRequest = new XmlEnteredFlatRateRequest(this._conn, this._cZaznamu);
					flatRateRequest.createOrUpdateXmlToDb();
					
					System.out.println(String.format("Zpracovan c_zaznam: %s pro %s.", this.getCZaznamu(), this._zpracovavaneSchema));
//					if (log.isDebugEnabled()) {
//					log.debug(String.format("Zpracovan c_zaznam: %s pro %s.",
//							this.getCZaznamu(), this._zpracovavaneSchema));
//				}
					break;
				//IDR-INT_IA_
				case ENTERED_FLAT_RATE_RESPONSE:
					XmlEnteredFlatRateResponse flatRateResponse = new XmlEnteredFlatRateResponse(this._conn, this._cZaznamu);
					flatRateResponse.createOrUpdateXmlToDb();
					
					System.out.println(String.format("Zpracovan c_zaznam: %s pro %s.", this.getCZaznamu(), this._zpracovavaneSchema));
//					if (log.isDebugEnabled()) {
//					log.debug(String.format("Zpracovan c_zaznam: %s pro %s.",
//							this.getCZaznamu(), this._zpracovavaneSchema));
//				}
					break;
				//IDR-INT_IA_
				case PROVIDE_PAYMENT_DETAILS_REQUEST:
					XmlProvidePaymentDetailsRequest providePaymentDetailsRequest = new XmlProvidePaymentDetailsRequest(this._conn, this._cZaznamu);
					providePaymentDetailsRequest.createOrUpdateXmlToDb();
					
					System.out.println(String.format("Zpracovan c_zaznam: %s pro %s.", this.getCZaznamu(), this._zpracovavaneSchema));
//					if (log.isDebugEnabled()) {
//					log.debug(String.format("Zpracovan c_zaznam: %s pro %s.",
//							this.getCZaznamu(), this._zpracovavaneSchema));
//				}
					break;
				//IDR-INT_IA_
				case PROVIDE_PAYMENT_DETAILS_RESPONSE:
					XmlProvidePaymentDetailsResponse providePaymentDetailsResponse = new XmlProvidePaymentDetailsResponse(this._conn, this._cZaznamu);
					providePaymentDetailsResponse.createOrUpdateXmlToDb();
					
					System.out.println(String.format("Zpracovan c_zaznam: %s pro %s.", this.getCZaznamu(), this._zpracovavaneSchema));
//					if (log.isDebugEnabled()) {
//					log.debug(String.format("Zpracovan c_zaznam: %s pro %s.",
//							this.getCZaznamu(), this._zpracovavaneSchema));
//				}
					break;
				//IDR-INT_IA_
				case TERMINATE_FLAT_RATE_REQUEST:
					XmlTerminateFlatRateRequest terminateFlatRateRequest = new XmlTerminateFlatRateRequest(this._conn, this._cZaznamu);
					terminateFlatRateRequest.createOrUpdateXmlToDb();
					
					System.out.println(String.format("Zpracovan c_zaznam: %s pro %s.", this.getCZaznamu(), this._zpracovavaneSchema));
//					if (log.isDebugEnabled()) {
//					log.debug(String.format("Zpracovan c_zaznam: %s pro %s.",
//							this.getCZaznamu(), this._zpracovavaneSchema));
//				}
					break;
				//IDR-INT_IA_
				case TERMINATE_FLAT_RATE_RESPONSE:
					XmlTerminateFlatRateResponse terminateFlatRateResponse = new XmlTerminateFlatRateResponse(this._conn, this._cZaznamu);
					terminateFlatRateResponse.createOrUpdateXmlToDb();
					
					System.out.println(String.format("Zpracovan c_zaznam: %s pro %s.", this.getCZaznamu(), this._zpracovavaneSchema));
//					if (log.isDebugEnabled()) {
//					log.debug(String.format("Zpracovan c_zaznam: %s pro %s.",
//							this.getCZaznamu(), this._zpracovavaneSchema));
//				}
					break;
				//ADIS_IA_
				case CHANGE_SUBJECT_ID_REQUEST:
					XmlChangeSubjectIdRequest changeSubjectIdRequest = new XmlChangeSubjectIdRequest(this._conn, this._cZaznamu);
					changeSubjectIdRequest.createOrUpdateXmlToDb();
					
					System.out.println(String.format("Zpracovan c_zaznam: %s pro %s.", this.getCZaznamu(), this._zpracovavaneSchema));
//						if (log.isDebugEnabled()) {
//						log.debug(String.format("Zpracovan c_zaznam: %s pro %s.",
//								this.getCZaznamu(), this._zpracovavaneSchema));
//					}
					break;
				//ADIS_IA_
				case CHANGE_SUBJECT_ID_RESPONSE:
					XmlChangeSubjectIdResponse changeSubjectIdResponse = new XmlChangeSubjectIdResponse(this._conn, this._cZaznamu);
					changeSubjectIdResponse.createOrUpdateXmlToDb();
					
					System.out.println(String.format("Zpracovan c_zaznam: %s pro %s.", this.getCZaznamu(), this._zpracovavaneSchema));
//						if (log.isDebugEnabled()) {
//						log.debug(String.format("Zpracovan c_zaznam: %s pro %s.",
//								this.getCZaznamu(), this._zpracovavaneSchema));
//					}
					break;
				default :
					throw new Exception("Neznamy typ hodnoty pro zpracovani PAR");
			}

			this._conn.commit();
		} catch (Exception exc) {
			this._conn.rollback();
			throw exc;
		} finally {
			this._conn.setAutoCommit(autoCommit);
		}
	}
	
	/**
	 * @throws Exception
	 */
	public void zpracovaniJson() throws Exception {
		
		boolean autoCommit = this._conn.getAutoCommit();

		try {
			this._conn.setAutoCommit(false);
			
			switch(this._zpracovavaneSchema) {
				case CHECK_SELF_EMPL_REQUEST:
					JsonCheckSelfEmplRequest checkSelfEmplRequest = new JsonCheckSelfEmplRequest(this._conn, this._cZaznamu);
					checkSelfEmplRequest.createOrUpdateJsonToDb();
					
					System.out.println(String.format("Zpracovan c_zaznam: %s pro %s.", this.getCZaznamu(), this._zpracovavaneSchema));
					break;
				case CHECK_SELF_EMPL_RESPONSE:
					throw new Exception("Not implemented.");
//					XmlCheckSelfEmplResponse checkSelfEmplResponse = new XmlCheckSelfEmplResponse(this._conn, this._cZaznamu);
//					checkSelfEmplResponse.createOrUpdateXmlToDb();
//					
//					System.out.println(String.format("Zpracovan c_zaznam: %s pro %s.", this.getCZaznamu(), this._zpracovavaneSchema));
//					break;
				case ENTERED_FLAT_RATE_REQUEST:
					throw new Exception("Not implemented.");
//					XmlEnteredFlatRateRequest flatRateRequest = new XmlEnteredFlatRateRequest(this._conn, this._cZaznamu);
//					flatRateRequest.createOrUpdateXmlToDb();
//					
//					System.out.println(String.format("Zpracovani c_zaznam: %s pro %s.", this.getCZaznamu(), this._zpracovavaneSchema));
//					break;
				case ENTERED_FLAT_RATE_RESPONSE:
					throw new Exception("Not implemented.");
//					XmlEnteredFlatRateResponse flatRateResponse = new XmlEnteredFlatRateResponse(this._conn, this._cZaznamu);
//					flatRateResponse.createOrUpdateXmlToDb();
//					
//					System.out.println(String.format("Zpracovani c_zaznam: %s pro %s.", this.getCZaznamu(), this._zpracovavaneSchema));
//					break;
				case PROVIDE_PAYMENT_DETAILS_REQUEST:
					throw new Exception("Not implemented.");
//					XmlProvidePaymentDetailsRequest providePaymentDetailsRequest = new XmlProvidePaymentDetailsRequest(this._conn, this._cZaznamu);
//					providePaymentDetailsRequest.createOrUpdateXmlToDb();
//					
//					System.out.println(String.format("Zpracovani c_zaznam: %s pro %s.", this.getCZaznamu(), this._zpracovavaneSchema));
//					break;
				case PROVIDE_PAYMENT_DETAILS_RESPONSE:
					throw new Exception("Not implemented.");
//					XmlProvidePaymentDetailsResponse providePaymentDetailsResponse = new XmlProvidePaymentDetailsResponse(this._conn, this._cZaznamu);
//					providePaymentDetailsResponse.createOrUpdateXmlToDb();
//					
//					System.out.println(String.format("Zpracovani c_zaznam: %s pro %s.", this.getCZaznamu(), this._zpracovavaneSchema));
//					break;
				default :
					throw new Exception("Neznamy typ hodnoty pro zpracovani PAR");
			}

			this._conn.commit();
		} catch (Exception exc) {
			this._conn.rollback();
			throw exc;
		} finally {
			this._conn.setAutoCommit(autoCommit);
		}
	}
	
	
	
}
