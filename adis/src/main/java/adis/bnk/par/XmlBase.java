package adis.bnk.par;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;


/**
 * @author valenta
 *
 * @param <T>
 */
public abstract class XmlBase<T> {

	private String _xsdSchemaFile;
	
	/**
	 * 
	 */
	protected Connection _conn;
	
	/**
	 * @param conn
	 */
	public XmlBase(Connection conn) {
		this._conn = conn;
	}

	/**
	 * @param _xsdSchemaFile
	 */
	protected final void set_xsdSchemaFile(String _xsdSchemaFile) {
		this._xsdSchemaFile = _xsdSchemaFile;
	}

	/**
	 * @return
	 */
	public String get_xsdSchemaFile() {
		return _xsdSchemaFile;
	}

	private URL _xsdSchemaURL;

	/**
	 * @param _xsdSchemaURL
	 */
	protected final void set_xsdSchemaURL(URL _xsdSchemaURL) {
		this._xsdSchemaURL = _xsdSchemaURL;
	}

	/**
	 * @return
	 */
	public URL get_xsdSchemaURL() {
		return _xsdSchemaURL;
	}
	
	/**
	 * 
	 */
	protected void ValidateDuties() {
		
	}
	
	/**
	 * @param date
	 * @return
	 * @throws DatatypeConfigurationException
	 */
	protected XMLGregorianCalendar convertFromDateToDatetime(java.util.Date date) throws DatatypeConfigurationException {

		if (date == null) {
			return null;
		}
		
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		return DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
		
	}

	/**
	 * @param date
	 * @return
	 * @throws DatatypeConfigurationException
	 */
	protected XMLGregorianCalendar convertFromDateToDate(java.util.Date date) throws DatatypeConfigurationException {

		if (date == null) {
			return null;
		}
		
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		return DatatypeFactory.newInstance().newXMLGregorianCalendarDate(cal.get(java.util.Calendar.YEAR), cal.get(java.util.Calendar.MONTH)+1, cal.get(java.util.Calendar.DAY_OF_MONTH), DatatypeConstants.FIELD_UNDEFINED);
	}

	/**
	 * @param localDateTime
	 * @return
	 */
	protected java.util.Date convertFromLocalDateTimeToDate(java.time.LocalDateTime localDateTime) {
		return java.util.Date.from(localDateTime.atZone(java.time.ZoneId.systemDefault()).toInstant());
	}

	/**
	 * @param date
	 * @return
	 */
	protected java.time.LocalDateTime convertFromDateToLocalDateTime(java.util.Date date) {
		return java.time.LocalDateTime.ofInstant(date.toInstant(), java.time.ZoneId.systemDefault());
	}
	
	/**
	 * @param d
	 * @return
	 */
	private SimpleDateFormat rfc3339 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
	protected String convertFromDatetoRFC3339(java.util.Date d)
	{
	   return rfc3339.format(d).replaceAll("(\\d\\d)(\\d\\d)$", "$1:$2");
	}
	
	/**
	 * @param objectForCreateXml
	 * @return
	 * @throws JAXBException 
	 * @throws SAXException 
	 */
	public ByteArrayOutputStream convertObjectToXmlOutputStream(T objectForCreateXml) throws JAXBException, SAXException {

		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		// create JAXB context and instantiate marshaller
		JAXBContext jaxbContext = JAXBContext.newInstance(objectForCreateXml.getClass());
		//JAXBContext jaxbContext = JAXBContext.newInstance("net.javaguides.javaxmlparser.jaxb");
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

		//Setup schema validator
		if (_xsdSchemaURL != null) {
	        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
	        Schema employeeSchema = sf.newSchema(this.get_xsdSchemaURL());
	        jaxbMarshaller.setSchema(employeeSchema);

	        System.out.println(String.format("The XSD Schema '%s' was used to check XML file.", _xsdSchemaURL.getFile()));
		} else {
	        System.out.println(String.format("The XSD Schema was NOT used to check XML file."));
		}

		// Write to System.out - zobrazeni v Console
		//jaxbMarshaller.marshal(objectForCreateXml, System.out);
		// Write to System.out - zapsani do File
		//jaxbMarshaller.marshal(objectForCreateXml, new File("./target/pokus.xml"));
		//jaxbMarshaller.marshal(objectForCreateXml, new File(
		//		String.format("e:/temp/Balik05/test/pokus_%s.xml", (int) (Math.random() * 1000000))));

		// Write to OutputStream
		jaxbMarshaller.marshal(objectForCreateXml, byteArrayOutputStream);

		return byteArrayOutputStream;
	}
	
	/**
	 * @param byteArr
	 * @param objectForUnmarshallXml
	 * @return
	 * @throws SAXException 
	 * @throws JAXBException 
	 */
	public T convertBytesToObject(byte[] byteArr, T objectForUnmarshallXml) throws JAXBException, SAXException {
		InputStream inputStream = new ByteArrayInputStream(byteArr);
		return convertInputStreamToObject(inputStream, objectForUnmarshallXml);
	}
	
	/**
	 * @param inputStream
	 * @param objectForUnmarshallXml
	 * @return
	 * @throws JAXBException 
	 * @throws SAXException 
	 */
	public T convertInputStreamToObject(InputStream inputStream, T objectForUnmarshallXml) throws JAXBException, SAXException {
		
		JAXBContext jaxbContext = JAXBContext.newInstance(objectForUnmarshallXml.getClass());
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		

		//Setup schema validator
		if (_xsdSchemaURL != null) {
	        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
	        Schema employeeSchema = sf.newSchema(this.get_xsdSchemaURL());
	        jaxbUnmarshaller.setSchema(employeeSchema);
	        
	        System.out.println(String.format("The XSD Schema '%s' was used to check XML file.", _xsdSchemaURL.getFile()));
		} else {
	        System.out.println(String.format("The XSD Schema was NOT used to check XML file."));
		}
		
		
		objectForUnmarshallXml = (T) jaxbUnmarshaller.unmarshal(inputStream);

		return objectForUnmarshallXml;
	}	

	public Document getXmlDocument(byte[] byteArr) throws SAXException, IOException, ParserConfigurationException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		return builder.parse(new ByteArrayInputStream(byteArr));
	}
	
	protected void printRootElement(Document document) {
		//optional, but recommended
		document.getDocumentElement().normalize();
		
		Element rootElement = document.getDocumentElement();
		
		System.out.println("Root element: " + rootElement.getNodeName());
		
		if (rootElement.hasAttributes()) {
            NamedNodeMap nodeMap = rootElement.getAttributes();
            for (int j = 0; j < nodeMap.getLength(); j++) {
                Node attr = nodeMap.item(j);
    			System.out.println(String.format("Attribute name: %s, value: %s", attr.getNodeName(), attr.getNodeValue()));
            }
        }
		
		String value = rootElement.getAttribute("xsi:schemaLocation");
		System.out.println(String.format("xsi:schemaLocation value: %s", value));
		
		String[] schemaName = value.split(" ");
		if (schemaName.length > 0) {
			System.out.println(String.format("SchemaName: %s", schemaName[1]));
		}
		
	}
	
}
