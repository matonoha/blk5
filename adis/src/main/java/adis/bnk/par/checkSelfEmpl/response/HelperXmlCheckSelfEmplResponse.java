package adis.bnk.par.checkSelfEmpl.response;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.xml.datatype.DatatypeConfigurationException;

import adis.bnk.par.XmlBase;
import adis.bnk.par.checkSelfEmpl.response.CheckSelfEmplResponse.Subjekty.Subjekt;
import adis.bnk.par.checkSelfEmpl.response.CheckSelfEmplResponse.Subjekty.Subjekt.IntervalySvc;
import adis.bnk.par.checkSelfEmpl.response.CheckSelfEmplResponse.Subjekty.Subjekt.IntervalyZam;
import adis.bnk.par.db.TXParDoDs.UcastCsszEnum;
import adis.bnk.par.db.TXParDoDs.UcastNaVzpEnum;
import adis.bnk.par.db.TXParDoDs.VysledekZpracovaniEnum;

/**
 * @author valenta
 *
 *
 */
public class HelperXmlCheckSelfEmplResponse extends XmlBase<CheckSelfEmplResponse> {

	private ObjectFactory _objectFactory = new ObjectFactory();
	private final String version = "v1.9_";
	
	/**
	 * @param conn
	 * @param cZaznamu
	 */
	public HelperXmlCheckSelfEmplResponse(Connection conn) {
		super(conn);
		
		this.set_xsdSchemaURL(
				this.getClass().getResource("/adis/bnk/par/schema/IDR-INT_IA_CheckSelfEmpl-P2_Response_v1.9.xsd"));
		
	}

	public CheckSelfEmplResponse getTestResponse(int pocetSubjektu, int pocetIntervalySvcVSubjektu, int pocetIntervalyZamVSubjektu) throws DatatypeConfigurationException {

		//<checkSelfEmplResponse>
		CheckSelfEmplResponse checkSelfEmplResponse = this._objectFactory.createCheckSelfEmplResponse();
		checkSelfEmplResponse.setCorrelationId(UUID.randomUUID().toString());
		
		List<Subjekt> listSubjekt = new ArrayList<Subjekt>();
		for (int sub = 1; sub <=  pocetSubjektu; sub++) {
			//<checkSelfEmplResponse><Subjekty><Subjekt>
			Subjekt subjekt = this._objectFactory.createCheckSelfEmplResponseSubjektySubjekt();
			
			subjekt.setMessageId(UUID.randomUUID().toString());
			
			String aifo = String.format("aaaaaaaaaaaaaaaaaaaaaaaa%s", sub);
			subjekt.setAifo(aifo.substring(aifo.length() - 24));
			
			String rodneCislo = String.format("0000000000%s", sub); 
			subjekt.setRodneCislo(rodneCislo.substring(rodneCislo.length() - 10));
			
			String cisloPojistence = String.format("%s0000000000", sub);
			subjekt.setCisloPojistence(cisloPojistence.substring(0,10));
			
			String evidencniCisloPojistence = String.format("0%s000000000", sub);
			subjekt.setEvidencniCisloPojistence(evidencniCisloPojistence.substring(0,10));
			
			subjekt.setJmeno(String.format("%sJméno%s", version, sub));
			
			subjekt.setPrijmeni(String.format("%sPříjmení%s", version, sub));
			
			switch ((int)(Math.random()*2)) {
				case 0:
					subjekt.setRodnePrijmeni(String.format("%sRodnéPříjmení%s", version, sub));
					break;
				case 1:
				default:
					break;
			}
			subjekt.setDatumNarozeni(this.convertFromDateToDate(new java.util.Date()));

			if (pocetIntervalySvcVSubjektu > 0) {
				subjekt.intervalySvc = new ArrayList<>();
			}
			for (int svc = 1; svc <=  pocetIntervalySvcVSubjektu; svc++) {
				//<checkSelfEmplResponse><Subjekty><Subjekt><IntervalySvc>
				IntervalySvc intervalySvc = this._objectFactory.createCheckSelfEmplResponseSubjektySubjektIntervalySvc();
				
				intervalySvc.setOznameniZahajeniSvc(this.convertFromDateToDate(new java.util.Date()));
				intervalySvc.setZahajeniSvc(this.convertFromDateToDate(new java.util.Date()));

				switch ((int)(Math.random()*2)) {
				case 0:
					intervalySvc.setUkonceniSvc(this.convertFromDateToDate(new java.util.Date()));
					break;
				case 1:
				default:
					break;
				}
				intervalySvc.setDuvodUkonceniSvc(String.format("%sDůvod ukončení SVC pro sub=%s, svc=%s", version, sub, svc));
				
				subjekt.intervalySvc.add(intervalySvc);
			}

			if (pocetIntervalyZamVSubjektu > 0) {
				subjekt.intervalyZam = new ArrayList<>();
			}
			for (int zam = 1; zam <=  pocetIntervalyZamVSubjektu; zam++) {
				//<checkSelfEmplResponse><Subjekty><Subjekt><IntervalySvc>
				IntervalyZam intervalyZam = this._objectFactory.createCheckSelfEmplResponseSubjektySubjektIntervalyZam();
				
				intervalyZam.setZahajeniZam(this.convertFromDateToDate(new java.util.Date()));

				switch ((int)(Math.random()*2)) {
				case 0:
					intervalyZam.setUkonceniZam(this.convertFromDateToDate(new java.util.Date()));
					break;
				case 1:
				default:
					break;
				}
				
				intervalyZam.setOznameniZahajeniZam(this.convertFromDateToDate(new java.util.Date()));
				intervalyZam.setOznameniUkonceniZam(this.convertFromDateToDate(new java.util.Date()));
				intervalyZam.setTypPracovnihoPomeru(String.format("%sTrvalý pro sub=%s, zam=%s", version, sub, zam));
				intervalyZam.setNazevZam(String.format("%sNázev zaměstnavatele pro sub=%s, zam=%s", version, sub, zam));
				
				switch ((int)(Math.random()*2)) {
				case 0:
					String ico = String.format("%s%s%s00000000000000000000000000000000000", version, sub, zam);
					intervalyZam.setIcoZam(ico.substring(0, ((int)(Math.random()*34)+1)));
					break;
				case 1:
				default:
					break;
				}

				intervalyZam.setAdresaZam(String.format("%sAdresa zaměstnavatele pro sub=%s, zam=%s", version, sub, zam));
				
				subjekt.intervalyZam.add(intervalyZam);
			}
			
			subjekt.setKodZpracovani(this._objectFactory.createCheckSelfEmplResponseSubjektySubjektKodZpracovani());
			subjekt.kodZpracovani.setZp(this._objectFactory.createCheckSelfEmplResponseSubjektySubjektKodZpracovaniZp());
			subjekt.kodZpracovani.zp.setKod(UcastNaVzpEnum.values()[(int)(Math.random()*5)].getHodnota());
			switch ((int)(Math.random()*2)) {
				case 0:
					String duvod = String.format("%s%s00000000000000000000000000000000000", version, sub);
					subjekt.kodZpracovani.zp.setDuvod(duvod);
					break;
				case 1:
				default:
					break;
			}
			switch ((int)(Math.random()*2)) {
				case 0:
					String popis = String.format("%s%s00000000000000000000000000000000000", version, sub);
					subjekt.kodZpracovani.zp.setPopis(popis);
					break;
				case 1:
				default:
					break;
			}
			
			
			subjekt.kodZpracovani.setCssz(this._objectFactory.createCheckSelfEmplResponseSubjektySubjektKodZpracovaniCssz());
			subjekt.kodZpracovani.setCssz(this._objectFactory.createCheckSelfEmplResponseSubjektySubjektKodZpracovaniCssz());
			subjekt.kodZpracovani.cssz.setKod(UcastCsszEnum.values()[(int)(Math.random()*6)].getHodnota());
			switch ((int)(Math.random()*2)) {
				case 0:
					String duvod = String.format("%s%s00000000000000000000000000000000000", version, sub);
					subjekt.kodZpracovani.cssz.setDuvod(duvod);
					break;
				case 1:
				default:
					break;
			}
			switch ((int)(Math.random()*2)) {
				case 0:
					String popis = String.format("%s%s00000000000000000000000000000000000", version, sub);
					subjekt.kodZpracovani.cssz.setPopis(popis);
					break;
				case 1:
				default:
					break;
			}
			
			listSubjekt.add(subjekt);
		}
		
		//<checkSelfEmplResponse><Subjekty>
		CheckSelfEmplResponse.Subjekty subjekty = this._objectFactory.createCheckSelfEmplResponseSubjekty();
		subjekty.subjekt = listSubjekt;
		
		checkSelfEmplResponse.setSubjekty(subjekty);
		
		return checkSelfEmplResponse;
	}
	
	
}
