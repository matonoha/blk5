package adis.bnk.par.checkSelfEmpl.request;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.xml.datatype.DatatypeConfigurationException;

import adis.bnk.par.XmlBase;
import adis.bnk.par.checkSelfEmpl.request.CheckSelfEmplRequest.Subjekty.Subjekt;

/**
 * @author valenta
 *
 *
 */
public class HelperXmlCheckSelfEmplRequest extends XmlBase<CheckSelfEmplRequest> {

	private ObjectFactory _objectFactory = new ObjectFactory();
	private final String version = "v1.7_";
	
	/**
	 * @param conn
	 * @param cZaznamu
	 */
	public HelperXmlCheckSelfEmplRequest(Connection conn) {
		super(conn);
		
		this.set_xsdSchemaURL(
				this.getClass().getResource("/adis/bnk/par/schema/IDR-INT_IA_CheckSelfEmpl-P1_Request_v1.7.xsd"));
		
	}

	public CheckSelfEmplRequest getTestRequest(int pocetSubjektu) throws DatatypeConfigurationException {

		//<checkSelfEmplRequest>
		CheckSelfEmplRequest checkSelfEmplRequest = this._objectFactory.createCheckSelfEmplRequest();
		checkSelfEmplRequest.setCorrelationId(UUID.randomUUID().toString());
		
		List<Subjekt> listSubjekt = new ArrayList<Subjekt>();
		for (int sub = 1; sub <=  pocetSubjektu; sub++) {
			//<checkSelfEmplRequest><Subjekty><Subjekt>
			Subjekt subjekt = this._objectFactory.createCheckSelfEmplRequestSubjektySubjekt();
			
			subjekt.setMessageId(UUID.randomUUID().toString());
			
			switch ((int)(Math.random()*2)) {
				case 0:
					String aifo = String.format("aaaaaaaaaaaaaaaaaaaaaaaa%s", sub);
					subjekt.setAifo(aifo.substring(aifo.length() - 24));
					break;
				case 1:
				default:
					break;
			}

			switch ((int)(Math.random()*2)) {
				case 0:
					String rodneCislo = String.format("0000000000%s", sub); 
					subjekt.setRodneCislo(rodneCislo.substring(rodneCislo.length() - 10));
					break;
				case 1:
				default:
					break;
			}
			
			String cisloPojistence = String.format("%s0000000000", sub);
			subjekt.setCisloPojistence(cisloPojistence.substring(0,10));
			
			switch ((int)(Math.random()*2)) {
				case 0:
					String evidencniCisloPojistence = String.format("0%s000000000", sub);
					subjekt.setEvidencniCisloPojistence(evidencniCisloPojistence.substring(0,10));
					break;
				case 1:
				default:
					break;
			}
			
			subjekt.setJmeno(String.format("%sJméno%s", version, sub));
			
			subjekt.setPrijmeni(String.format("%sPříjmení%s", version, sub));
			
			switch ((int)(Math.random()*2)) {
				case 0:
					subjekt.setRodnePrijmeni(String.format("%sRodnéPříjmení%s", version, sub));
					break;
				case 1:
				default:
					break;
			}

			subjekt.setDatumNarozeni(this.convertFromDateToDate(new java.util.Date()));

			subjekt.setDatumOSVCPPod(this.convertFromDateToDate(new java.util.Date()));
			
			switch ((int)(Math.random()*2)) {
				case 0:
					subjekt.setKodAdresnihoMista((int)(Math.random()*999999998));
					break;
				case 1:
					subjekt.setAdresa(String.format("%sAdresa_aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa_%s", version, sub));
					break;
				default:
					break;
			}
			
			listSubjekt.add(subjekt);
		}
		
		//<checkSelfEmplRequest><Subjekty>
		CheckSelfEmplRequest.Subjekty subjekty = this._objectFactory.createCheckSelfEmplRequestSubjekty();
		subjekty.subjekt = listSubjekt;
		
		checkSelfEmplRequest.setSubjekty(subjekty);
		
		return checkSelfEmplRequest;
	}
	
	
}
