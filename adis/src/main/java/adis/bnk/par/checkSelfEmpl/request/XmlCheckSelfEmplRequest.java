package adis.bnk.par.checkSelfEmpl.request;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;

import org.xml.sax.SAXException;

import adis.bnk.par.XmlBase;
import adis.bnk.par.checkSelfEmpl.request.CheckSelfEmplRequest.Subjekty.Subjekt;
import adis.bnk.par.db.TXParOdDs;
import adis.bnk.par.db.TXParOdDsCollection;
import adis.bnk.par.db.TXParOddoXml;

/**
 * @author valenta
 *
 */
public class XmlCheckSelfEmplRequest extends XmlBase<CheckSelfEmplRequest> {

	private ObjectFactory _objectFactory = new ObjectFactory();
	private int _cZaznamu;
	private TXParOddoXml _tXParOddoXml;
	
	/**
	 * @param conn
	 * @param cZaznamu
	 */
	public XmlCheckSelfEmplRequest(Connection conn, int cZaznamu) {
		super(conn);
		this._cZaznamu = cZaznamu;
		
		this.set_xsdSchemaURL(
				this.getClass().getResource("/adis/bnk/par/schema/IDR-INT_IA_CheckSelfEmpl-P1_Request_v1.8.xsd"));
		
//		this.set_xsdSchemaFile(
//				String.format(
//						"%s%s", 
//						System.getProperty("user.dir"), 
//						"\\src\\main\\java\\adis\\bnk\\par\\schema\\IDR-INT_IA_CheckSelfEmpl-P1_Request_v1.8.xsd"));
	}
	
	/**
	 * @return
	 * @throws SQLException
	 * @throws DatatypeConfigurationException
	 */
	public CheckSelfEmplRequest getRequest() throws SQLException, DatatypeConfigurationException {
		
		this._tXParOddoXml = new TXParOddoXml(this._conn, this._cZaznamu);
		this._tXParOddoXml.setPropertiesForCZaznamu();
		
		TXParOdDsCollection tXParOdDsCollection = new TXParOdDsCollection(this._conn);
		tXParOdDsCollection.fillForCZaznamu(this._tXParOddoXml.getCZaznamu());
		
		List<Subjekt> listSubjekt = new ArrayList<Subjekt>();
		for (TXParOdDs item : tXParOdDsCollection) {
			//<checkSelfEmplRequest><Subjekty><Subjekt>
			Subjekt subjekt = this._objectFactory.createCheckSelfEmplRequestSubjektySubjekt();
			
			subjekt.setMessageId(item.getMessageId());
			subjekt.setAifo(item.getAifo());
			subjekt.setRodneCislo(item.getRodneCislo());
			subjekt.setCisloPojistence(item.getCPoj());
			subjekt.setEvidencniCisloPojistence(item.getEcp());
			subjekt.setJmeno(item.getJmeno());
			subjekt.setPrijmeni(item.getPrijmeni());
			subjekt.setRodnePrijmeni(item.getRodnepr());
			if (item.getDNaroz() != null) {
				subjekt.setDatumNarozeni(this.convertFromDateToDate(item.getDNaroz()));
			}
			if (item.getKeDni() != null) {
				subjekt.setDatumOSVCPPod(this.convertFromDateToDate(item.getKeDni()));
			}
			if (item.getCAdrbod() != null) {
				subjekt.setKodAdresnihoMista(item.getCAdrbod());
			}
			subjekt.setAdresa(item.getAdresa());
			
			listSubjekt.add(subjekt);
		}
		
		//<checkSelfEmplRequest><Subjekty>
		CheckSelfEmplRequest.Subjekty subjekty = this._objectFactory.createCheckSelfEmplRequestSubjekty();
		subjekty.subjekt = listSubjekt;
		
		//<checkSelfEmplRequest>
		CheckSelfEmplRequest checkSelfEmplRequest = this._objectFactory.createCheckSelfEmplRequest();
		checkSelfEmplRequest.setCorrelationId(this._tXParOddoXml.getCorrelationId());
		checkSelfEmplRequest.setSubjekty(subjekty);
		
		return checkSelfEmplRequest;
	}
	
	/**
	 * @return
	 * @throws SQLException
	 * @throws DatatypeConfigurationException
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws JAXBException 
	 */
	public boolean createOrUpdateXmlToDb() throws SQLException, DatatypeConfigurationException, IOException, JAXBException, SAXException {
		
		CheckSelfEmplRequest re = this.getRequest();
		ByteArrayOutputStream baos = this.convertObjectToXmlOutputStream(re);
		boolean result = this._tXParOddoXml.updateXmlOdesl(baos);
		baos.close();
		
		return result;
	}
	
	@Override
	protected void ValidateDuties() {
		
	}
	
}
