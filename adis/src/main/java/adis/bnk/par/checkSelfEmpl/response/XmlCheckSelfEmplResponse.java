package adis.bnk.par.checkSelfEmpl.response;

import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import adis.bnk.par.XmlBase;
import adis.bnk.par.checkSelfEmpl.response.CheckSelfEmplResponse.Subjekty.Subjekt;
import adis.bnk.par.db.TXParDoDs;
import adis.bnk.par.db.TXParDoDs.UcastCsszEnum;
import adis.bnk.par.db.TXParDoDs.UcastNaVzpEnum;
import adis.bnk.par.db.TXParDoSvc;
import adis.bnk.par.db.TXParDoSvcCollection;
import adis.bnk.par.db.TXParDoZc;
import adis.bnk.par.db.TXParDoZcCollection;
import adis.bnk.par.db.TXParOddoXml;
import adis.bnk.par.db.TpParTXParDoDs;
import adis.bnk.par.db.TpParTXParDoSvc;
import adis.bnk.par.db.TpParTXParDoZc;

/**
 * @author valenta
 *
 */
public class XmlCheckSelfEmplResponse extends XmlBase<CheckSelfEmplResponse> {

	private ObjectFactory _objectFactory = new ObjectFactory();
	private int _cZaznamu;
	private TXParOddoXml _tXParOddoXml;
	
	/**
	 * @param conn
	 * @param cZaznamu
	 */
	public XmlCheckSelfEmplResponse(Connection conn, int cZaznamu) {
		super(conn);
		this._cZaznamu = cZaznamu;
		
		this.set_xsdSchemaURL(
				this.getClass().getResource("/adis/bnk/par/schema/IDR-INT_IA_CheckSelfEmpl-P2_Response_v1.9.xsd"));
		
//		this.set_xsdSchemaFile(
//				String.format(
//						"%s%s", 
//						System.getProperty("user.dir"), 
//						"/src/main/java/adis/bnk/par/schema/IDR-INT_IA_CheckSelfEmpl-P2_Response_v1.9.xsd"));
	}
	
	/**
	 * @return
	 * @throws SQLException
	 * @throws DatatypeConfigurationException
	 * @throws SAXException 
	 * @throws JAXBException 
	 */
	public CheckSelfEmplResponse getResponse() throws SQLException, DatatypeConfigurationException, JAXBException, SAXException {
		
		this._tXParOddoXml = new TXParOddoXml(this._conn, this.	_cZaznamu);
		this._tXParOddoXml.setPropertiesForCZaznamu();
		if (this._tXParOddoXml.getXmlDosly() == null) {
			throw new NullPointerException("xml_dosly");
		}
		
		Blob blob = this._tXParOddoXml.getXmlDosly();
		int blobLength = (int) blob.length();  
		byte[] blobAsBytes = blob.getBytes(1, blobLength);
		//blob.free();
		
		//Helper.writeFile("./target/pokus1.xml", blobAsBytes);
		
		CheckSelfEmplResponse checkSelfEmplResponse = this._objectFactory.createCheckSelfEmplResponse();
		
		org.w3c.dom.Document xmlDocument = null;
		try {
			xmlDocument = this.getXmlDocument(blobAsBytes);
			this.printRootElement(xmlDocument);
			
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		checkSelfEmplResponse = this.convertBytesToObject(blobAsBytes, checkSelfEmplResponse);
		return checkSelfEmplResponse;
	}
	
	/**
	 * @return
	 * @throws Exception 
	 */
	public boolean createOrUpdateXmlToDb() throws Exception {
		
		boolean result = false;
		
		long startTime;
		long elapsedTime;

//		startTime = System.nanoTime();
//		result = this.processResponse();
//		elapsedTime = System.nanoTime() - startTime;
//		System.out.println(String.format("By statement. Total execution time in millis: %s", elapsedTime/1000000));

		startTime = System.nanoTime();
		result = this.processResponseDeleteInsertBatch();
		elapsedTime = System.nanoTime() - startTime;
		System.out.println(String.format("By deleteInsertBatch. Total execution time in millis: %s", elapsedTime/1000000));
		
		return result;
	}
	
	private boolean processResponse() throws Exception {
	
		CheckSelfEmplResponse checkSelfEmplResponse = this.getResponse();

		boolean result = false;

		this._tXParOddoXml.updateCorrelationId(checkSelfEmplResponse.getCorrelationId());
		for(CheckSelfEmplResponse.Subjekty.Subjekt subjekt : checkSelfEmplResponse.getSubjekty().getSubjekt()) {
			
			int cZaznamu = this._tXParOddoXml.getCZaznamu();
			String messageId = subjekt.getMessageId();
			
			TXParDoDs tXParDoDs = new TXParDoDs(this._conn, cZaznamu, subjekt);
			result = tXParDoDs.insertOrUpdate();
			
			//pro jistotu smazeme z tabulky t_x_par_do_svc vsechny zaznamy pro cZaznamu, messageId a budeme do ni pouze INSERTovat
			TXParDoSvcCollection tXParDoSvcCollection = new TXParDoSvcCollection(this._conn);
			tXParDoSvcCollection.deleteForCZaznamuAndMessageId(cZaznamu, messageId);
			
			short cPorIntervalSvc = 1;
			for(CheckSelfEmplResponse.Subjekty.Subjekt.IntervalySvc intervalSvc : subjekt.getIntervalySvc()) {
				TXParDoSvc tXParDoSvc = new TXParDoSvc(this._conn, cZaznamu,  messageId, cPorIntervalSvc, intervalSvc);
				tXParDoSvc.insert();
				cPorIntervalSvc++;
			}

			//pro jistotu smazeme z tabulky t_x_par_do_zc vsechny zaznamy pro cZaznamu, messageId a budeme do ni pouze INSERTovat
			TXParDoZcCollection tXParDoZcCollection = new TXParDoZcCollection(this._conn);
			tXParDoZcCollection.deleteForCZaznamuAndMessageId(cZaznamu, messageId);
			
			short cPorIntervalZc = 1;
			for(CheckSelfEmplResponse.Subjekty.Subjekt.IntervalyZam intervalZam : subjekt.getIntervalyZam()) {
				TXParDoZc tXParDoZc = new TXParDoZc(this._conn, cZaznamu,  messageId, cPorIntervalZc, intervalZam);
				tXParDoZc.insert();
				cPorIntervalZc++;
			}
		}
		
		return result;
		
	}
	
	private boolean processResponseDeleteInsertBatch() throws Exception {

		CheckSelfEmplResponse checkSelfEmplResponse = this.getResponse();
		
		boolean result = false;
		boolean resultAll = true;

		int cZaznamu = this._tXParOddoXml.getCZaznamu();
		this._tXParOddoXml.updateCorrelationId(checkSelfEmplResponse.getCorrelationId());

//		long startTime;
//		long elapsedTime;
		
		int pocetZaznamuVDavceTXParDoDs = 1;
		int maxPocetZaznamuVDavceTXParDoDs = 10000;
		int pocetZaznamuVDavceTXParDoSvc = 1;
		int maxPocetZaznamuVDavceTXParDoSvc = 10000;
		int pocetZaznamuVDavceTXParDoZc = 1;
		int maxPocetZaznamuVDavceTXParDoZc = 10000;

		ArrayList<TpParTXParDoDs> dataDoDs = new ArrayList<TpParTXParDoDs>();
		ArrayList<TpParTXParDoSvc> dataDoSvc = new ArrayList<TpParTXParDoSvc>();
		ArrayList<TpParTXParDoZc> dataDoZc = new ArrayList<TpParTXParDoZc>();
		
		Map<String, List<Subjekt>> gList = checkSelfEmplResponse.getSubjekty().getSubjekt().stream().collect(Collectors.groupingBy(Subjekt::getMessageId));
		for (Entry<String, List<Subjekt>> gListItem :  gList.entrySet()) {
			long pocetForMessageId = gListItem.getValue().stream().count();
			if (gListItem.getValue().stream().count() > 1) {
				throw new Exception(String.format("Subjekt s messageId = '%s' je v kolekci %sx.", gListItem.getKey(), pocetForMessageId));
			}
		}
		
		for(CheckSelfEmplResponse.Subjekty.Subjekt subjekt : checkSelfEmplResponse.getSubjekty().getSubjekt()) {
			
			String messageId = subjekt.getMessageId();
			
			CheckSelfEmplResponse.Subjekty.Subjekt.KodZpracovani.Zp zp = 
					subjekt.getKodZpracovani() != null && subjekt.getKodZpracovani().getZp() != null
					? subjekt.getKodZpracovani().getZp()
					: null;
			
			CheckSelfEmplResponse.Subjekty.Subjekt.KodZpracovani.Cssz cssz = 
					subjekt.getKodZpracovani() != null && subjekt.getKodZpracovani().getCssz() != null
					? subjekt.getKodZpracovani().getCssz()
					: null;
			
	    	dataDoDs.add(
					new TpParTXParDoDs(
							cZaznamu, 
							subjekt.getMessageId(), 
							subjekt.getAifo(), 
							subjekt.getRodneCislo(),
							subjekt.getCisloPojistence(), 
							subjekt.getEvidencniCisloPojistence(), 
							subjekt.getJmeno(), 
							subjekt.getPrijmeni(), 
							subjekt.getRodnePrijmeni(), 
							subjekt.getDatumNarozeni() != null 
								? subjekt.getDatumNarozeni().toGregorianCalendar().getTime() 
								: null,
							zp != null && zp.getKod() != null 
				    			? UcastNaVzpEnum.getByHodnota(zp.getKod()).toString() 
				    			: null,
				    		null,
				    		zp != null
				    			? zp.getDuvod()
				    			: null,
				    		zp != null
				    			? zp.getPopis()
				    			: null,
							cssz != null && cssz.getKod() != null 
				    			? UcastCsszEnum.getByHodnota(cssz.getKod()).toString() 
				    			: null,
				    		cssz != null
				    			? cssz.getDuvod()
				    			: null,
				    		cssz != null
				    			? cssz.getPopis()
				    			: null,
				    		null,
				    		null));

	    	pocetZaznamuVDavceTXParDoDs++;

			if(pocetZaznamuVDavceTXParDoDs > maxPocetZaznamuVDavceTXParDoDs) {
//				startTime = System.nanoTime();
				result = TXParDoDs.insertOrUpdateByDeleteInsertBatch(this._conn, dataDoDs);
//				elapsedTime = System.nanoTime() - startTime;
//				System.out.println(String.format("1. Processed %s records. Total execution time in millis: %s", data.size(), elapsedTime/1000000));
				
				dataDoDs.clear();
				pocetZaznamuVDavceTXParDoDs = 1;
				
				if (!result) {
					resultAll = result;
				}
			}
			
			//nemusime mazat z tabulky t_x_par_do_svc vsechny zaznamy pro cZaznamu, messageId, protoze si to zajistime Batchem
			short cPorIntervalSvc = 1;
			for(CheckSelfEmplResponse.Subjekty.Subjekt.IntervalySvc intervalSvc : subjekt.getIntervalySvc()) {

				dataDoSvc.add(
						new TpParTXParDoSvc(
								cZaznamu,
								messageId,
								cPorIntervalSvc,
				    			intervalSvc.getOznameniZahajeniSvc() != null
					    			? intervalSvc.getOznameniZahajeniSvc().toGregorianCalendar().getTime()
					    			: null,
				    			intervalSvc.getZahajeniSvc() != null
					    			? intervalSvc.getZahajeniSvc().toGregorianCalendar().getTime()
					    			: null,
				    			intervalSvc.getUkonceniSvc() != null
					    			? intervalSvc.getUkonceniSvc().toGregorianCalendar().getTime()
					    			: null,
				    			intervalSvc.getDuvodUkonceniSvc()));
				pocetZaznamuVDavceTXParDoSvc++;

				if(pocetZaznamuVDavceTXParDoSvc > maxPocetZaznamuVDavceTXParDoSvc) {
//					startTime = System.nanoTime();
					TXParDoSvc.insertByDeleteInsertBatch(this._conn, dataDoSvc);
//					elapsedTime = System.nanoTime() - startTime;
//					System.out.println(String.format("1. Processed %s records. Total execution time in millis: %s", data.size(), elapsedTime/1000000));
					
					dataDoSvc.clear();
					pocetZaznamuVDavceTXParDoSvc = 1;
				}
				
				
				cPorIntervalSvc++;
			}

			//nemusime mazat z tabulky t_x_par_do_zc vsechny zaznamy pro cZaznamu, messageId, protoze si to zajistime Batchem
			short cPorIntervalZc = 1;
			for(CheckSelfEmplResponse.Subjekty.Subjekt.IntervalyZam intervalZam : subjekt.getIntervalyZam()) {
				
				dataDoZc.add(
						new TpParTXParDoZc(
								cZaznamu,
								messageId,
								cPorIntervalZc,
								intervalZam.getZahajeniZam() != null
									? intervalZam.getZahajeniZam().toGregorianCalendar().getTime()
					    			: null,
				    			intervalZam.getUkonceniZam() != null
					    			? intervalZam.getUkonceniZam().toGregorianCalendar().getTime()
					    			: null,
				    			intervalZam.getOznameniZahajeniZam() != null
					    			? intervalZam.getOznameniZahajeniZam().toGregorianCalendar().getTime()
					    			: null,
				    			intervalZam.getOznameniUkonceniZam() != null
					    			? intervalZam.getOznameniUkonceniZam().toGregorianCalendar().getTime()
					    			: null,
				    			intervalZam.getTypPracovnihoPomeru(),
				    			intervalZam.getNazevZam(),
				    			intervalZam.getIcoZam(),
					    		intervalZam.getAdresaZam()));
				pocetZaznamuVDavceTXParDoZc++;

				if(pocetZaznamuVDavceTXParDoZc > maxPocetZaznamuVDavceTXParDoZc) {
//					startTime = System.nanoTime();
					TXParDoZc.insertByDeleteInsertBatch(this._conn, dataDoZc);
//					elapsedTime = System.nanoTime() - startTime;
//					System.out.println(String.format("1. Processed %s records. Total execution time in millis: %s", data.size(), elapsedTime/1000000));
					
					dataDoZc.clear();
					pocetZaznamuVDavceTXParDoZc = 1;
				}
				
				
				cPorIntervalZc++;
			}
		}

		if (dataDoDs.size() > 0) {
//			startTime = System.nanoTime();
			result = TXParDoDs.insertOrUpdateByDeleteInsertBatch(this._conn, dataDoDs);
//			elapsedTime = System.nanoTime() - startTime;
//			System.out.println(String.format("2. Processed %s records. Total execution time in millis: %s", data.size(), elapsedTime/1000000));

			if (!result) {
				resultAll = result;
			}
		}
		if (dataDoSvc.size() > 0) {
//			startTime = System.nanoTime();
			TXParDoSvc.insertByDeleteInsertBatch(this._conn, dataDoSvc);
//			elapsedTime = System.nanoTime() - startTime;
//			System.out.println(String.format("2. Processed %s records. Total execution time in millis: %s", data.size(), elapsedTime/1000000));
		}
		if (dataDoZc.size() > 0) {
//			startTime = System.nanoTime();
			TXParDoZc.insertByDeleteInsertBatch(this._conn, dataDoZc);
//			elapsedTime = System.nanoTime() - startTime;
//			System.out.println(String.format("2. Processed %s records. Total execution time in millis: %s", data.size(), elapsedTime/1000000));
		}
			
		return resultAll; 
	}

	
	@Override
	protected void ValidateDuties() {
		
	}
	
}
