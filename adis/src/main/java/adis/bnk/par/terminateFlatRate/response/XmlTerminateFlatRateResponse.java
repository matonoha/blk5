package adis.bnk.par.terminateFlatRate.response;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;

import org.xml.sax.SAXException;

import adis.bnk.par.XmlBase;
import adis.bnk.par.db.TXParDoDs;
import adis.bnk.par.db.TXParOddoXml;
import adis.bnk.par.db.TpParTXParDoDs;

/**
 * @author valenta
 *
 */
public class XmlTerminateFlatRateResponse extends XmlBase<TerminateFlatRateResponse> {

	private ObjectFactory _objectFactory = new ObjectFactory();
	private int _cZaznamu;
	private TXParOddoXml _tXParOddoXml;
	
    public enum ProcessingResultEnum {
    	A ("A"),
    	N ("N");
    	
    	private final String _hodnota;
    	
    	public String getHodnota() {
    		return _hodnota;
    	}
    	
    	ProcessingResultEnum (String hodnota) {
    		this._hodnota = hodnota;
    	}
    	
    	public static ProcessingResultEnum getByHodnota(String hodnota) throws Exception {
    		switch(hodnota) {
    		case "A":
    			return A;
    		case "N":
    			return N;
    		default:
    			throw new Exception("Neznamý typ hodnoty.");
    		}
    	}
    }
	
	/**
	 * @param conn
	 * @param cZaznamu
	 */
	public XmlTerminateFlatRateResponse(Connection conn, int cZaznamu) {
		super(conn);
		this._cZaznamu = cZaznamu;
		
		this.set_xsdSchemaURL(
				this.getClass().getResource("/adis/bnk/par/schema/IDR-INT_IA_TerminateFlatRate-P2_Response_v1.1.xsd"));
		
	}
	
	/**
	 * @return
	 * @throws SQLException
	 * @throws DatatypeConfigurationException
	 * @throws SAXException 
	 * @throws JAXBException 
	 */
	public TerminateFlatRateResponse getResponse() throws SQLException, DatatypeConfigurationException, JAXBException, SAXException {
		
		_tXParOddoXml = new TXParOddoXml(_conn, _cZaznamu);
		_tXParOddoXml.setPropertiesForCZaznamu();
		if (_tXParOddoXml.getXmlDosly() == null) {
			throw new NullPointerException("xml_dosly");
		}
		
		Blob blob = _tXParOddoXml.getXmlDosly();
		int blobLength = (int) blob.length();  
		byte[] blobAsBytes = blob.getBytes(1, blobLength);
		//blob.free();
		
		//Helper.writeFile("e:/temp/pokus2.xml", blobAsBytes);
		
		TerminateFlatRateResponse terminateFlatRateResponse = this._objectFactory.createTerminateFlatRateResponse();
		terminateFlatRateResponse = this.convertBytesToObject(blobAsBytes, terminateFlatRateResponse);
		return terminateFlatRateResponse;
	}

	/**
	 * @return
	 * @throws Exception 
	 */
	public boolean createOrUpdateXmlToDb() throws Exception {
		
		boolean result = false;
		
		long startTime;
		long elapsedTime;

//		startTime = System.nanoTime();
//		result = this.processResponse();
//		elapsedTime = System.nanoTime() - startTime;
//		System.out.println(String.format("By statement. Total execution time in millis: %s", elapsedTime/1000000));

		startTime = System.nanoTime();
		result = this.processResponseDeleteInsertBatch();
		elapsedTime = System.nanoTime() - startTime;
		System.out.println(String.format("By deleteInsertBatch. Total execution time in millis: %s", elapsedTime/1000000));
		
		return result;
	}

	
	private boolean processResponse() throws Exception {
		
		TerminateFlatRateResponse terminateFlatRateResponse = this.getResponse();

		boolean result = false;

		this._tXParOddoXml.updateCorrelationId(terminateFlatRateResponse.getHeader().getCorrelationId());
		for(TerminateFlatRateResponse.Polozky.Polozka polozka : terminateFlatRateResponse.getPolozky().getPolozka()) {
			
			int cZaznamu = this._tXParOddoXml.getCZaznamu();
			String messageId = polozka.getId();
			
			TXParDoDs tXParDoDs = new TXParDoDs(this._conn, cZaznamu, polozka);
			result = tXParDoDs.insertOrUpdate();
			
		}
		
		return result;
		
	}
	
	private boolean processResponseDeleteInsertBatch() throws Exception {

		TerminateFlatRateResponse terminateFlatRateResponse = this.getResponse();
		
		boolean result = false;
		boolean resultAll = true;

		int cZaznamu = this._tXParOddoXml.getCZaznamu();
		this._tXParOddoXml.updateCorrelationId(terminateFlatRateResponse.getHeader().getCorrelationId());

//		long startTime;
//		long elapsedTime;
		
		int pocetZaznamuVDavceTXParDoDs = 1;
		int maxPocetZaznamuVDavceTXParDoDs = 10000;

		ArrayList<TpParTXParDoDs> dataDoDs = new ArrayList<TpParTXParDoDs>();
		for(TerminateFlatRateResponse.Polozky.Polozka polozka : terminateFlatRateResponse.getPolozky().getPolozka()) {
			
			String messageId = polozka.getId();
			
			String zp = 
					polozka.getProcessingResult() != null && polozka.getProcessingResult().getZp() != null
					? polozka.getProcessingResult().getZp()
					: null;
			
			String cssz = 
					polozka.getProcessingResult() != null && polozka.getProcessingResult().getCssz() != null
					?  polozka.getProcessingResult().getCssz()
					: null;
			
	    	dataDoDs.add(
					new TpParTXParDoDs(
							cZaznamu, 
							polozka.getId(), 
							null, 
							null,
							null, 
							null, 
							null, 
							null, 
							null, 
							null,
							null,
				    		null,
				    		null,
				    		null,
							null,
				    		null,
				    		null,
				    		zp != null
				    			? ProcessingResultEnum.getByHodnota(zp).toString()
				    			: null,
				    		cssz != null
				    			? ProcessingResultEnum.getByHodnota(cssz).toString()
				    			: null));

	    	pocetZaznamuVDavceTXParDoDs++;

			if(pocetZaznamuVDavceTXParDoDs > maxPocetZaznamuVDavceTXParDoDs) {
//				startTime = System.nanoTime();
				result = TXParDoDs.insertOrUpdateByDeleteInsertBatch(this._conn, dataDoDs);
//				elapsedTime = System.nanoTime() - startTime;
//				System.out.println(String.format("1. Processed %s records. Total execution time in millis: %s", data.size(), elapsedTime/1000000));
				
				dataDoDs.clear();
				pocetZaznamuVDavceTXParDoDs = 1;
				
				if (!result) {
					resultAll = result;
				}
			}
			
		}

		if (dataDoDs.size() > 0) {
//			startTime = System.nanoTime();
			result = TXParDoDs.insertOrUpdateByDeleteInsertBatch(this._conn, dataDoDs);
//			elapsedTime = System.nanoTime() - startTime;
//			System.out.println(String.format("2. Processed %s records. Total execution time in millis: %s", data.size(), elapsedTime/1000000));

			if (!result) {
				resultAll = result;
			}
		}
			
		return resultAll; 
	}
//	
//	
//	@Override
//	protected void ValidateDuties() {
//		
//	}
	
}
