package adis.bnk.par.terminateFlatRate.response;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.xml.datatype.DatatypeConfigurationException;

import adis.bnk.par.XmlBase;
import adis.bnk.par.terminateFlatRate.response.TerminateFlatRateResponse.Header;
import adis.bnk.par.terminateFlatRate.response.TerminateFlatRateResponse.Header.Parameters;
import adis.bnk.par.terminateFlatRate.response.TerminateFlatRateResponse.Header.Parameters.Parameter;
import adis.bnk.par.terminateFlatRate.response.TerminateFlatRateResponse.Polozky.Polozka;
import adis.bnk.par.terminateFlatRate.response.TerminateFlatRateResponse.Polozky.Polozka.ProcessingResult;

/**
 * @author valenta
 *
 */
public class HelperXmlTerminateFlatRateResponse extends XmlBase<TerminateFlatRateResponse> {

	private final static String CONSUMER_ID = "ADIS";
	private final static String PROVIDER_ID = "IDR";
	private final static int PROCESSING_PRIORITY = 0;
	private final static String VERSION = "1.1";
	private final static String PARAMETER_KEY_FU = "FU";
	private final static String PARAMETER_VALUE_FU = "230";

	private ObjectFactory _objectFactory = new ObjectFactory();
	private final String version = "v1.1_";

	
    public enum ProcessingResultEnum {
    	A ("A"),
    	N ("N");
    	
    	private final String _hodnota;
    	
    	public String getHodnota() {
    		return _hodnota;
    	}
    	
    	ProcessingResultEnum (String hodnota) {
    		this._hodnota = hodnota;
    	}
    	
    	public static ProcessingResultEnum getByHodnota(String hodnota) throws Exception {
    		switch(hodnota) {
    		case "A":
    			return A;
    		case "N":
    			return N;
    		default:
    			throw new Exception("Neznamý typ hodnoty.");
    		}
    	}
    }
	
	/**
	 * @param conn
	 * @param cZaznamu
	 */
	public HelperXmlTerminateFlatRateResponse(Connection conn) {
		super(conn);
		
		this.set_xsdSchemaURL(
				this.getClass().getResource("/adis/bnk/par/schema/IDR-INT_IA_TerminateFlatRate-P2_Response_v1.1.xsd"));
	}
	
	public TerminateFlatRateResponse getTestResponse(int pocetPolozek) throws DatatypeConfigurationException {
		
		//<terminateFlatRateResponse><header>
		Header header = this._objectFactory.createTerminateFlatRateResponseHeader();
		header.setMessageId(UUID.randomUUID().toString());
		header.setTimestamp(this.convertFromDateToDatetime(new java.util.Date()));
		header.setCorrelationId(UUID.randomUUID().toString());
		header.setConsumerId(CONSUMER_ID);
		header.setProviderId(PROVIDER_ID);
		header.setProcessingPriority(PROCESSING_PRIORITY);
		header.setVersion(VERSION);
		
		//<terminateFlatRateResponse><header><parameters><parameter>
		List<Parameter> listParameter = new ArrayList<Parameter>();
		Parameter parameter1 = this._objectFactory.createTerminateFlatRateResponseHeaderParametersParameter();
		parameter1.setKey(PARAMETER_KEY_FU);
		parameter1.setValue(PARAMETER_VALUE_FU);
		listParameter.add(parameter1);
		
		//<terminateFlatRateResponse><header><parameters>
		Parameters parameters = this._objectFactory.createTerminateFlatRateResponseHeaderParameters();
		parameters.parameter = listParameter;
		
		header.setParameters(parameters);

		List<Polozka> listPolozka = new ArrayList<Polozka>();
		for (int pol = 1; pol <=  pocetPolozek; pol++) {
			
			//<terminateFlatRateResponse><polozky><polozka>
			Polozka polozka = this._objectFactory.createTerminateFlatRateResponsePolozkyPolozka();
			polozka.setId(UUID.randomUUID().toString());

			ProcessingResult processingResult = this._objectFactory.createTerminateFlatRateResponsePolozkyPolozkaProcessingResult();
			switch ((int)(Math.random()*2)) {
				case 0:
					processingResult.setZp(ProcessingResultEnum.A.toString());
					break;
				case 1:
					processingResult.setZp(ProcessingResultEnum.N.toString());
					break;
				default:
					break;
			}
			switch ((int)(Math.random()*2)) {
				case 0:
					processingResult.setCssz(ProcessingResultEnum.A.toString());
					break;
				case 1:
					processingResult.setCssz(ProcessingResultEnum.N.toString());
					break;
				default:
					break;
			}
			
			//<terminateFlatRateResponse><polozky><polozka><procesingResult>
			polozka.setProcessingResult(processingResult);
			
			listPolozka.add(polozka);
		}
		
		//<terminateFlatRateResponse><Polozky>
		TerminateFlatRateResponse.Polozky polozky = this._objectFactory.createTerminateFlatRateResponsePolozky();
		polozky.polozka = listPolozka;
		
		//<terminateFlatRateResponse>
		TerminateFlatRateResponse terminateFlatRateResponse = this._objectFactory.createTerminateFlatRateResponse();
		terminateFlatRateResponse.setHeader(header);
		terminateFlatRateResponse.setPolozky(polozky);
		
		return terminateFlatRateResponse;
	
	}

	
}
