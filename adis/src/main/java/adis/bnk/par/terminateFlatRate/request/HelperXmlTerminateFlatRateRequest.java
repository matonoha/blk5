package adis.bnk.par.terminateFlatRate.request;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.xml.datatype.DatatypeConfigurationException;

import adis.bnk.par.XmlBase;
import adis.bnk.par.db.TXParOdDuvukon;
import adis.bnk.par.db.TXParOdDuvukonCollection;
import adis.bnk.par.terminateFlatRate.request.TerminateFlatRateRequest.Header;
import adis.bnk.par.terminateFlatRate.request.TerminateFlatRateRequest.Header.Parameters;
import adis.bnk.par.terminateFlatRate.request.TerminateFlatRateRequest.Header.Parameters.Parameter;
import adis.bnk.par.terminateFlatRate.request.TerminateFlatRateRequest.Polozky.Polozka;
import adis.bnk.par.terminateFlatRate.request.TerminateFlatRateRequest.Polozky.Polozka.UdajeOsoby;
import adis.bnk.par.terminateFlatRate.request.TerminateFlatRateRequest.Polozky.Polozka.UkonceniPR;
import adis.bnk.par.terminateFlatRate.request.TerminateFlatRateRequest.Polozky.Polozka.UkonceniPR.DuvodUkonceni;
import adis.bnk.par.terminateFlatRate.request.TerminateFlatRateRequest.Polozky.Polozka.UkonceniPR.DuvodUkonceni.KodyUpresneniDuvodu;

/**
 * @author valenta
 *
 */
public class HelperXmlTerminateFlatRateRequest extends XmlBase<TerminateFlatRateRequest> {

	private final static String CONSUMER_ID = "ADIS";
	private final static String PROVIDER_ID = "IDR";
	private final static int PROCESSING_PRIORITY = 0;
	private final static String VERSION = "1.6";
	private final static String PARAMETER_KEY_FU = "FU";
	private final static String PARAMETER_VALUE_FU = "230";

	private ObjectFactory _objectFactory = new ObjectFactory();
	private final String version = "v1.5_";

    public enum PreferovanyIdentifikatorCSSZEnum {
    	RC ("rc"),
    	ECP ("ecp");
    	
    	private final String _hodnota;
    	
    	String getHodnota() {
    		return _hodnota;
    	}
    	
    	PreferovanyIdentifikatorCSSZEnum (String hodnota) {
    		this._hodnota = hodnota;
    	}
    	
    	public static PreferovanyIdentifikatorCSSZEnum getByHodnota(String hodnota) throws Exception {
    		switch(hodnota) {
    		case "rc":
    			return RC;
    		case "ecp":
    			return ECP;
    		default:
    			throw new Exception("Neznamý typ hodnoty.");
    		}
    	}
    }

    public enum DuvodEnum {
    	F ("F"),
    	G ("G"),
    	E ("E"),
    	S ("S");
    	
    	private final String _hodnota;
    	
    	String getHodnota() {
    		return _hodnota;
    	}
    	
    	DuvodEnum (String hodnota) {
    		this._hodnota = hodnota;
    	}
    	
    	public static DuvodEnum getByHodnota(String hodnota) throws Exception {
    		switch(hodnota) {
    		case "F":
    			return F;
    		case "G":
    			return G;
    		case "E":
    			return E;
    		case "S":
    			return S;
    		default:
    			throw new Exception("Neznamý typ hodnoty.");
    		}
    	}
    }

	/**
	 * @param conn
	 * @param cZaznamu
	 */
	public HelperXmlTerminateFlatRateRequest(Connection conn) {
		super(conn);
		
		this.set_xsdSchemaURL(
				this.getClass().getResource("/adis/bnk/par/schema/IDR-INT_IA_TerminateFlatRate-P1_Request_v1.5.xsd"));
	}
	
	public TerminateFlatRateRequest getTestRequest(int pocetPolozek, int pocetKodyUpresneniDuvodu) throws DatatypeConfigurationException {
		
		//<terminateFlatRateRequest><header>
		Header header = this._objectFactory.createTerminateFlatRateRequestHeader();
		header.setMessageId(UUID.randomUUID().toString());
		header.setTimestamp(this.convertFromDateToDatetime(new java.util.Date()));
		header.setCorrelationId(UUID.randomUUID().toString());
		header.setConsumerId(CONSUMER_ID);
		header.setProviderId(PROVIDER_ID);
		header.setProcessingPriority(PROCESSING_PRIORITY);
		header.setVersion(VERSION);
		
		//<terminateFlatRateRequest><header><parameters><parameter>
		List<Parameter> listParameter = new ArrayList<Parameter>();
		Parameter parameter1 = this._objectFactory.createTerminateFlatRateRequestHeaderParametersParameter();
		parameter1.setKey(PARAMETER_KEY_FU);
		parameter1.setValue(PARAMETER_VALUE_FU);
		listParameter.add(parameter1);
		
		//<terminateFlatRateRequest><header><parameters>
		Parameters parameters = this._objectFactory.createTerminateFlatRateRequestHeaderParameters();
		parameters.parameter = listParameter;
		
		header.setParameters(parameters);

		List<Polozka> listPolozka = new ArrayList<Polozka>();
		for (int pol = 1; pol <=  pocetPolozek; pol++) {
			
			//<terminateFlatRateRequest><polozky><polozka>
			Polozka polozka = this._objectFactory.createTerminateFlatRateRequestPolozkyPolozka();
			polozka.setId(UUID.randomUUID().toString());
			
			BigDecimal idUkonceni = new BigDecimal(Math.random()*999999999999999d);
			polozka.setIdUkonceni(idUkonceni.toBigInteger());

			switch ((int)(Math.random()*2)) {
				case 0:
					BigDecimal idUkonceniOprava = new BigDecimal(Math.random()*999999999999999d);
					polozka.setIdUkonceniOprava(idUkonceniOprava.toBigInteger());
					break;
				case 1:
				default:
					break;
			}

			BigDecimal idZahajeniOriginal = new BigDecimal(Math.random()*999999999999999d);
			polozka.setIdZahajeniOriginal(idZahajeniOriginal.toBigInteger());

			switch ((int)(Math.random()*2)) {
				case 0:
					BigDecimal idZahajeniPosledniOprava = new BigDecimal(Math.random()*999999999999999d);
					polozka.setIdZahajeniPosledniOprava(idZahajeniPosledniOprava.toBigInteger());
					break;
				case 1:
				default:
					break;
			}
			
			//v 1.5 - neplneno
			//polozka.setIdPodnetuUkonceni(null);

			//<terminateFlatRateRequest><polozky><polozka><udajeOsoby>
			UdajeOsoby udajeOsoby = this._objectFactory.createTerminateFlatRateRequestPolozkyPolozkaUdajeOsoby();

			switch ((int)(Math.random()*2)) {
				case 0:
					String aifo = String.format("aaaaaaaaaaaaaaaaaaaaaaaa%s", pol);
					udajeOsoby.setAifo(aifo.substring(aifo.length() - 24));
					break;
				case 1:
				default:
					break;
			}

			switch ((int)(Math.random()*2)) {
				case 0:
					String rodneCislo = String.format("0000000000%s", pol); 
					udajeOsoby.setRodneCislo(rodneCislo.substring(rodneCislo.length() - 10));
					break;
				case 1:
				default:
					break;
			}

			switch ((int)(Math.random()*2)) {
				case 0:
						String cisloPojistence = String.format("%s0000000000", pol);
						udajeOsoby.setCisloPojistence(cisloPojistence.substring(0,10));
						break;
				case 1:
				default:
					break;
			}
		
			switch ((int)(Math.random()*2)) {
				case 0:
					String evidencniCisloPojistence = String.format("0%s000000000", pol);
					udajeOsoby.setEvidencniCisloPojistence(evidencniCisloPojistence.substring(0,10));
					break;
				case 1:
				default:
					break;
			}

			switch ((int)(Math.random()*2)) {
				case 0:
					udajeOsoby.setPreferovanyIdentifikatorCSSZ(PreferovanyIdentifikatorCSSZEnum.RC.getHodnota());
					break;
				case 1:
					udajeOsoby.setPreferovanyIdentifikatorCSSZ(PreferovanyIdentifikatorCSSZEnum.ECP.getHodnota());
					break;
				default:
					break;
			}
			
			udajeOsoby.setJmeno(String.format("%sJméno%s", version, pol));
			
			udajeOsoby.setPrijmeni(String.format("%sPříjmení%s", version, pol));
			
			udajeOsoby.setDatumNarozeni(this.convertFromDateToDate(new java.util.Date()));
			
			switch ((int)(Math.random()*2)) {
				case 0:
					udajeOsoby.setRodnePrijmeni(String.format("%sRodnéPříjmení%s", version, pol));
					break;
				case 1:
				default:
					break;
			}
	
			polozka.setUdajeOsoby(udajeOsoby);
			
			//<terminateFlatRateRequest><polozky><polozka><ukonceniPR>
			UkonceniPR ukonceniPR = this._objectFactory.createTerminateFlatRateRequestPolozkyPolozkaUkonceniPR();
			ukonceniPR.setDatumZahajeni(this.convertFromDateToDate(new java.util.Date()));
			ukonceniPR.setDatumUkonceni(this.convertFromDateToDate(new java.util.Date()));

			//<terminateFlatRateRequest><polozky><polozka><ukonceniPR><duvodUkonceni>
			DuvodUkonceni duvodUkonceni = this._objectFactory.createTerminateFlatRateRequestPolozkyPolozkaUkonceniPRDuvodUkonceni();
			
			switch ((int)(Math.random()*4)) {
				case 0:
					duvodUkonceni.setDuvod(DuvodEnum.F.getHodnota());
					break;
				case 1:
					duvodUkonceni.setDuvod(DuvodEnum.G.getHodnota());
					break;
				case 2:
					duvodUkonceni.setDuvod(DuvodEnum.E.getHodnota());
					break;
				case 3:
					duvodUkonceni.setDuvod(DuvodEnum.S.getHodnota());
					break;
				default:
					break;
			}
			
			List<BigInteger> listKod = new ArrayList<BigInteger>();
			for(int pocKod = 1; pocKod <=  pocetKodyUpresneniDuvodu; pocKod++) {
				//<terminateFlatRateRequest><polozky><polozka><ukonceniPR><duvodUkonceni><kodyUpresneniDuvodu><kod>
				listKod.add(new BigDecimal(pocKod).toBigInteger());
			}

			//<terminateFlatRateRequest><polozky><polozka><ukonceniPR><duvodUkonceni><kodyUpresneniDuvodu>
			KodyUpresneniDuvodu kodyUpresneniDuvodu = this._objectFactory.createTerminateFlatRateRequestPolozkyPolozkaUkonceniPRDuvodUkonceniKodyUpresneniDuvodu();
			kodyUpresneniDuvodu.kod = listKod;
			
			duvodUkonceni.setKodyUpresneniDuvodu(kodyUpresneniDuvodu);
			
			ukonceniPR.setDuvodUkonceni(duvodUkonceni);
			
			polozka.setUkonceniPR(ukonceniPR);
			
			//v 1.5 - neplneno
			//<terminateFlatRateRequest><polozky><polozka><ukonceniPR><duvodUkonceni><povinnostDapProRoky>
			//PovinnostDapProRoky povinnostDapProRoky = this._objectFactory.createTerminateFlatRateRequestPolozkyPolozkaUkonceniPRPovinnostDapProRoky();
			
			listPolozka.add(polozka);
		}
		
		//<terminateFlatRateRequest><Polozky>
		TerminateFlatRateRequest.Polozky polozky = this._objectFactory.createTerminateFlatRateRequestPolozky();
		polozky.polozka = listPolozka;
		
		//<terminateFlatRateRequest>
		TerminateFlatRateRequest terminateFlatRateRequest = this._objectFactory.createTerminateFlatRateRequest();
		terminateFlatRateRequest.setHeader(header);
		terminateFlatRateRequest.setPolozky(polozky);
		
		return terminateFlatRateRequest;
	
	}

	
}
