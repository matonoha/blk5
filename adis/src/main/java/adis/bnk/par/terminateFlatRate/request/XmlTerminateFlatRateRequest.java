package adis.bnk.par.terminateFlatRate.request;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;

import org.xml.sax.SAXException;

import adis.bnk.par.XmlBase;
import adis.bnk.par.db.TXParOdDs;
import adis.bnk.par.db.TXParOdDsCollection;
import adis.bnk.par.db.TXParOdDuvukon;
import adis.bnk.par.db.TXParOdDuvukonCollection;
import adis.bnk.par.db.TXParOddoXml;
import adis.bnk.par.terminateFlatRate.request.TerminateFlatRateRequest.Header;
import adis.bnk.par.terminateFlatRate.request.TerminateFlatRateRequest.Header.Parameters;
import adis.bnk.par.terminateFlatRate.request.TerminateFlatRateRequest.Header.Parameters.Parameter;
import adis.bnk.par.terminateFlatRate.request.TerminateFlatRateRequest.Polozky.Polozka;
import adis.bnk.par.terminateFlatRate.request.TerminateFlatRateRequest.Polozky.Polozka.UdajeOsoby;
import adis.bnk.par.terminateFlatRate.request.TerminateFlatRateRequest.Polozky.Polozka.UkonceniPR;
import adis.bnk.par.terminateFlatRate.request.TerminateFlatRateRequest.Polozky.Polozka.UkonceniPR.DuvodUkonceni;
import adis.bnk.par.terminateFlatRate.request.TerminateFlatRateRequest.Polozky.Polozka.UkonceniPR.DuvodUkonceni.KodyUpresneniDuvodu;

/**
 * @author valenta
 */
public class XmlTerminateFlatRateRequest extends XmlBase<TerminateFlatRateRequest> {

	private final static String CONSUMER_ID = "ADIS";
	private final static String PROVIDER_ID = "IDR";
	private final static int PROCESSING_PRIORITY = 0;
	private final static String VERSION = "1.6";
	private final static String PARAMETER_KEY_FU = "FU";
	
	private ObjectFactory _objectFactory = new ObjectFactory();
	private int _cZaznamu;
	private TXParOddoXml _tXParOddoXml;
	
	/**
	 * @param conn
	 * @param cZaznamu
	 */
	public XmlTerminateFlatRateRequest(Connection conn, int cZaznamu) {
		super(conn);
		this._cZaznamu = cZaznamu;
		
		this.set_xsdSchemaURL(
				this.getClass().getResource(
						"/adis/bnk/par/schema/IDR-INT_IA_TerminateFlatRate-P1_Request_v1.6.xsd"));
		
	}
	
	/**
	 * verze metody, ktera v kazdem cyklu nacita hodnoty TXParOdDuvukon pro c_zaznamu a message_id
	 * 
	 * @return
	 * @throws SQLException
	 * @throws DatatypeConfigurationException
	 */
	public TerminateFlatRateRequest getRequestOld() 
			throws SQLException, DatatypeConfigurationException {
		
		this._tXParOddoXml = new TXParOddoXml(this._conn, this._cZaznamu);
		this._tXParOddoXml.setPropertiesForCZaznamu();
		
		//<terminateFlatRateRequest><header>
		Header header = this._objectFactory.createTerminateFlatRateRequestHeader();
		header.setMessageId(UUID.randomUUID().toString());
		header.setTimestamp(this.convertFromDateToDatetime(new java.util.Date()));
		header.setCorrelationId(this._tXParOddoXml.getCorrelationId());
		header.setConsumerId(CONSUMER_ID);
		header.setProviderId(PROVIDER_ID);
		header.setProcessingPriority(PROCESSING_PRIORITY);
		header.setVersion(VERSION);
		
		//<terminateFlatRateRequest><header><parameters><parameter>
		List<Parameter> listParameter = new ArrayList<>();
		Parameter parameter1 = this._objectFactory.createTerminateFlatRateRequestHeaderParametersParameter();
		parameter1.setKey(PARAMETER_KEY_FU);
		parameter1.setValue(this._tXParOddoXml.getCFu());
		listParameter.add(parameter1);
		
		//<terminateFlatRateRequest><header><parameters>
		Parameters parameters = this._objectFactory.createTerminateFlatRateRequestHeaderParameters();
		parameters.parameter = listParameter;
		
		header.setParameters(parameters);
		
		TXParOdDsCollection tXParOdDsCollection = new TXParOdDsCollection(this._conn);
		tXParOdDsCollection.fillForCZaznamu(this._cZaznamu);
		
		List<Polozka> listPolozka = new ArrayList<>();
		for (TXParOdDs itemPolozka : tXParOdDsCollection) {
			//<terminateFlatRateRequest><polozky><polozka>
			Polozka polozka = this._objectFactory.createTerminateFlatRateRequestPolozkyPolozka();
			
			polozka.setId(itemPolozka.getMessageId());
			polozka.setIdUkonceni(itemPolozka.getIdUkonceni().toBigInteger());

			if (itemPolozka.getIdUkonceniO() != null) {
				polozka.setIdUkonceniOprava(itemPolozka.getIdUkonceniO().toBigInteger());
			}
			
			polozka.setIdZahajeniOriginal(itemPolozka.getIdZahajeniOrig().toBigInteger());
			
			if (itemPolozka.getIdZahajeniO() != null) {
				polozka.setIdZahajeniPosledniOprava(itemPolozka.getIdZahajeniO().toBigInteger());
			}

			//v 1.5 - neplneno
			//polozka.setIdPodnetuUkonceni(null);
			
			//<terminateFlatRateRequest><polozky><polozka><udajeOsoby>
			UdajeOsoby udajeOsoby = this._objectFactory.createTerminateFlatRateRequestPolozkyPolozkaUdajeOsoby();
			udajeOsoby.setAifo(itemPolozka.getAifo());
			udajeOsoby.setRodneCislo(itemPolozka.getRodneCislo());
			udajeOsoby.setCisloPojistence(itemPolozka.getCPoj());
			udajeOsoby.setEvidencniCisloPojistence(itemPolozka.getEcp());
			udajeOsoby.setPreferovanyIdentifikatorCSSZ(itemPolozka.getPrefCssz());
			udajeOsoby.setJmeno(itemPolozka.getJmeno());
			udajeOsoby.setPrijmeni(itemPolozka.getPrijmeni());
			if (itemPolozka.getDNaroz() != null) {
				udajeOsoby.setDatumNarozeni(this.convertFromDateToDate(itemPolozka.getDNaroz()));
			}
			udajeOsoby.setRodnePrijmeni(itemPolozka.getRodnepr());

			polozka.setUdajeOsoby(udajeOsoby);
			
			//<terminateFlatRateRequest><polozky><polozka><ukonceniPR>
			UkonceniPR ukonceniPR = this._objectFactory.createTerminateFlatRateRequestPolozkyPolozkaUkonceniPR();
			if (itemPolozka.getDZahajeni() != null) {
				ukonceniPR.setDatumZahajeni(this.convertFromDateToDate(itemPolozka.getDZahajeni()));
			}
			if (itemPolozka.getKeDni() != null) {
				ukonceniPR.setDatumUkonceni(this.convertFromDateToDate(itemPolozka.getKeDni()));
			}

			TXParOdDuvukonCollection tXParOdDuvukonCollection = new TXParOdDuvukonCollection(
					this._conn);
			tXParOdDuvukonCollection.fillForCZaznamuAndMessageId(this._cZaznamu, 
					itemPolozka.getMessageId());
			
			//<terminateFlatRateRequest><polozky><polozka><ukonceniPR><duvodUkonceni>
			DuvodUkonceni duvodUkonceni = this._objectFactory.createTerminateFlatRateRequestPolozkyPolozkaUkonceniPRDuvodUkonceni();
			
			String duvod = !tXParOdDuvukonCollection.isEmpty()
					? tXParOdDuvukonCollection.stream().findFirst().get().getDuvodKon()
					: null;
			duvodUkonceni.setDuvod(duvod);
			
			List<BigInteger> listKod = new ArrayList<>();
			for (TXParOdDuvukon itemKdodyUpresneniDuvoduKod : tXParOdDuvukonCollection) {
				//<terminateFlatRateRequest><polozky><polozka><ukonceniPR><duvodUkonceni><kodyUpresneniDuvodu><kod>
				if (itemKdodyUpresneniDuvoduKod.getDuvod() != null) {
					listKod.add(new BigInteger(itemKdodyUpresneniDuvoduKod.getDuvod()));
				}
			}

			//<terminateFlatRateRequest><polozky><polozka><ukonceniPR><duvodUkonceni><kodyUpresneniDuvodu>
			KodyUpresneniDuvodu kodyUpresneniDuvodu = this._objectFactory.createTerminateFlatRateRequestPolozkyPolozkaUkonceniPRDuvodUkonceniKodyUpresneniDuvodu();
			kodyUpresneniDuvodu.kod = listKod;
			
			duvodUkonceni.setKodyUpresneniDuvodu(kodyUpresneniDuvodu);

			ukonceniPR.setDuvodUkonceni(duvodUkonceni);
			
			polozka.setUkonceniPR(ukonceniPR);
			
			//v 1.5 - neplneno
			//<terminateFlatRateRequest><polozky><polozka><ukonceniPR><duvodUkonceni><povinnostDapProRoky>
			//PovinnostDapProRoky povinnostDapProRoky = this._objectFactory.createTerminateFlatRateRequestPolozkyPolozkaUkonceniPRPovinnostDapProRoky();
			
			listPolozka.add(polozka);
		}
		
		//<terminateFlatRateRequest><Polozky>
		TerminateFlatRateRequest.Polozky polozky = this._objectFactory.createTerminateFlatRateRequestPolozky();
		polozky.polozka = listPolozka;
		
		//<terminateFlatRateRequest>
		TerminateFlatRateRequest terminateFlatRateRequest = this._objectFactory.createTerminateFlatRateRequest();
		terminateFlatRateRequest.setHeader(header);
		terminateFlatRateRequest.setPolozky(polozky);
		
		return terminateFlatRateRequest;
	}

	/**
	 * verze metody, ktera nacita hodnoty z TXParOdDuvukon jednou pro c_zaznamu a provadi filtrovani 
	 * kolekce
	 * 
	 * @return
	 * @throws SQLException
	 * @throws DatatypeConfigurationException
	 */
	public TerminateFlatRateRequest getRequest() 
			throws SQLException, DatatypeConfigurationException {
		
		this._tXParOddoXml = new TXParOddoXml(this._conn, this._cZaznamu);
		this._tXParOddoXml.setPropertiesForCZaznamu();

		//nacteni vsech hodnot z tabulky TXParOdDs pro _cZaznamu
		TXParOdDsCollection tXParOdDsCollection = new TXParOdDsCollection(this._conn);
		tXParOdDsCollection.fillForCZaznamu(this._cZaznamu);

		//nacteni vsech hodnot z tabulky TXParUhradaPl pro _cZaznamu
		TXParOdDuvukonCollection tXParOdDuvukonCollection = new TXParOdDuvukonCollection(
				this._conn);
		tXParOdDuvukonCollection.fillForCZaznamu(this._cZaznamu);
		
		//<terminateFlatRateRequest><header>
		Header header = this._objectFactory.createTerminateFlatRateRequestHeader();
		header.setMessageId(UUID.randomUUID().toString());
		header.setTimestamp(this.convertFromDateToDatetime(new java.util.Date()));
		header.setCorrelationId(this._tXParOddoXml.getCorrelationId());
		header.setConsumerId(CONSUMER_ID);
		header.setProviderId(PROVIDER_ID);
		header.setProcessingPriority(PROCESSING_PRIORITY);
		header.setVersion(VERSION);
		
		//<terminateFlatRateRequest><header><parameters><parameter>
		List<Parameter> listParameter = new ArrayList<>();
		Parameter parameter1 = this._objectFactory.createTerminateFlatRateRequestHeaderParametersParameter();
		parameter1.setKey(PARAMETER_KEY_FU);
		parameter1.setValue(this._tXParOddoXml.getCFu());
		listParameter.add(parameter1);
		
		//<terminateFlatRateRequest><header><parameters>
		Parameters parameters = this._objectFactory.createTerminateFlatRateRequestHeaderParameters();
		parameters.parameter = listParameter;
		
		header.setParameters(parameters);
		
		List<Polozka> listPolozka = new ArrayList<>();
		for (TXParOdDs itemPolozka : tXParOdDsCollection) {
			//<terminateFlatRateRequest><polozky><polozka>
			Polozka polozka = this._objectFactory.createTerminateFlatRateRequestPolozkyPolozka();
			
			polozka.setId(itemPolozka.getMessageId());
			polozka.setIdUkonceni(itemPolozka.getIdUkonceni().toBigInteger());

			if (itemPolozka.getIdUkonceniO() != null) {
				polozka.setIdUkonceniOprava(itemPolozka.getIdUkonceniO().toBigInteger());
			}
			
			polozka.setIdZahajeniOriginal(itemPolozka.getIdZahajeniOrig().toBigInteger());
			
			if (itemPolozka.getIdZahajeniO() != null) {
				polozka.setIdZahajeniPosledniOprava(itemPolozka.getIdZahajeniO().toBigInteger());
			}

			//v 1.5 - neplneno
			//polozka.setIdPodnetuUkonceni(null);
			
			//<terminateFlatRateRequest><polozky><polozka><udajeOsoby>
			UdajeOsoby udajeOsoby = this._objectFactory.createTerminateFlatRateRequestPolozkyPolozkaUdajeOsoby();
			udajeOsoby.setAifo(itemPolozka.getAifo());
			udajeOsoby.setRodneCislo(itemPolozka.getRodneCislo());
			udajeOsoby.setCisloPojistence(itemPolozka.getCPoj());
			udajeOsoby.setEvidencniCisloPojistence(itemPolozka.getEcp());
			udajeOsoby.setPreferovanyIdentifikatorCSSZ(itemPolozka.getPrefCssz());
			udajeOsoby.setJmeno(itemPolozka.getJmeno());
			udajeOsoby.setPrijmeni(itemPolozka.getPrijmeni());
			if (itemPolozka.getDNaroz() != null) {
				udajeOsoby.setDatumNarozeni(this.convertFromDateToDate(itemPolozka.getDNaroz()));
			}
			udajeOsoby.setRodnePrijmeni(itemPolozka.getRodnepr());

			polozka.setUdajeOsoby(udajeOsoby);
			
			//<terminateFlatRateRequest><polozky><polozka><ukonceniPR>
			UkonceniPR ukonceniPR = this._objectFactory.createTerminateFlatRateRequestPolozkyPolozkaUkonceniPR();
			if (itemPolozka.getDZahajeni() != null) {
				ukonceniPR.setDatumZahajeni(this.convertFromDateToDate(itemPolozka.getDZahajeni()));
			}
			if (itemPolozka.getKeDni() != null) {
				ukonceniPR.setDatumUkonceni(this.convertFromDateToDate(itemPolozka.getKeDni()));
			}

			//<terminateFlatRateRequest><polozky><polozka><ukonceniPR><duvodUkonceni>
			DuvodUkonceni duvodUkonceni = this._objectFactory.createTerminateFlatRateRequestPolozkyPolozkaUkonceniPRDuvodUkonceni();
			
			List<TXParOdDuvukon> listTXPaOdDuvukonForCZaznamuAndMessageId = tXParOdDuvukonCollection.stream().filter(
					p -> p.getMessageId().equals(itemPolozka.getMessageId())).collect(
							Collectors.toList());
			
			String duvod = !listTXPaOdDuvukonForCZaznamuAndMessageId.isEmpty()
					? listTXPaOdDuvukonForCZaznamuAndMessageId.stream().findFirst().get().getDuvodKon()
					: null;
			duvodUkonceni.setDuvod(duvod);
			
			List<BigInteger> listKod = new ArrayList<>();
			for (TXParOdDuvukon itemKodyUpresneniDuvoduKod : listTXPaOdDuvukonForCZaznamuAndMessageId) {
				//<terminateFlatRateRequest><polozky><polozka><ukonceniPR><duvodUkonceni><kodyUpresneniDuvodu><kod>
				if (itemKodyUpresneniDuvoduKod.getDuvod() != null) {
					listKod.add(new BigInteger(itemKodyUpresneniDuvoduKod.getDuvod()));
				}
			}

			//<terminateFlatRateRequest><polozky><polozka><ukonceniPR><duvodUkonceni><kodyUpresneniDuvodu>
			KodyUpresneniDuvodu kodyUpresneniDuvodu = this._objectFactory.createTerminateFlatRateRequestPolozkyPolozkaUkonceniPRDuvodUkonceniKodyUpresneniDuvodu();
			kodyUpresneniDuvodu.kod = listKod;
			
			duvodUkonceni.setKodyUpresneniDuvodu(kodyUpresneniDuvodu);

			ukonceniPR.setDuvodUkonceni(duvodUkonceni);
			
			polozka.setUkonceniPR(ukonceniPR);
			
			//v 1.5 - neplneno
			//<terminateFlatRateRequest><polozky><polozka><ukonceniPR><duvodUkonceni><povinnostDapProRoky>
			//PovinnostDapProRoky povinnostDapProRoky = this._objectFactory.createTerminateFlatRateRequestPolozkyPolozkaUkonceniPRPovinnostDapProRoky();
			
			listPolozka.add(polozka);
		}
		
		//<terminateFlatRateRequest><Polozky>
		TerminateFlatRateRequest.Polozky polozky = this._objectFactory.createTerminateFlatRateRequestPolozky();
		polozky.polozka = listPolozka;
		
		//<terminateFlatRateRequest>
		TerminateFlatRateRequest terminateFlatRateRequest = this._objectFactory.createTerminateFlatRateRequest();
		terminateFlatRateRequest.setHeader(header);
		terminateFlatRateRequest.setPolozky(polozky);
		
		return terminateFlatRateRequest;
	}
	
	/**
	 * @return
	 * @throws SQLException
	 * @throws DatatypeConfigurationException
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws JAXBException 
	 */
	public boolean createOrUpdateXmlToDb() throws SQLException, DatatypeConfigurationException, 
					IOException, JAXBException, SAXException {
		
		long startTime;
		long elapsedTime;

		//startTime = System.nanoTime();
		//TerminateFlatRateRequest re = this.getRequestOld();
		//ByteArrayOutputStream baos = this.convertObjectToXmlOutputStream(re);
		//boolean result = this._tXParOddoXml.updateXmlOdesl(baos);
		//elapsedTime = System.nanoTime() - startTime;
		//System.out.println(String.format("By getRequestOld(). Total execution time in millis: %s", elapsedTime/1000000));
		
		startTime = System.nanoTime();
		TerminateFlatRateRequest re = this.getRequest();
		ByteArrayOutputStream baos = this.convertObjectToXmlOutputStream(re);
		boolean result = this._tXParOddoXml.updateXmlOdesl(baos);
		elapsedTime = System.nanoTime() - startTime;
		System.out.println(String.format("By getRequest(). Total execution time in millis: %s", elapsedTime/1000000));

		baos.close();
		
		return result;
	}
	
	@Override
	protected void ValidateDuties() {
		
	}
}
