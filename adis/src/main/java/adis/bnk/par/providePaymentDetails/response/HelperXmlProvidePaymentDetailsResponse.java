package adis.bnk.par.providePaymentDetails.response;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import adis.bnk.par.XmlBase;
import adis.bnk.par.db.TXParRozpis.StatusCsszEnum;
import adis.bnk.par.db.TXParRozpis.StatusVzpEnum;
import adis.bnk.par.providePaymentDetails.response.ProvidePaymentDetailsResponse.Cssz.Polozky;
import adis.bnk.par.providePaymentDetails.response.ProvidePaymentDetailsResponse.Cssz.Polozky.Polozka;
import adis.bnk.par.providePaymentDetails.response.ProvidePaymentDetailsResponse.Cssz.Polozky.Polozka.Platby;
import adis.bnk.par.providePaymentDetails.response.ProvidePaymentDetailsResponse.Cssz.Polozky.Polozka.Platby.Platba;

/**
 * @author valenta
 *
 */
public class HelperXmlProvidePaymentDetailsResponse extends XmlBase<ProvidePaymentDetailsResponse> {

	private ObjectFactory _objectFactory = new ObjectFactory();
	private final String version = "v1.7_";

	
    public enum PrijemceEnum {
    	VZP ("VZP"),
    	CSSZ ("CSSZ");
    	
    	private final String _hodnota;
    	
    	String getHodnota() {
    		return _hodnota;
    	}
    	
    	PrijemceEnum (String hodnota) {
    		this._hodnota = hodnota;
    	}
    	
    	public static PrijemceEnum getByHodnota(String hodnota) throws Exception {
    		switch(hodnota) {
    		case "VZP":
    			return VZP;
    		case "CSSZ":
    			return CSSZ;
    		default:
    			throw new Exception("Neznamý typ hodnoty.");
    		}
    	}
    }
	
	/**
	 * @param conn
	 * @param cZaznamu
	 */
	public HelperXmlProvidePaymentDetailsResponse(Connection conn) {
		super(conn);
		
		this.set_xsdSchemaURL(
				this.getClass().getResource("/adis/bnk/par/schema/IDR-INT_IA_ProvidePaymentDetails-P2_Response_v1.7.xsd"));
	}
	
	public ProvidePaymentDetailsResponse getTestResponse(PrijemceEnum prijemce, int pocetPolozek, int pocetPlatebVPolozce) {
		
		//<providePaymentDetailsResponse>
		ProvidePaymentDetailsResponse providePaymentDetailsResponse = this._objectFactory.createProvidePaymentDetailsResponse();
		providePaymentDetailsResponse.setCorrelationId(UUID.randomUUID().toString());
		providePaymentDetailsResponse.setPrijemce(prijemce.getHodnota());

		ProvidePaymentDetailsResponse.Cssz cssz;
		ProvidePaymentDetailsResponse.Vzp vzp;
		
		if (prijemce == PrijemceEnum.CSSZ) {
			List<Polozka> listPolozka = new ArrayList<Polozka>();
			for (int pol = 1; pol <=  pocetPolozek; pol++) {
				
				//<providePaymentDetailsResponse><cssz><polozky><polozka>
				Polozky.Polozka polozka = this._objectFactory.createProvidePaymentDetailsResponseCsszPolozkyPolozka();
				
				polozka.setMessageId(UUID.randomUUID().toString());
	
				List<Platba> listPlatba = new ArrayList<Platba>();
				for(int pla = 1; pla <= pocetPlatebVPolozce; pla++) {
					//<providePaymentDetailsResponse><cssz><polozky><polozka><platby><platba>
					Platba platba = this._objectFactory.createProvidePaymentDetailsResponseCsszPolozkyPolozkaPlatbyPlatba();
					
					String idPlatby = String.valueOf(
							BigDecimal.valueOf(
									((Double.valueOf("900000000000")) + pol)*1000 + pla));
					
					platba.setIdPlatby(idPlatby);
					platba.setKodZpracovani("000000000000000");
					platba.setPopisZpracovani(String.format("%sPopis zpracování pro pol=%s, pla=%s", version, pol, pla));
					
					listPlatba.add(platba);
				}
	
				//<providePaymentDetailsResponse><cssz><polozky><polozka><platby>
				Platby platby = this._objectFactory.createProvidePaymentDetailsResponseCsszPolozkyPolozkaPlatby();
				platby.platba = listPlatba;
				
				polozka.setPlatby(platby);
				
				listPolozka.add(polozka);
			}
			
			//<providePaymentDetailsResponse><cssz><polozky>
			ProvidePaymentDetailsResponse.Cssz.Polozky polozky = this._objectFactory.createProvidePaymentDetailsResponseCsszPolozky();
			polozky.polozka = listPolozka;
	
			//<providePaymentDetailsResponse><cssz>
			cssz = this._objectFactory.createProvidePaymentDetailsResponseCssz();
			if (pocetPolozek > 0) {
				cssz.setPolozky(polozky);
			}

			String statusCssz = StatusCsszEnum.values()[(int)(Math.random()*3)].getHodnota();
			cssz.setStatus(statusCssz);
			
			//<providePaymentDetailsResponse>
			providePaymentDetailsResponse.setCssz(cssz);
			
		} else if (prijemce == PrijemceEnum.VZP) {
			//<providePaymentDetailsResponse><vzp>
			vzp = this._objectFactory.createProvidePaymentDetailsResponseVzp();

			String statusVzp = StatusVzpEnum.values()[(int)(Math.random()*3)].getHodnota();
			vzp.setStatus(statusVzp);
			
			//<providePaymentDetailsResponse>
			providePaymentDetailsResponse.setVzp(vzp);
		}
		
		return providePaymentDetailsResponse;
	}

	
}
