package adis.bnk.par.providePaymentDetails.request;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;

import org.xml.sax.SAXException;

import adis.bnk.par.XmlBase;
import adis.bnk.par.db.TXParOdDs;
import adis.bnk.par.db.TXParOdDsCollection;
import adis.bnk.par.db.TXParOddoXml;
import adis.bnk.par.db.TXParRozpis;
import adis.bnk.par.db.TXParUhradaPl;
import adis.bnk.par.db.TXParUhradaPlCollection;
import adis.bnk.par.providePaymentDetails.request.ProvidePaymentDetailsRequest.KoncovaVeta;
import adis.bnk.par.providePaymentDetails.request.ProvidePaymentDetailsRequest.KoncovaVeta.KoncovaVetaTituly;
import adis.bnk.par.providePaymentDetails.request.ProvidePaymentDetailsRequest.KoncovaVeta.KoncovaVetaTituly.KoncovaVetaTitul;
import adis.bnk.par.providePaymentDetails.request.ProvidePaymentDetailsRequest.Polozky.Polozka;
import adis.bnk.par.providePaymentDetails.request.ProvidePaymentDetailsRequest.Polozky.Polozka.Platby;
import adis.bnk.par.providePaymentDetails.request.ProvidePaymentDetailsRequest.Polozky.Polozka.Platby.Platba;
import adis.bnk.par.providePaymentDetails.request.ProvidePaymentDetailsRequest.UvodniVeta;

/**
 * @author valenta
 */
public class XmlProvidePaymentDetailsRequest extends XmlBase<ProvidePaymentDetailsRequest> {

	private ObjectFactory _objectFactory = new ObjectFactory();
	private int _cZaznamu;
	private TXParOddoXml _tXParOddoXml;
	
	/**
	 * @param conn
	 * @param cZaznamu
	 */
	public XmlProvidePaymentDetailsRequest(Connection conn, int cZaznamu) {
		super(conn);
		this._cZaznamu = cZaznamu;
		
		this.set_xsdSchemaURL(
				this.getClass().getResource(
						"/adis/bnk/par/schema/IDR-INT_IA_ProvidePaymentDetails-P1_Request_v1.6.xsd"));
		
		//this.set_xsdSchemaFile(
		//		String.format(
		//				"%s%s", 
		//				System.getProperty("user.dir"), 
		//				"\\src\\main\\java\\adis\\bnk\\par\\schema\\IDR-INT_IA_ProvidePaymentDetails-P1_Request_v1.6.xsd"));
	}
	
	/**
	 * verze metody, ktera v kazdem cyklu nacita hodnoty z TXParUhradaPl pro c_zaznamu a message_id
	 * 
	 * @return
	 * @throws SQLException
	 * @throws DatatypeConfigurationException
	 */
	public ProvidePaymentDetailsRequest getRequestOld() 
			throws SQLException, DatatypeConfigurationException {
		
		this._tXParOddoXml = new TXParOddoXml(this._conn, this._cZaznamu);
		this._tXParOddoXml.setPropertiesForCZaznamu();
		
		TXParOdDsCollection tXParOdDsCollection = new TXParOdDsCollection(this._conn);
		tXParOdDsCollection.fillForCZaznamu(this._cZaznamu);
		
		List<Polozka> listPolozka = new ArrayList<>();
		for (TXParOdDs itemPolozka : tXParOdDsCollection) {
			//<providePaymentDetailsRequest><Polozky><Polozka>
			Polozka polozka = this._objectFactory.createProvidePaymentDetailsRequestPolozkyPolozka();
			
			polozka.setMessageId(itemPolozka.getMessageId());
			polozka.setLokalniAifo(itemPolozka.getAifo());
			polozka.setRodneCislo(itemPolozka.getRodneCislo());
			polozka.setCisloPojistence(itemPolozka.getCPoj());
			polozka.setEvidencniCisloPojistence(itemPolozka.getEcp());
			polozka.setJmeno(itemPolozka.getJmeno());
			polozka.setPrijmeni(itemPolozka.getPrijmeni());
			polozka.setRodnePrijmeni(itemPolozka.getRodnepr());
			if (itemPolozka.getDNaroz() != null) {
				polozka.setDatumNarozeni(this.convertFromDateToDate(itemPolozka.getDNaroz()));
			}

			TXParUhradaPlCollection tXParUhradaPlCollection = new TXParUhradaPlCollection(
					this._conn);
			tXParUhradaPlCollection.fillForCZaznamuAndMessageId(this._cZaznamu, 
					itemPolozka.getMessageId());

			List<Platba> listPlatba = new ArrayList<>();
			for(TXParUhradaPl itemPolozkaPlatba : tXParUhradaPlCollection) {
				//<providePaymentDetailsRequest><Polozky><Polozka><Platby><Platba>
				Platba platba = this._objectFactory.createProvidePaymentDetailsRequestPolozkyPolozkaPlatbyPlatba();
				
				//platba.setIdPlatby(itemPolozkaPlatba.getCUhrady() != null ? this.convertDecimal15ToUuidLike(itemPolozkaPlatba.getCUhrady()) : null);
				platba.setIdPlatby(itemPolozkaPlatba.getCUhrady() != null 
							? itemPolozkaPlatba.getCUhrady().toString() 
							: null);
				//platba.setIdPlatbyOprava(itemPolozkaPlatba.getCPuvUhrady() != null ? this.convertDecimal15ToUuidLike(itemPolozkaPlatba.getCPuvUhrady()) : null);
				platba.setIdPlatbyOprava(itemPolozkaPlatba.getCPuvUhrady() != null 
							? itemPolozkaPlatba.getCPuvUhrady().toString() 
							: null);
				platba.setTypInformace(itemPolozkaPlatba.getTypUhrady() != null 
							? itemPolozkaPlatba.getTypUhrady().trim() 
							: null);
				platba.setObdobiPredpis(itemPolozkaPlatba.getObdobi());
				platba.setVariabilniSymbol(
						itemPolozkaPlatba.getCPopl() != null ? itemPolozkaPlatba.getCPopl().trim() 
								: null);
				platba.setTitulPlatby(
						itemPolozkaPlatba.getNTitul() != null ? itemPolozkaPlatba.getNTitul().trim() 
								: null);
				platba.setDatumUhrady(this.convertFromDateToDate(itemPolozkaPlatba.getDUhrady()));
				platba.setCastka(itemPolozkaPlatba.getKcUhrady());
				
				listPlatba.add(platba);
			}

			//<providePaymentDetailsRequest><Polozky><Polozka><Platby>
			Platby platby = this._objectFactory.createProvidePaymentDetailsRequestPolozkyPolozkaPlatby();
			platby.platba = listPlatba;
			
			polozka.setPlatby(platby);
			
			listPolozka.add(polozka);
		}
		
		//<providePaymentDetailsRequest><Polozky>
		ProvidePaymentDetailsRequest.Polozky polozky = this._objectFactory.createProvidePaymentDetailsRequestPolozky();
		polozky.polozka = listPolozka;
		
		TXParRozpis tXParRozpis = new TXParRozpis(this._conn, this._cZaznamu);
		tXParRozpis.setPropertiesForPrimaryKey();

		//<providePaymentDetailsRequest><UvodniVeta>
		UvodniVeta uvodniVeta = this._objectFactory.createProvidePaymentDetailsRequestUvodniVeta();
		uvodniVeta.setVarSymbolPrerozdel(
				tXParRozpis.getCPoplPrijemce() != null ? tXParRozpis.getCPoplPrijemce().trim() 
						: null);
		uvodniVeta.setSpecSymbolPrerozdel(
				tXParRozpis.getSpecSymb() != null ? tXParRozpis.getSpecSymb().toString() : null);
		uvodniVeta.setObdobi(
				tXParRozpis.getObdobi() != null ? tXParRozpis.getObdobi().trim() : null);
		uvodniVeta.setDatumPrevodu(this.convertFromDateToDate(tXParRozpis.getDPlatby()));
		
		//<providePaymentDetailsRequest><KoncovaVeta>
		KoncovaVeta koncovaVeta = this._objectFactory.createProvidePaymentDetailsRequestKoncovaVeta();
		koncovaVeta.setPocetPolozek(tXParRozpis.getPocetCelk());
		koncovaVeta.setSuma(tXParRozpis.getKcCelk());
		
		List<KoncovaVetaTitul> listKoncovaVetaTitul = new ArrayList<>();
		//<providePaymentDetailsRequest><KoncovaVeta><SouhrnTitulPlatby><KoncovaVetaTitul> - PLPOJ
		if (tXParRozpis.getNDan() != null) {
			KoncovaVetaTitul koncovaVetaTitul1 = this._objectFactory.createProvidePaymentDetailsRequestKoncovaVetaKoncovaVetaTitulyKoncovaVetaTitul();
			koncovaVetaTitul1.setTitulPlatby(tXParRozpis.getNDan().trim());
			koncovaVetaTitul1.setTitulPlatbySuma(tXParRozpis.getKcDan());
			koncovaVetaTitul1.setTitulPlatbyPocetPolozek(tXParRozpis.getPocetDan());
			
			listKoncovaVetaTitul.add(koncovaVetaTitul1);
		}
		//<providePaymentDetailsRequest><KoncovaVeta><SouhrnTitulPlatby><KoncovaVetaTitul> - PLPRI
		if (tXParRozpis.getNPrisl() != null) {
			KoncovaVetaTitul koncovaVetaTitul2 = this._objectFactory.createProvidePaymentDetailsRequestKoncovaVetaKoncovaVetaTitulyKoncovaVetaTitul();
			koncovaVetaTitul2.setTitulPlatby(tXParRozpis.getNPrisl().trim());
			koncovaVetaTitul2.setTitulPlatbySuma(tXParRozpis.getKcPrisl());
			koncovaVetaTitul2.setTitulPlatbyPocetPolozek(tXParRozpis.getPocetPrisl());

			listKoncovaVetaTitul.add(koncovaVetaTitul2);
		}

		
		//<providePaymentDetailsRequest><KoncovaVeta><SouhrnTitulPlatby>
		KoncovaVetaTituly koncovaVetaTituly = this._objectFactory.createProvidePaymentDetailsRequestKoncovaVetaKoncovaVetaTituly();
		koncovaVetaTituly.koncovaVetaTitul = listKoncovaVetaTitul;
		
		koncovaVeta.setKoncovaVetaTituly(koncovaVetaTituly);
		
		//<providePaymentDetailsRequest>
		ProvidePaymentDetailsRequest providePaymentDetailsRequest = this._objectFactory.createProvidePaymentDetailsRequest();
		providePaymentDetailsRequest.setCorrelationId(this._tXParOddoXml.getCorrelationId());
		providePaymentDetailsRequest.setPrijemce(
				tXParRozpis.getKodPrijemce() != null ? tXParRozpis.getKodPrijemce().trim() : null);
		providePaymentDetailsRequest.setUvodniVeta(uvodniVeta);
		providePaymentDetailsRequest.setPolozky(polozky);
		providePaymentDetailsRequest.setKoncovaVeta(koncovaVeta);
		
		return providePaymentDetailsRequest;
	}

	/**
	 * verze metody, ktera nacita hodnoty z TXParUhradaPl jednou pro c_zaznamu a provadi filtrovani kolekce
	 * 
	 * @return
	 * @throws SQLException
	 * @throws DatatypeConfigurationException
	 */
	public ProvidePaymentDetailsRequest getRequest() 
			throws SQLException, DatatypeConfigurationException {
		
		this._tXParOddoXml = new TXParOddoXml(this._conn, this._cZaznamu);
		this._tXParOddoXml.setPropertiesForCZaznamu();

		//nacteni vsech hodnot z tabulky TXParOdDs pro _cZaznamu
		TXParOdDsCollection tXParOdDsCollection = new TXParOdDsCollection(this._conn);
		tXParOdDsCollection.fillForCZaznamu(this._cZaznamu);

		//nacteni vsech hodnot z tabulky TXParUhradaPl pro _cZaznamu
		TXParUhradaPlCollection tXParUhradaPlCollection = new TXParUhradaPlCollection(this._conn);
		tXParUhradaPlCollection.fillForCZaznamu(this._cZaznamu);
		
		List<Polozka> listPolozka = new ArrayList<>();
		for (TXParOdDs itemPolozka : tXParOdDsCollection) {
			//<providePaymentDetailsRequest><Polozky><Polozka>
			Polozka polozka = this._objectFactory.createProvidePaymentDetailsRequestPolozkyPolozka();
			
			polozka.setMessageId(itemPolozka.getMessageId());
			polozka.setLokalniAifo(itemPolozka.getAifo());
			polozka.setRodneCislo(itemPolozka.getRodneCislo());
			polozka.setCisloPojistence(itemPolozka.getCPoj());
			polozka.setEvidencniCisloPojistence(itemPolozka.getEcp());
			polozka.setJmeno(itemPolozka.getJmeno());
			polozka.setPrijmeni(itemPolozka.getPrijmeni());
			polozka.setRodnePrijmeni(itemPolozka.getRodnepr());
			if (itemPolozka.getDNaroz() != null) {
				polozka.setDatumNarozeni(this.convertFromDateToDate(itemPolozka.getDNaroz()));
			}

			List<Platba> listPlatba = new ArrayList<>();
			for(TXParUhradaPl itemPolozkaPlatba : tXParUhradaPlCollection.stream().filter(
					p -> p.getMessageId().equals(itemPolozka.getMessageId())).collect(
							Collectors.toList())) {
				//<providePaymentDetailsRequest><Polozky><Polozka><Platby><Platba>
				Platba platba = this._objectFactory.createProvidePaymentDetailsRequestPolozkyPolozkaPlatbyPlatba();
				
				//platba.setIdPlatby(itemPolozkaPlatba.getCUhrady() != null ? this.convertDecimal15ToUuidLike(itemPolozkaPlatba.getCUhrady()) : null);
				platba.setIdPlatby(itemPolozkaPlatba.getCUhrady() != null 
								? itemPolozkaPlatba.getCUhrady().toString() 
								: null);
				//platba.setIdPlatbyOprava(itemPolozkaPlatba.getCPuvUhrady() != null ? this.convertDecimal15ToUuidLike(itemPolozkaPlatba.getCPuvUhrady()) : null);
				platba.setIdPlatbyOprava(itemPolozkaPlatba.getCPuvUhrady() != null 
								? itemPolozkaPlatba.getCPuvUhrady().toString() 
								: null);
				platba.setTypInformace(itemPolozkaPlatba.getTypUhrady() != null 
								? itemPolozkaPlatba.getTypUhrady().trim() 
								: null);
				platba.setObdobiPredpis(itemPolozkaPlatba.getObdobi());
				platba.setVariabilniSymbol(
						itemPolozkaPlatba.getCPopl() != null ? itemPolozkaPlatba.getCPopl().trim() 
											: null);
				platba.setTitulPlatby(
						itemPolozkaPlatba.getNTitul() != null ? itemPolozkaPlatba.getNTitul().trim() 
											: null);
				platba.setDatumUhrady(this.convertFromDateToDate(itemPolozkaPlatba.getDUhrady()));
				platba.setCastka(itemPolozkaPlatba.getKcUhrady());
				
				listPlatba.add(platba);
			}

			//<providePaymentDetailsRequest><Polozky><Polozka><Platby>
			Platby platby = this._objectFactory.createProvidePaymentDetailsRequestPolozkyPolozkaPlatby();
			platby.platba = listPlatba;
			
			polozka.setPlatby(platby);
			
			listPolozka.add(polozka);
		}
		
		//<providePaymentDetailsRequest><Polozky>
		ProvidePaymentDetailsRequest.Polozky polozky = this._objectFactory.createProvidePaymentDetailsRequestPolozky();
		polozky.polozka = listPolozka;
		
		TXParRozpis tXParRozpis = new TXParRozpis(this._conn, this._cZaznamu);
		tXParRozpis.setPropertiesForPrimaryKey();

		//<providePaymentDetailsRequest><UvodniVeta>
		UvodniVeta uvodniVeta = this._objectFactory.createProvidePaymentDetailsRequestUvodniVeta();
		uvodniVeta.setVarSymbolPrerozdel(
				tXParRozpis.getCPoplPrijemce() != null ? tXParRozpis.getCPoplPrijemce().trim() 
								: null);
		uvodniVeta.setSpecSymbolPrerozdel(
				tXParRozpis.getSpecSymb() != null ? tXParRozpis.getSpecSymb().toString() : null);
		uvodniVeta.setObdobi(
				tXParRozpis.getObdobi() != null ? tXParRozpis.getObdobi().trim() : null);
		uvodniVeta.setDatumPrevodu(this.convertFromDateToDate(tXParRozpis.getDPlatby()));
		
		//<providePaymentDetailsRequest><KoncovaVeta>
		KoncovaVeta koncovaVeta = this._objectFactory.createProvidePaymentDetailsRequestKoncovaVeta();
		koncovaVeta.setPocetPolozek(tXParRozpis.getPocetCelk());
		koncovaVeta.setSuma(tXParRozpis.getKcCelk());
		
		
		List<KoncovaVetaTitul> listKoncovaVetaTitul = new ArrayList<>();
		//<providePaymentDetailsRequest><KoncovaVeta><SouhrnTitulPlatby><KoncovaVetaTitul> - PLPOJ
		if (tXParRozpis.getNDan() != null) {
			KoncovaVetaTitul koncovaVetaTitul1 = this._objectFactory.createProvidePaymentDetailsRequestKoncovaVetaKoncovaVetaTitulyKoncovaVetaTitul();
			koncovaVetaTitul1.setTitulPlatby(tXParRozpis.getNDan().trim());
			koncovaVetaTitul1.setTitulPlatbySuma(tXParRozpis.getKcDan());
			koncovaVetaTitul1.setTitulPlatbyPocetPolozek(tXParRozpis.getPocetDan());
			
			listKoncovaVetaTitul.add(koncovaVetaTitul1);
		}
		//<providePaymentDetailsRequest><KoncovaVeta><SouhrnTitulPlatby><KoncovaVetaTitul> - PLPRI
		if (tXParRozpis.getNPrisl() != null) {
			KoncovaVetaTitul koncovaVetaTitul2 = this._objectFactory.createProvidePaymentDetailsRequestKoncovaVetaKoncovaVetaTitulyKoncovaVetaTitul();
			koncovaVetaTitul2.setTitulPlatby(tXParRozpis.getNPrisl().trim());
			koncovaVetaTitul2.setTitulPlatbySuma(tXParRozpis.getKcPrisl());
			koncovaVetaTitul2.setTitulPlatbyPocetPolozek(tXParRozpis.getPocetPrisl());

			listKoncovaVetaTitul.add(koncovaVetaTitul2);
		}

		
		//<providePaymentDetailsRequest><KoncovaVeta><SouhrnTitulPlatby>
		KoncovaVetaTituly koncovaVetaTituly = this._objectFactory.createProvidePaymentDetailsRequestKoncovaVetaKoncovaVetaTituly();
		koncovaVetaTituly.koncovaVetaTitul = listKoncovaVetaTitul;
		
		koncovaVeta.setKoncovaVetaTituly(koncovaVetaTituly);
		
		//<providePaymentDetailsRequest>
		ProvidePaymentDetailsRequest providePaymentDetailsRequest = this._objectFactory.createProvidePaymentDetailsRequest();
		providePaymentDetailsRequest.setCorrelationId(this._tXParOddoXml.getCorrelationId());
		providePaymentDetailsRequest.setPrijemce(
				tXParRozpis.getKodPrijemce() != null ? tXParRozpis.getKodPrijemce().trim() : null);
		providePaymentDetailsRequest.setUvodniVeta(uvodniVeta);
		providePaymentDetailsRequest.setPolozky(polozky);
		providePaymentDetailsRequest.setKoncovaVeta(koncovaVeta);
		
		return providePaymentDetailsRequest;
	}
	
	/**
	 * @return
	 * @throws SQLException
	 * @throws DatatypeConfigurationException
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws JAXBException 
	 */
	public boolean createOrUpdateXmlToDb() 
			throws SQLException, DatatypeConfigurationException, IOException, JAXBException, SAXException {
		
		long startTime;
		long elapsedTime;

		//startTime = System.nanoTime();
		//ProvidePaymentDetailsRequest re = this.getRequestOld();
		//ByteArrayOutputStream baos = this.convertObjectToXmlOutputStream(re);
		//boolean result = this._tXParOddoXml.updateXmlOdesl(baos);
		//elapsedTime = System.nanoTime() - startTime;
		//System.out.println(String.format("By getRequestOld(). Total execution time in millis: %s", elapsedTime/1000000));

		startTime = System.nanoTime();
		ProvidePaymentDetailsRequest re = this.getRequest();
		ByteArrayOutputStream baos = this.convertObjectToXmlOutputStream(re);
		boolean result = this._tXParOddoXml.updateXmlOdesl(baos);
		elapsedTime = System.nanoTime() - startTime;
		System.out.println(String.format("By getRequest(). Total execution time in millis: %s", elapsedTime/1000000));
		
		baos.close();
		
		return result;
	}
	
	@Override
	protected void ValidateDuties() {
		
	}

	/**
	 * Vraci decimal(15,0) prevedeny na format uuid - napr. 00000000-0000-0020-0020-000000000175
	 * slouzi pro prevod hodnot c_uhradz a c_puv_uhrady do hodnot idPlatby a iPlatbyOprava
	 * 
	 * @param bd
	 * @return
	 */
	private String convertDecimal15ToUuidLike(BigDecimal bd) {
	
		String bdString = bd.toString().trim();
		if(bdString.length() != 15) {
			throw new IllegalArgumentException("Prevadena hodnota musi obsahovat 15 znaku.");
		}
		
		return String.format(
				"00000000-0000-00%s-00%s-0%s" 
				,bdString.substring(0,2)
				,bdString.substring(2,4)
				,bdString.substring(bdString.length()-11));
	}
}
