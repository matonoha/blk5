package adis.bnk.par.providePaymentDetails.response;

import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;

import org.xml.sax.SAXException;

import adis.bnk.par.XmlBase;
import adis.bnk.par.db.TXParOddoXml;
import adis.bnk.par.db.TXParRozpis;
import adis.bnk.par.db.TXParUhradaPl;
import adis.bnk.par.db.TpParTXParUhradaPl;
import adis.bnk.par.db.TpParTXParUhradaPlString;

/**
 * @author valenta
 *
 */
/**
 * @author valenta
 *
 */
public class XmlProvidePaymentDetailsResponse extends XmlBase<ProvidePaymentDetailsResponse> {

	private ObjectFactory _objectFactory = new ObjectFactory();
	private int _cZaznamu;
	private TXParOddoXml _tXParOddoXml;
	
    public enum PrijemceEnum {
    	VZP ("VZP"),
    	CSSZ ("CSSZ");
    	
    	private final String _hodnota;
    	
    	public String getHodnota() {
    		return _hodnota;
    	}
    	
    	PrijemceEnum (String hodnota) {
    		this._hodnota = hodnota;
    	}
    	
    	public static PrijemceEnum getByHodnota(String hodnota) throws Exception {
    		switch(hodnota) {
    		case "VZP":
    			return VZP;
    		case "CSSZ":
    			return CSSZ;
    		default:
    			throw new Exception("Neznamý typ hodnoty.");
    		}
    	}
    }
	
	/**
	 * @param conn
	 * @param cZaznamu
	 */
	public XmlProvidePaymentDetailsResponse(Connection conn, int cZaznamu) {
		super(conn);
		this._cZaznamu = cZaznamu;
		
		this.set_xsdSchemaURL(
				this.getClass().getResource("/adis/bnk/par/schema/IDR-INT_IA_ProvidePaymentDetails-P2_Response_v1.7.xsd"));
		
//		this.set_xsdSchemaFile(
//				String.format(
//						"%s%s", 
//						System.getProperty("user.dir"), 
//						"\\src\\main\\java\\adis\\bnk\\par\\schema\\IDR-INT_IA_ProvidePaymentDetails-P2_Response_v1.7.xsd"));
	}
	
	/**
	 * @return
	 * @throws SQLException
	 * @throws DatatypeConfigurationException
	 * @throws SAXException 
	 * @throws JAXBException 
	 */
	public ProvidePaymentDetailsResponse getResponse() 
			throws SQLException, DatatypeConfigurationException, JAXBException, SAXException {
		
		this._tXParOddoXml = new TXParOddoXml(this._conn, this._cZaznamu);
		this._tXParOddoXml.setPropertiesForCZaznamu();
		if (this._tXParOddoXml.getXmlDosly() == null) {
			throw new NullPointerException("xml_dosly");
		}
		
		Blob blob = this._tXParOddoXml.getXmlDosly();
		int blobLength = (int) blob.length();  
		byte[] blobAsBytes = blob.getBytes(1, blobLength);
		//blob.free();
		
		//Helper.writeFile("e:/temp/pokus2.xml", blobAsBytes);
		
		ProvidePaymentDetailsResponse providePaymentDetailsResponse = this._objectFactory.createProvidePaymentDetailsResponse();
		providePaymentDetailsResponse = this.convertBytesToObject(blobAsBytes, 
				providePaymentDetailsResponse);
		return providePaymentDetailsResponse;
	}
	
	/**
	 * @return
	 * @throws Exception 
	 */
	public boolean createOrUpdateXmlToDb() throws Exception {
		
		boolean result = false;
		ProvidePaymentDetailsResponse providePaymentDetailsResponse = this.getResponse();
		this._tXParOddoXml.updateCorrelationId(providePaymentDetailsResponse.getCorrelationId());
		
		PrijemceEnum prijemce =  PrijemceEnum.getByHodnota(
				providePaymentDetailsResponse.getPrijemce());
		
		int cZaznamu = this._tXParOddoXml.getCZaznamu();
		switch(prijemce) {
			case VZP:
				ProvidePaymentDetailsResponse.Vzp vzp = providePaymentDetailsResponse.getVzp();
				if (vzp != null) {
					result = processVZPResponse(vzp, cZaznamu);
				}
				break;
			case CSSZ:
				ProvidePaymentDetailsResponse.Cssz cssz = providePaymentDetailsResponse.getCssz();
				if (cssz != null) {
					
					long startTime;
					long elapsedTime;

//					startTime = System.nanoTime();
//					result = processCSSZResponse(cssz, cZaznamu);
//					elapsedTime = System.nanoTime() - startTime;
//					System.out.println(String.format("By statement. Total execution time in millis: %s", elapsedTime/1000000));
//
//					startTime = System.nanoTime();
//					result = processCSSZResponseProcedure(cssz, cZaznamu);
//					elapsedTime = System.nanoTime() - startTime;
//					System.out.println(String.format("By procedure. Total execution time in millis: %s", elapsedTime/1000000));

					startTime = System.nanoTime();
					result = processCSSZResponseDeleteInsertBatch(cssz, cZaznamu);
					elapsedTime = System.nanoTime() - startTime;
					System.out.println(String.format("By deleteInsertBatch. Total execution time in millis: %s", elapsedTime/1000000));
////					
////					startTime = System.nanoTime();
////					result = processCSSZResponseProcedureBatch(cssz, cZaznamu);
////					elapsedTime = System.nanoTime() - startTime;
////					System.out.println(String.format("By procedureBatch. Total execution time in millis: %s", elapsedTime/1000000));
////
//					startTime = System.nanoTime();
//					result = processCSSZResponseProcedureMultiString(cssz, cZaznamu);
//					elapsedTime = System.nanoTime() - startTime;
//					System.out.println(String.format("By procedureMultiString. Total execution time in millis: %s", elapsedTime/1000000));
//
//					startTime = System.nanoTime();
//					result = processCSSZResponseProcedureMultiType(cssz, cZaznamu);
//					elapsedTime = System.nanoTime() - startTime;
//					System.out.println(String.format("By procedureMultiType. Total execution time in millis: %s", elapsedTime/1000000));
					
				}
				break;
			default:
    			throw new Exception("Neznamý typ hodnoty.");
		}
		
		return result;
	}
	
	/**
	 * volani ukladani pres statement
	 * 
	 * @param cssz
	 * @param cZaznamu
	 * @return
	 * @throws Exception
	 */
	private boolean processVZPResponse(ProvidePaymentDetailsResponse.Vzp vzp, int cZaznamu) 
			throws Exception {
		boolean result = false;
		
		String statusVzp = vzp.getStatus();
		TXParRozpis tXParRozpis = new TXParRozpis(this._conn, cZaznamu, PrijemceEnum.VZP, 
				statusVzp);
		result = tXParRozpis.insertOrUpdate();
		
		return result;
	}

	/**
	 * volani ukladani pres statement
	 * 
	 * @param cssz
	 * @param cZaznamu
	 * @return
	 * @throws Exception
	 */
	private boolean processCSSZResponse(ProvidePaymentDetailsResponse.Cssz cssz, int cZaznamu) 
			throws Exception {
		boolean result = false;
		boolean resultAll = true;
		
		String statusCssz = cssz.getStatus();
		TXParRozpis tXParRozpis = new TXParRozpis(this._conn, cZaznamu, PrijemceEnum.CSSZ, 
				statusCssz);
		result = tXParRozpis.insertOrUpdate();

		if (!result) {
			resultAll = result;
		}

		if (cssz.getPolozky() == null) {
			return resultAll;
		}
		
		for(ProvidePaymentDetailsResponse.Cssz.Polozky.Polozka polozka : cssz.getPolozky().getPolozka()) {
			String messageId = polozka.getMessageId();

			for(ProvidePaymentDetailsResponse.Cssz.Polozky.Polozka.Platby.Platba platba : polozka.getPlatby().getPlatba()) {
				
				TXParUhradaPl tXParUhradaPl = new TXParUhradaPl(this._conn, cZaznamu, messageId, 
						platba);
				result = tXParUhradaPl.insertOrUpdate();
				if (!result) {
					resultAll = result;
				}
			}
		}
		
		return resultAll;
	}

	/**
	 * volani ukladani pres proceduru
	 * 
	 * @param cssz
	 * @param cZaznamu
	 * @return
	 * @throws Exception
	 */
	private boolean processCSSZResponseProcedure(ProvidePaymentDetailsResponse.Cssz cssz, int cZaznamu) throws Exception {
		boolean result = false;
		boolean resultAll = true;
		
		String statusCssz = cssz.getStatus();
		TXParRozpis tXParRozpis = new TXParRozpis(this._conn, cZaznamu, PrijemceEnum.CSSZ, statusCssz);
		result = tXParRozpis.insertOrUpdate();

		if (!result) {
			resultAll = result;
		}

		if (cssz.getPolozky() == null) {
			return resultAll;
		}

		for(ProvidePaymentDetailsResponse.Cssz.Polozky.Polozka polozka : cssz.getPolozky().getPolozka()) {
			String messageId = polozka.getMessageId();

			for(ProvidePaymentDetailsResponse.Cssz.Polozky.Polozka.Platby.Platba platba : polozka.getPlatby().getPlatba()) {
				
				TXParUhradaPl tXParUhradaPl = new TXParUhradaPl(this._conn, cZaznamu, messageId, platba);
				result = tXParUhradaPl.insertOrUpdateByProcedure();
				if (!result) {
					resultAll = result;
				}
			}
		}
		
		return resultAll;
	}

	/**
	 * volani ukladani pres proceduru jako BATCH - 1 volani do DB
	 * 
	 * @param cssz
	 * @param cZaznamu
	 * @return
	 * @throws Exception
	 */
	private boolean processCSSZResponseDeleteInsertBatch(ProvidePaymentDetailsResponse.Cssz cssz, int cZaznamu) throws Exception {
		boolean result = false;
		boolean resultAll = true;
		
		String statusCssz = cssz.getStatus();
		TXParRozpis tXParRozpis = new TXParRozpis(this._conn, cZaznamu, PrijemceEnum.CSSZ, statusCssz);
		result = tXParRozpis.insertOrUpdate();

		if (!result) {
			resultAll = result;
		}

		if (cssz.getPolozky() == null) {
			return resultAll;
		}
		
//		long startTime;
//		long elapsedTime;
		
		int pocetZaznamuVDavce = 1;
		int maxPocetZaznamuVDavce = 10000;
		
		ArrayList<TpParTXParUhradaPl> data = new ArrayList<TpParTXParUhradaPl>();
		for(ProvidePaymentDetailsResponse.Cssz.Polozky.Polozka polozka : cssz.getPolozky().getPolozka()) {
			String messageId = polozka.getMessageId();

			for(ProvidePaymentDetailsResponse.Cssz.Polozky.Polozka.Platby.Platba platba : polozka.getPlatby().getPlatba()) {
				
			    	if (platba.getIdPlatby() == null) {
			    		throw new NullPointerException("Povinna polozka idPlatby je NULL.");
			    	}

			    	data.add(
							new TpParTXParUhradaPl(
									cZaznamu, 
									BigDecimal.valueOf(Double.parseDouble(platba.getIdPlatby())).setScale(0), 
									messageId, 
									null,
									null, 
									null, 
									null, 
									null, 
									null, 
									null,
									platba.getKodZpracovani(), 
									platba.getPopisZpracovani()));
					pocetZaznamuVDavce++;

					if(pocetZaznamuVDavce > maxPocetZaznamuVDavce) {
//						startTime = System.nanoTime();
						result = TXParUhradaPl.insertOrUpdateByDeleteInsertBatch(this._conn, data);
//						elapsedTime = System.nanoTime() - startTime;
//						System.out.println(String.format("1. Processed %s records. Total execution time in millis: %s", data.size(), elapsedTime/1000000));
	
						
						data.clear();
						pocetZaznamuVDavce = 1;
						
						if (!result) {
							resultAll = result;
					}
				}
			}
			
			if (pocetZaznamuVDavce > maxPocetZaznamuVDavce) {
//				startTime = System.nanoTime();
				result = TXParUhradaPl.insertOrUpdateByDeleteInsertBatch(this._conn, data);
//				elapsedTime = System.nanoTime() - startTime;
//				System.out.println(String.format("2. Processed %s records. Total execution time in millis: %s", data.size(), elapsedTime/1000000));

				
				data.clear();
				pocetZaznamuVDavce = 1;
				
				if (!result) {
					resultAll = result;
				}
				
			}
		
		}

		if (data.size() > 0) {
//			startTime = System.nanoTime();
			result = TXParUhradaPl.insertOrUpdateByDeleteInsertBatch(this._conn, data);
//			elapsedTime = System.nanoTime() - startTime;
//			System.out.println(String.format("3. Processed %s records. Total execution time in millis: %s", data.size(), elapsedTime/1000000));
			
			if (!result) {
				resultAll = result;
			}
			
		}
		
		return resultAll;
	}
	
//	//volani ukladani pres proceduru jako BATCH - 1 volani do DB
//	private boolean processCSSZResponseProcedureBatch(ProvidePaymentDetailsResponse.Cssz cssz, int cZaznamu) throws Exception {
//		boolean result = false;
//		boolean resultAll = true;
//		
//		String statusCssz = cssz.getStatus();
//		TXParRozpis tXParRozpis = new TXParRozpis(this._conn, cZaznamu, PrijemceEnum.CSSZ, statusCssz);
//		result = tXParRozpis.insertOrUpdate();
//	
//		if (!result) {
//			resultAll = result;
//		}
//	
//		if (cssz.getPolozky() == null) {
//			return resultAll;
//		}
//		
////		long startTime;
////		long elapsedTime;
//		
//		int pocetZaznamuVDavce = 1;
//		int maxPocetZaznamuVDavce = 10000;
//		
//		ArrayList<TpParTXParUhradaPl> data = new ArrayList<TpParTXParUhradaPl>();
//		for(ProvidePaymentDetailsResponse.Cssz.Polozky.Polozka polozka : cssz.getPolozky().getPolozka()) {
//			String messageId = polozka.getMessageId();
//
//			for(ProvidePaymentDetailsResponse.Cssz.Polozky.Polozka.Platby.Platba platba : polozka.getPlatby().getPlatba()) {
//				
//			    	if (platba.getIdPlatby() == null) {
//			    		throw new NullPointerException("Povinna polozka idPlatby je NULL.");
//			    	}
//
//			    	data.add(
//							new TpParTXParUhradaPl(
//									cZaznamu, 
//									BigDecimal.valueOf(Double.parseDouble(platba.getIdPlatby())).setScale(0), 
//									messageId, 
//									null,
//									null, 
//									null, 
//									null, 
//									null, 
//									null, 
//									null,
//									platba.getKodZpracovani(), 
//									platba.getPopisZpracovani()));
//					pocetZaznamuVDavce++;
//
//					if(pocetZaznamuVDavce > maxPocetZaznamuVDavce) {
////						startTime = System.nanoTime();
//						result = TXParUhradaPl.insertOrUpdateByProcedureBatch(this._conn, data);
////						elapsedTime = System.nanoTime() - startTime;
////						System.out.println(String.format("1. Processed %s records. Total execution time in millis: %s", data.size(), elapsedTime/1000000));
//	
//						
//						data.clear();
//						pocetZaznamuVDavce = 1;
//						
//						if (!result) {
//							resultAll = result;
//					}
//				}
//			}
//			
//			if (pocetZaznamuVDavce > maxPocetZaznamuVDavce) {
////				startTime = System.nanoTime();
//				result = TXParUhradaPl.insertOrUpdateByProcedureBatch(this._conn, data);
////				elapsedTime = System.nanoTime() - startTime;
////				System.out.println(String.format("2. Processed %s records. Total execution time in millis: %s", data.size(), elapsedTime/1000000));
//
//				
//				data.clear();
//				pocetZaznamuVDavce = 1;
//				
//				if (!result) {
//					resultAll = result;
//				}
//				
//			}
//		
//		}
//
//		if (data.size() > 0) {
////			startTime = System.nanoTime();
//			result = TXParUhradaPl.insertOrUpdateByProcedureBatch(this._conn, data);
////			elapsedTime = System.nanoTime() - startTime;
////			System.out.println(String.format("3. Processed %s records. Total execution time in millis: %s", data.size(), elapsedTime/1000000));
//			
//			if (!result) {
//				resultAll = result;
//			}
//			
//		}
//		
//		return resultAll;
//	}
	
	/**
	 * volani ukladani pres proceduru, ktera prijima hodnoty jako tabulku - hodnoty jako STRING - 1 volani do DB
	 * 
	 * @param cssz
	 * @param cZaznamu
	 * @return
	 * @throws Exception
	 */
	private boolean processCSSZResponseProcedureMultiString(ProvidePaymentDetailsResponse.Cssz cssz, int cZaznamu) throws Exception {
		boolean result = false;
		boolean resultAll = true;
		
		String statusCssz = cssz.getStatus();
		TXParRozpis tXParRozpis = new TXParRozpis(this._conn, cZaznamu, PrijemceEnum.CSSZ, statusCssz);
		result = tXParRozpis.insertOrUpdate();

		if (!result) {
			resultAll = result;
		}

		if (cssz.getPolozky() == null) {
			return resultAll;
		}
		
//		long startTime;
//		long elapsedTime;
		
		int pocetZaznamuVDavce = 1;
		int maxPocetZaznamuVDavce = 10000;
		
		ArrayList<TpParTXParUhradaPlString> data = new ArrayList<TpParTXParUhradaPlString>();
		for(ProvidePaymentDetailsResponse.Cssz.Polozky.Polozka polozka : cssz.getPolozky().getPolozka()) {
			String messageId = polozka.getMessageId();

			for(ProvidePaymentDetailsResponse.Cssz.Polozky.Polozka.Platby.Platba platba : polozka.getPlatby().getPlatba()) {
				
			    	if (platba.getIdPlatby() == null) {
			    		throw new NullPointerException("Povinna polozka idPlatby je NULL.");
			    	}

			    	data.add(
							new TpParTXParUhradaPlString(
									String.valueOf(cZaznamu), 
									String.valueOf(Double.parseDouble(platba.getIdPlatby())), 
									messageId, 
									"null",
									"null", 
									"null", 
									"null", 
									"null", 
									"null", 
									"null",
									platba.getKodZpracovani(), 
									platba.getPopisZpracovani()));
					pocetZaznamuVDavce++;

					if(pocetZaznamuVDavce > maxPocetZaznamuVDavce) {
//						startTime = System.nanoTime();
						result = TXParUhradaPl.insertOrUpdateByProcedureMultiString(this._conn, data);
//						elapsedTime = System.nanoTime() - startTime;
//						System.out.println(String.format("1. Processed %s records. Total execution time in millis: %s", data.size(), elapsedTime/1000000));
	
						
						data.clear();
						pocetZaznamuVDavce = 1;
						
						if (!result) {
							resultAll = result;
					}
				}
			}
			
			if (pocetZaznamuVDavce > maxPocetZaznamuVDavce) {
//				startTime = System.nanoTime();
				result = TXParUhradaPl.insertOrUpdateByProcedureMultiString(this._conn, data);
//				elapsedTime = System.nanoTime() - startTime;
//				System.out.println(String.format("2. Processed %s records. Total execution time in millis: %s", data.size(), elapsedTime/1000000));

				
				data.clear();
				pocetZaznamuVDavce = 1;
				
				if (!result) {
					resultAll = result;
				}
				
			}
		
		}

		if (data.size() > 0) {
//			startTime = System.nanoTime();
			result = TXParUhradaPl.insertOrUpdateByProcedureMultiString(this._conn, data);
//			elapsedTime = System.nanoTime() - startTime;
//			System.out.println(String.format("3. Processed %s records. Total execution time in millis: %s", data.size(), elapsedTime/1000000));
			
			if (!result) {
				resultAll = result;
			}
			
		}
		
		return resultAll;
	}
	
	/**
	 * volani ukladani pres proceduru, ktera prijima hodnoty jako tabulku - hodnoty jako TYPE - 1 volani do DB
	 * 
	 * @param cssz
	 * @param cZaznamu
	 * @return
	 * @throws Exception
	 */
	private boolean processCSSZResponseProcedureMultiType(ProvidePaymentDetailsResponse.Cssz cssz, int cZaznamu) throws Exception {
		boolean result = false;
		boolean resultAll = true;
		
		String statusCssz = cssz.getStatus();
		TXParRozpis tXParRozpis = new TXParRozpis(this._conn, cZaznamu, PrijemceEnum.CSSZ, statusCssz);
		result = tXParRozpis.insertOrUpdate();

		if (!result) {
			resultAll = result;
		}

		if (cssz.getPolozky() == null) {
			return resultAll;
		}
		
//		long startTime;
//		long elapsedTime;
		
		int pocetZaznamuVDavce = 1;
		int maxPocetZaznamuVDavce = 10000;
		
		ArrayList<TpParTXParUhradaPl> data = new ArrayList<TpParTXParUhradaPl>();
		for(ProvidePaymentDetailsResponse.Cssz.Polozky.Polozka polozka : cssz.getPolozky().getPolozka()) {
			String messageId = polozka.getMessageId();

			for(ProvidePaymentDetailsResponse.Cssz.Polozky.Polozka.Platby.Platba platba : polozka.getPlatby().getPlatba()) {
				
			    	if (platba.getIdPlatby() == null) {
			    		throw new NullPointerException("Povinna polozka idPlatby je NULL.");
			    	}

			    	data.add(
							new TpParTXParUhradaPl(
									cZaznamu, 
									BigDecimal.valueOf(Double.parseDouble(platba.getIdPlatby())).setScale(0), 
									messageId, 
									BigDecimal.ZERO.setScale(0),
									null, 
									null, 
									null, 
									null, 
									null, 
									BigDecimal.ZERO.setScale(2),
									platba.getKodZpracovani(), 
									platba.getPopisZpracovani()));
					pocetZaznamuVDavce++;

					if(pocetZaznamuVDavce > maxPocetZaznamuVDavce) {
//						startTime = System.nanoTime();
						result = TXParUhradaPl.insertOrUpdateByProcedureMulti(this._conn, data);
//						elapsedTime = System.nanoTime() - startTime;
//						System.out.println(String.format("1. Processed %s records. Total execution time in millis: %s", data.size(), elapsedTime/1000000));
	
						
						data.clear();
						pocetZaznamuVDavce = 1;
						
						if (!result) {
							resultAll = result;
					}
				}
			}
			
			if (pocetZaznamuVDavce > maxPocetZaznamuVDavce) {
//				startTime = System.nanoTime();
				result = TXParUhradaPl.insertOrUpdateByProcedureMulti(this._conn, data);
//				elapsedTime = System.nanoTime() - startTime;
//				System.out.println(String.format("2. Processed %s records. Total execution time in millis: %s", data.size(), elapsedTime/1000000));

				
				data.clear();
				pocetZaznamuVDavce = 1;
				
				if (!result) {
					resultAll = result;
				}
				
			}
		
		}

		if (data.size() > 0) {
//			startTime = System.nanoTime();
			result = TXParUhradaPl.insertOrUpdateByProcedureMulti(this._conn, data);
//			elapsedTime = System.nanoTime() - startTime;
//			System.out.println(String.format("3. Processed %s records. Total execution time in millis: %s", data.size(), elapsedTime/1000000));
			
			if (!result) {
				resultAll = result;
			}
			
		}
		
		return resultAll;
	}
	
	@Override
	protected void ValidateDuties() {
		
	}
	
}
