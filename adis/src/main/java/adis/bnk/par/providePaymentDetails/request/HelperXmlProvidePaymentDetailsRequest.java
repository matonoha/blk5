package adis.bnk.par.providePaymentDetails.request;

import java.math.BigDecimal;
import java.math.MathContext;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.xml.datatype.DatatypeConfigurationException;

import adis.bnk.par.XmlBase;
import adis.bnk.par.providePaymentDetails.request.ProvidePaymentDetailsRequest.KoncovaVeta.KoncovaVetaTituly;
import adis.bnk.par.providePaymentDetails.request.ProvidePaymentDetailsRequest.KoncovaVeta.KoncovaVetaTituly.KoncovaVetaTitul;
import adis.bnk.par.providePaymentDetails.request.ProvidePaymentDetailsRequest.Polozky;
import adis.bnk.par.providePaymentDetails.request.ProvidePaymentDetailsRequest.Polozky.Polozka;
import adis.bnk.par.providePaymentDetails.request.ProvidePaymentDetailsRequest.Polozky.Polozka.Platby;
import adis.bnk.par.providePaymentDetails.request.ProvidePaymentDetailsRequest.Polozky.Polozka.Platby.Platba;

/**
 * @author valenta
 *
 */
public class HelperXmlProvidePaymentDetailsRequest extends XmlBase<ProvidePaymentDetailsRequest> {

	private ObjectFactory _objectFactory = new ObjectFactory();
	private final String version = "v1.6_";

	
    public enum PrijemceEnum {
    	VZP ("VZP"),
    	CSSZ ("CSSZ");
    	
    	private final String _hodnota;
    	
    	String getHodnota() {
    		return _hodnota;
    	}
    	
    	PrijemceEnum (String hodnota) {
    		this._hodnota = hodnota;
    	}
    	
    	public static PrijemceEnum getByHodnota(String hodnota) throws Exception {
    		switch(hodnota) {
    		case "VZP":
    			return VZP;
    		case "CSSZ":
    			return CSSZ;
    		default:
    			throw new Exception("Neznamý typ hodnoty.");
    		}
    	}
    }

    public enum TypInformaceEnum {
    	PLATBA ("PLATBA"),
    	OPRAVA ("OPRAVA");
    	
    	private final String _hodnota;
    	
    	String getHodnota() {
    		return _hodnota;
    	}
    	
    	TypInformaceEnum (String hodnota) {
    		this._hodnota = hodnota;
    	}
    	
    	public static TypInformaceEnum getByHodnota(String hodnota) throws Exception {
    		switch(hodnota) {
    		case "PLATBA":
    			return PLATBA;
    		case "OPRAVA":
    			return OPRAVA;
    		default:
    			throw new Exception("Neznamý typ hodnoty.");
    		}
    	}
    }

    public enum TitulPlatbyEnum {
    	PLPOJ ("PLPOJ"),
    	PLPRI ("PLPRI");
    	
    	private final String _hodnota;
    	
    	String getHodnota() {
    		return _hodnota;
    	}
    	
    	TitulPlatbyEnum (String hodnota) {
    		this._hodnota = hodnota;
    	}
    	
    	public static TitulPlatbyEnum getByHodnota(String hodnota) throws Exception {
    		switch(hodnota) {
    		case "PLPOJ":
    			return PLPOJ;
    		case "PLPRI":
    			return PLPRI;
    		default:
    			throw new Exception("Neznamý typ hodnoty.");
    		}
    	}
    }

	/**
	 * @param conn
	 * @param cZaznamu
	 */
	public HelperXmlProvidePaymentDetailsRequest(Connection conn) {
		super(conn);
		
		this.set_xsdSchemaURL(
				this.getClass().getResource("/adis/bnk/par/schema/IDR-INT_IA_ProvidePaymentDetails-P1_Request_v1.6.xsd"));
	}
	
	public ProvidePaymentDetailsRequest getTestRequest(PrijemceEnum prijemce, int pocetPolozek, int pocetPlatebVPolozce) throws DatatypeConfigurationException {
		
		//<providePaymentDetailsRequest>
		ProvidePaymentDetailsRequest providePaymentDetailsRequest = this._objectFactory.createProvidePaymentDetailsRequest();
		providePaymentDetailsRequest.setCorrelationId(UUID.randomUUID().toString());

		providePaymentDetailsRequest.setPrijemce(prijemce.getHodnota());
		
		providePaymentDetailsRequest.setUvodniVeta(this._objectFactory.createProvidePaymentDetailsRequestUvodniVeta());
		providePaymentDetailsRequest.uvodniVeta.setVarSymbolPrerozdel(String.format("1100000000"));
		providePaymentDetailsRequest.uvodniVeta.setSpecSymbolPrerozdel(String.valueOf((long)(Math.random()*9999999999d)));
		providePaymentDetailsRequest.uvodniVeta.setObdobi("01/2015");
		providePaymentDetailsRequest.uvodniVeta.setDatumPrevodu(this.convertFromDateToDate(new java.util.Date()));
		
		List<Polozka> listPolozka = new ArrayList<Polozka>();
		for (int pol = 1; pol <=  pocetPolozek; pol++) {
			
			//<providePaymentDetailsRequest><polozky><polozka>
			Polozky.Polozka polozka = this._objectFactory.createProvidePaymentDetailsRequestPolozkyPolozka();
			
			polozka.setMessageId(UUID.randomUUID().toString());

			switch ((int)(Math.random()*2)) {
				case 0:
					String aifo = String.format("aaaaaaaaaaaaaaaaaaaaaaaa%s", pol);
					polozka.setLokalniAifo(aifo.substring(aifo.length() - 24));
					break;
				case 1:
				default:
					break;
			}

			switch ((int)(Math.random()*2)) {
			case 0:
					String rodneCislo = String.format("0000000000%s", pol); 
					polozka.setRodneCislo(rodneCislo.substring(rodneCislo.length() - 10));
					break;
				case 1:
				default:
					break;
			}
			
			String cisloPojistence = String.format("%s0000000000", pol);
			polozka.setCisloPojistence(cisloPojistence.substring(0,10));
			
			switch ((int)(Math.random()*2)) {
				case 0:
					String evidencniCisloPojistence = String.format("0%s000000000", pol);
					polozka.setEvidencniCisloPojistence(evidencniCisloPojistence.substring(0,10));
					break;
				case 1:
				default:
					break;
			}
			
			polozka.setJmeno(String.format("%sJméno%s", version, pol));
			
			polozka.setPrijmeni(String.format("%sPříjmení%s", version, pol));
			
			switch ((int)(Math.random()*2)) {
				case 0:
					polozka.setRodnePrijmeni(String.format("%sRodnéPříjmení%s", version, pol));
					break;
				case 1:
				default:
					break;
			}
	
			polozka.setDatumNarozeni(this.convertFromDateToDate(new java.util.Date()));
			
			List<Platba> listPlatba = new ArrayList<Platba>();
			for(int pla = 1; pla <= pocetPlatebVPolozce; pla++) {
				//<providePaymentDetailsRequest><polozky><polozka><platby><platba>
				Platba platba = this._objectFactory.createProvidePaymentDetailsRequestPolozkyPolozkaPlatbyPlatba();
				
				String idPlatby = String.valueOf(
						BigDecimal.valueOf(
								((Double.valueOf("900000000000")) + pol)*1000 + pla));
				platba.setIdPlatby(idPlatby);

				switch ((int)(Math.random()*2)) {
					case 0:
						String idPlatbyOprava = String.valueOf(
								BigDecimal.valueOf(
										((Double.valueOf("910000000000")) + pol)*1000 + pla));
						platba.setIdPlatbyOprava(idPlatbyOprava);
						break;
					case 1:
					default:
						break;
				}

				switch ((int)(Math.random()*2)) {
					case 0:
						platba.setTypInformace(TypInformaceEnum.PLATBA.getHodnota());
						break;
					case 1:
						platba.setTypInformace(TypInformaceEnum.OPRAVA.getHodnota());
						break;
					default:
						break;
				}
				
				platba.setObdobiPredpis("01/2015");
				
				String variabilniSymbol = String.format("0000000000%s", pol); 
				platba.setVariabilniSymbol(variabilniSymbol.substring(variabilniSymbol.length() - 10));
				
				switch ((int)(Math.random()*2)) {
					case 0:
						platba.setTitulPlatby(TitulPlatbyEnum.PLPOJ.getHodnota());
						break;
					case 1:
						platba.setTitulPlatby(TitulPlatbyEnum.PLPRI.getHodnota());
						break;
					default:
						break;
				}
				
				platba.setDatumUhrady(this.convertFromDateToDate(new java.util.Date()));

				BigDecimal castka = new BigDecimal((Math.random()*99999999999999d), MathContext.DECIMAL64);
				platba.setCastka(castka.setScale(2, BigDecimal.ROUND_HALF_EVEN));
				
				listPlatba.add(platba);
			}

			//<providePaymentDetailsRequest><polozky><polozka><platby>
			Platby platby = this._objectFactory.createProvidePaymentDetailsRequestPolozkyPolozkaPlatby();
			platby.platba = listPlatba;
			
			polozka.setPlatby(platby);
			
			listPolozka.add(polozka);
		}
		
		//<providePaymentDetailsRequest><polozky>
		Polozky polozky = this._objectFactory.createProvidePaymentDetailsRequestPolozky();
		polozky.polozka = listPolozka;
		providePaymentDetailsRequest.setPolozky(polozky);
		
		providePaymentDetailsRequest.setKoncovaVeta(this._objectFactory.createProvidePaymentDetailsRequestKoncovaVeta());
		providePaymentDetailsRequest.koncovaVeta.setPocetPolozek(pocetPolozek);
		
		BigDecimal suma = new BigDecimal((Math.random()*99999999999999d), MathContext.DECIMAL64);
		providePaymentDetailsRequest.koncovaVeta.setSuma(suma.setScale(2, BigDecimal.ROUND_HALF_EVEN));
		
		List<KoncovaVetaTitul> listKoncovaVetatTitul = new ArrayList<KoncovaVetaTitul>();
		for(int kvt = 1; kvt <= 2; kvt++) {
			//<providePaymentDetailsRequest><koncovaVeta><koncovaVetatTituly><koncovaVetaTitul>
			KoncovaVetaTitul koncovaVetaTitul = this._objectFactory.createProvidePaymentDetailsRequestKoncovaVetaKoncovaVetaTitulyKoncovaVetaTitul();
			
			switch (kvt) {
				case 1:
					koncovaVetaTitul.setTitulPlatby(TitulPlatbyEnum.PLPOJ.getHodnota());
					break;
				case 2:
					koncovaVetaTitul.setTitulPlatby(TitulPlatbyEnum.PLPRI.getHodnota());
					break;
				default:
					break;
			}
			BigDecimal titulPlatbySuma = new BigDecimal((Math.random()*99999999999999d), MathContext.DECIMAL64);
			koncovaVetaTitul.setTitulPlatbySuma(titulPlatbySuma.setScale(2, BigDecimal.ROUND_HALF_EVEN));
			koncovaVetaTitul.setTitulPlatbyPocetPolozek((int)(Math.random()*100));
			
			listKoncovaVetatTitul.add(koncovaVetaTitul);
		}

		//<providePaymentDetailsRequest><koncovaVeta><koncovaVetatTituly>
		KoncovaVetaTituly koncovaVetaTituly = this._objectFactory.createProvidePaymentDetailsRequestKoncovaVetaKoncovaVetaTituly();
		koncovaVetaTituly.koncovaVetaTitul = listKoncovaVetatTitul;
		
		providePaymentDetailsRequest.koncovaVeta.setKoncovaVetaTituly(koncovaVetaTituly);
		
		return providePaymentDetailsRequest;
	}

	
}
